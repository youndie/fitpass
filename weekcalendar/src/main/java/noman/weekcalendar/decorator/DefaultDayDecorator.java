package noman.weekcalendar.decorator;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.util.TypedValue;
import android.view.View;

import org.joda.time.DateTime;

import noman.weekcalendar.fragment.WeekFragment;
import noman.weekcalendar.view.DayTextView;

/**
 * Created by gokhan on 7/27/16.
 */
public class DefaultDayDecorator implements DayDecorator {

    private int todayDateTextColor;
    private int textColor;
    private float textSize;

    public DefaultDayDecorator(
            @ColorInt int todayDateTextColor,
            @ColorInt int textColor,
            float textSize) {
        this.todayDateTextColor = todayDateTextColor;
        this.textColor = textColor;
        this.textSize = textSize;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void decorate(View view, DayTextView dayTextView,
                         DateTime dateTime, DateTime firstDayOfTheWeek, DateTime selectedDateTime, boolean isEvent) {
//        //DateTime dt = new DateTime();
//        Drawable holoCircle = ContextCompat.getDrawable(context, R.drawable.holo_circle);
//        Drawable solidCircle = ContextCompat.getDrawable(context, R.drawable.solid_circle);
////        Drawable solidCircleEvent = ContextCompat.getDrawable(context, R.drawable.solid_circle);
//
//        holoCircle.setColorFilter(selectedDateColor, PorterDuff.Mode.SRC_ATOP);
//        solidCircle.setColorFilter(todayDateColor, PorterDuff.Mode.SRC_ATOP);
////        solidCircleEvent.setColorFilter(eventBackColor, PorterDuff.Mode.SRC_ATOP);
//        // solidCircle.mutate().setAlpha(200);
//        //holoCircle.mutate().setAlpha(200);


        if (firstDayOfTheWeek.getMonthOfYear() < dateTime.getMonthOfYear()
                || firstDayOfTheWeek.getYear() < dateTime.getYear())
            dayTextView.setTextColor(Color.GRAY);

        DateTime calendarStartDate = WeekFragment.CalendarStartDate;

        if (selectedDateTime != null) {
            if (selectedDateTime.toLocalDate().equals(dateTime.toLocalDate())) {
                if (!selectedDateTime.toLocalDate().equals(calendarStartDate.toLocalDate()))
                    dayTextView.setBackground(dayTextView.getHoloCircle());
            } else {
                dayTextView.setBackground(null);
            }
        }

        if (dateTime.toLocalDate().equals(calendarStartDate.toLocalDate())) {
            dayTextView.setBackground(dayTextView.getSolidCircle());
            dayTextView.setTextColor(this.todayDateTextColor);
        } else {
            dayTextView.setTextColor(textColor);
        }

        if (!isEvent) {
            dayTextView.setAlpha(0.5f);
        } else {
            dayTextView.setAlpha(1f);
        }

        float size = textSize;
        if (size == -1)
            size = dayTextView.getTextSize();
        dayTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
    }
}
