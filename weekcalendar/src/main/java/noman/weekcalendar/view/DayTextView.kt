package noman.weekcalendar.view

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.view.Gravity.CENTER
import android.view.View.MeasureSpec.AT_MOST
import android.widget.TextView
import noman.weekcalendar.R

/**
 * Created by Pavel Votyakov on 23.04.17.
 * Work in progress © 2017
 */

class DayTextView(context: Context) : TextView(context) {
    var holoCircle: Drawable = ContextCompat.getDrawable(context, R.drawable.holo_circle)!!
    var solidCircle: Drawable = ContextCompat.getDrawable(context, R.drawable.solid_circle)!!
    val size = context.resources.getDimension(R.dimen.day_text_wh).toInt()

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val measureSpec = MeasureSpec.makeMeasureSpec(size, AT_MOST)
        setMeasuredDimension(measureSpec, measureSpec)
    }

    init {
        holoCircle.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
        solidCircle.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
        gravity = CENTER
    }

}