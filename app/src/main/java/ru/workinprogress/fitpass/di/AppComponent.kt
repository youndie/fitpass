package ru.workinprogress.fitpass.di

import dagger.Component

import okhttp3.OkHttpClient
import ru.workinprogress.fitpass.presentation.LessonPresenter
import ru.workinprogress.fitpass.presentation.*
import ru.workinprogress.fitpass.presentation.settings.*
import ru.workinprogress.fitpass.ui.activity.MainActivity

@Component(modules = [(AppModule::class)])
@ApplicationScope
interface AppComponent {

    val okHttpClient: OkHttpClient

    fun inject(mainActivity: MainActivity)
    fun inject(loginPresenter: LoginPresenter)
    fun inject(registrationPresenter: RegistrationPresenter)
    fun inject(loginPhonePresenter: LoginPhonePresenter)
    fun inject(loginSMSPresenter: LoginSMSPresenter)
    fun inject(schedulePresenter: SchedulePresenter)
    fun inject(mainPresenter: MainPresenter)
    fun inject(pricelistPresenter: PricelistPresenter)
    fun inject(pricelistItemPresenter: PricelistItemPresenter)
    fun inject(servicesPresenter: ServicesPresenter)
    fun inject(addClubPresenter: AddClubPresenter)
    fun inject(profilePresenter: ProfilePresenter)
    fun inject(lessonPresenter: LessonPresenter)
    fun inject(aboutClubPresenter: AboutClubPresenter)
    fun inject(bonusesPresenter: BonusesPresenter)
    fun inject(newsPresenter: NewsPresenter)
    fun inject(specialOfferPresenter: SpecialOfferPresenter)
    fun inject(newsDetailPresenter: NewsDetailPresenter)
    fun inject(notificationDetailPresenter: NotificationDetailPresenter)
    fun inject(mainWindowPresenter: MainWindowPresenter)
    fun inject(contractPriceListPresenter: ContractPriceListPresenter)
    fun inject(contractPricelistItemPresenter: ContractPricelistItemPresenter)
    fun inject(contractPresenter: ContractPresenter)
    fun inject(unpaidGroupPresenter: UnpaidGroupPresenter)
    fun inject(lessonPricelistPresenter: LessonPricelistPresenter)
    fun inject(lessonPricelistDetailPresenter: LessonPricelistDetailPresenter)
    fun inject(accountsPresenter: AccountsPresenter)
    fun inject(operationsPresenter: OperationsPresenter)
    fun inject(settingsPresenter: SettingsPresenter)
    fun inject(cardsPresenter: CardsPresenter)
    fun inject(clubsPresenter: ClubsPresenter)
    fun inject(editProfilePresenter: EditProfilePresenter)
    fun inject(changePhonePresenter: ChangePhonePresenter)
    fun inject(deleteAccountPresenter: DeleteAccountPresenter)
    fun inject(profileSettingsPresenter: ProfileSettingsPresenter)
    fun inject(availableBlockPresenter: AvailableBlockPresenter)
    fun inject(changePasswordPresenter: ChangePasswordPresenter)
    fun inject(passwordRecoverPresenter: PasswordRecoverPresenter)
    fun inject(splashPresenter: SplashPresenter)
    fun inject(changePincodePresenter: ChangePincodePresenter)
    fun inject(notificationsPresenter: NotificationsPresenter)
    fun inject(createPincodePresenter: CreatePincodePresenter)
    fun inject(pinPresenter: PinPresenter)
    fun inject(advertisementPresenter: AdvertisementPresenter)
    fun inject(aboutMyClubPresenter: AboutMyClubPresenter)
    fun inject(profileContractFreezePresenter: ProfileContractFreezePresenter)
    fun inject(confirmPhonePresenter: ConfirmPhonePresenter)
    fun inject(externalAppsPresenter: ExternalAppsPresenter)

}