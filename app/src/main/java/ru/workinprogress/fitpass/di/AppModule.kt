package ru.workinprogress.fitpass.di

import android.app.Application
import dagger.Module
import dagger.Provides
import ru.workinprogress.fitpass.api.ApiModule
import ru.workinprogress.fitpass.data.DataModule

@Module(includes = arrayOf(ApiModule::class, DataModule::class, NavigationModule::class))
class AppModule(val app: Application) {

    @ApplicationScope
    @Provides
    fun provideApplication(): Application = app

}