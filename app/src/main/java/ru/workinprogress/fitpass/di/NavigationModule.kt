package ru.workinprogress.fitpass.di

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.workinprogress.fitpass.di.ApplicationScope

/**
 * Created by panic on 06.10.2017.
 */
@Module
class NavigationModule {

    private val cicerone by lazy { Cicerone.create() }

    @Provides
    @ApplicationScope
    fun providesRouter(): Router = cicerone.router

    @Provides
    @ApplicationScope
    fun providesNavigatorHolder(): NavigatorHolder = cicerone.navigatorHolder

//    object AuthNavigation {
//        val cicerone by lazy { Cicerone.create() }
//    }
//
//    object SplashNavigation {
//        val cicerone by lazy { Cicerone.create() }
//    }
}
