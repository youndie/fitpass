package ru.workinprogress.fitpass.di

import ru.workinprogress.fitpass.App.Companion.appComponent
import ru.workinprogress.fitpass.di.AppComponent
import ru.workinprogress.fitpass.presentation.AboutClubPresenter

/**
 * Created by panic on 05.10.2017.
 */

object Injector : AppComponent by appComponent {

}
