package ru.workinprogress.fitpass.utils

import java.util.regex.Pattern
import android.text.TextUtils


/**
 * Created by panic on 22.02.2018.
 */

object Validation {
    fun isEmailValid(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun isValidPhone(phone: String): Boolean {
        val phoneNormalize = phoneNormalize(phone)
        return phoneNormalize.matches("-?\\d+(\\.\\d+)?".toRegex()) && phoneNormalize.length > 9
    }

    fun phoneNormalize(phone: String): String {
        if (phone.startsWith("8") && phone.length == 11) {
            return phone.drop(1)
        }
        if (phone.startsWith("+7") && phone.length == 12) {
            return phone.drop(2)
        }
        return phone
//        return (phone.toIntOrNull() != null && (phone.length == 10 || (phone.startsWith("8") && phone.length == 11))) || ((phone.startsWith("+7") && (phone.replace("+7","").toIntOrNull()!=null) && (phone.replace("+7",""))
    }

    fun isValidPassword(password: String): Boolean {
        var cardinality = 0
        if (password.any { it.isLowerCase() }) cardinality = 26
        if (password.any { it.isUpperCase() }) cardinality += 26
        if (password.any { it.isDigit() }) cardinality += 10
        if (password.indexOfAny("\\|¬¦`!\"£\$%^&*()_+-=[]{};:'@#~<>,./?".toCharArray()) > 0) {
            cardinality += 36
        }


        return ((Math.log(cardinality.toDouble()) / Math.log(2.toDouble())) * password.length) > 42
    }


//    public double CalculateEntropy(string password)
//    {
//        var cardinality = 0;
//
//        // Password contains lowercase letters.
//        if (password.Any(c => char.IsLower(c)))
//        {
//            cardinality = 26;
//        }
//
//        // Password contains uppercase letters.
//        if (password.Any(c => char.IsUpper(c)))
//        {
//            cardinality += 26;
//        }
//
//        // Password contains numbers.
//        if (password.Any(c => char.IsDigit(c)))
//        {
//            cardinality += 10;
//        }
//
//        // Password contains symbols.
//        if (password.IndexOfAny("\\|¬¦`!\"£$%^&*()_+-=[]{};:'@#~<>,./? ".ToCharArray()) >= 0)
//        {
//            cardinality += 36;
//        }
//
//        return Math.Log(cardinality, 2) * password.Length;
//    }
}

