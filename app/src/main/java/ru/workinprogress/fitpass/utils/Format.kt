package ru.workinprogress.fitpass.utils

import android.os.Build
import ru.workinprogress.fitpass.utils.Format.sourceYMDFormat
import ru.workinprogress.fitpass.utils.Format.targetYMDFormat
import ru.workinprogress.fitpass.utils.Format.weekdayYMDFormat
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat

object Format {
    val roublesFormat = DecimalFormat("###,###.### ${if (Build.VERSION.SDK_INT > 21) "\u20BD" else "р"}")
    val sourceYMDFormat = SimpleDateFormat("yyyy-MM-dd")
    val targetYMDFormat = SimpleDateFormat("dd.MM.yyyy")
    val weekdayYMDFormat = SimpleDateFormat("EEEE dd.MM.yyyy")
    val sourceYMDHMSFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val targetYMDHMSFormat = SimpleDateFormat("HH:mm dd.MM.yyyy")
}

fun Int.rouble(): String {
    return Format.roublesFormat.format(this)
}

fun String.toReadableYMDDate(): String {
    return try {
        targetYMDFormat.format(sourceYMDFormat.parse(this))
    } catch (e: ParseException) {
        ""
    }
}


fun String.toWeekdayYMDDate(): String {
    return try {
        weekdayYMDFormat.format(sourceYMDFormat.parse(this))
    } catch (e: ParseException) {
        ""
    }
}