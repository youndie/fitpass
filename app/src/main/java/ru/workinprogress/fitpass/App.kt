package ru.workinprogress.fitpass

import android.app.Application
import ru.workinprogress.fitpass.di.AppComponent
import ru.workinprogress.fitpass.di.AppModule
import ru.workinprogress.fitpass.di.DaggerAppComponent
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins
import java.io.IOException
import java.net.SocketException


/**
 * Created by panic on 07.02.2018.
 */

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()


    }


    companion object {
        @JvmStatic
        lateinit var appComponent: AppComponent

    }

}


