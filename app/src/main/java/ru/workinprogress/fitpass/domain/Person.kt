package ru.workinprogress.fitpass.domain

import android.content.SharedPreferences
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.unbox
import ru.workinprogress.fitpass.di.ApplicationScope
import javax.inject.Inject

/**
 * Created by panic on 06.03.2018.
 */

@ApplicationScope
class PersonInteractor @Inject constructor(private val api: Api, private val sharedPreferences: SharedPreferences) {
    fun reset() {
        sharedPreferences.edit().remove(personTag).apply()
    }


    companion object {
        const val personTag = "person:id"
    }

    var personId
        get() = sharedPreferences.getString(personTag, "")
        set(value) {
            sharedPreferences.edit().putString(personTag, value).apply()
        }


    val person
        get() = api.getPerson().unbox().doOnSuccess {
            personId = it.PersonID
        }
}