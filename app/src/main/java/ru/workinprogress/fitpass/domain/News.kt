package ru.workinprogress.fitpass.domain

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.RegisterImpressionParams
import ru.workinprogress.fitpass.api.serializers.*
import ru.workinprogress.fitpass.di.ApplicationScope
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 06.03.2018.
 */

@ApplicationScope
class NewsInteractor @Inject constructor(val api: Api, val clubInteractor: ClubInteractor) {

    private val cachedNews = mutableListOf<News>()

    val loaded get() = cachedNews.isNotEmpty()

    fun getMainNews(count: Int = 3, offset: Int = 0): Array<News> {
        return cachedNews.toTypedArray()
    }

    fun loadNews(): Single<MutableList<News>> {
        return api.getNews(clubInteractor.selectedClubId.takeIf { it.isNotEmpty() }, cachedNews.size).unbox().map {
            cachedNews.addAll(it)
            cachedNews
        }
    }

    fun loadAds(): Single<Array<Advertisement>> {
        return api.getAds(clubInteractor.selectedClubId.takeIf { it.isNotEmpty() }).unbox()
    }

}

@ApplicationScope
class SpecialOffersInteractor @Inject constructor(val api: Api, val clubInteractor: ClubInteractor) {

    private val specialOffers = mutableListOf<SpecialOffer>()
    private val offersSubject = BehaviorSubject.create<Array<SpecialOffer>>()
    val observeSpecialOffers: Observable<Array<SpecialOffer>> = offersSubject

    fun refresh(): Single<MutableList<SpecialOffer>> {
        specialOffers.clear()
        return loadMore()
    }

    fun loadMore(): Single<MutableList<SpecialOffer>> {
        return api.getSpecialOffers(clubInteractor.selectedClubId.takeIf { it.isNotEmpty() }, specialOffers.size).unbox().map {
            if (it.isNotEmpty()) {
                specialOffers.addAll(it)
                offersSubject.onNext(specialOffers.toTypedArray())
            }
            specialOffers
        }
    }


    fun onOfferAccept(uniqueId: String): Single<ApiResponse<Boolean>> {
        return api.acceptSpecialOffer(RegisterImpressionParams(uniqueId)).doOnSuccess {
            specialOffers.find { it.UniqueID == uniqueId }?.Accepted = true
            offersSubject.onNext(specialOffers.toTypedArray())
        }
    }

    fun onOfferReject(uniqueId: String): Single<ApiResponse<Boolean>> {
        return api.declineSpecialOffer(RegisterImpressionParams(uniqueId)).doOnSuccess {
            specialOffers.removeAll { it.UniqueID == uniqueId }
            offersSubject.onNext(specialOffers.toTypedArray())
        }
    }

}