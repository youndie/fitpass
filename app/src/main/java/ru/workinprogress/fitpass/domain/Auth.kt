package ru.workinprogress.fitpass.domain

import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.AuthService
import ru.workinprogress.fitpass.api.params.LoginParams
import ru.workinprogress.fitpass.api.serializers.Login
import ru.workinprogress.fitpass.api.serializers.unbox
import ru.workinprogress.fitpass.data.API_KEY
import ru.workinprogress.fitpass.di.ApplicationScope
import javax.inject.Inject

@ApplicationScope
class AuthInteractor @Inject constructor(val api: Api, val authService: AuthService) {

    fun resetSession() = authService.save(Login("", ""))
    fun reset() = authService.reset()
    val paramsExists get() = authService.params != null
    fun simpleLogin() = api.login(LoginParams(SessionType = 1, APIKey = API_KEY)).unbox().doOnSuccess { authService.save(it) }
    fun advancedLogin(params: LoginParams = authService.params!!) = api.login(params).unbox().doOnSuccess {
        authService.save(it)
        authService.save(params)
    }

    val pinEnabled: Boolean get() = authService.pin.isNotBlank()

    fun savePin(pincode: String) {
        authService.pin = pincode
    }

    fun checkPin(pincode: String): Boolean {
        return authService.pin == pincode
    }
}