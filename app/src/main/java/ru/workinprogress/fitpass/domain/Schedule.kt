package ru.workinprogress.fitpass.domain

import android.text.format.DateUtils
import io.reactivex.Single
import io.reactivex.functions.Function3
import org.joda.time.DateTime
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.*
import ru.workinprogress.fitpass.di.ApplicationScope
import ru.workinprogress.fitpass.presentation.UILessonDetail
import ru.workinprogress.fitpass.presentation.UIScheduleItem
import ru.workinprogress.fitpass.presentation.UIWeek
import java.text.SimpleDateFormat
import javax.inject.Inject

/**
 * Created by panic on 06.03.2018.
 */

@ApplicationScope
class ScheduleInteractor @Inject constructor(private val api: Api, private val clubInteractor: ClubInteractor, val personInteractor: PersonInteractor) {
    val DATE_FORMATTER = SimpleDateFormat("yyyy-MM-dd")

    fun Lesson.toUIScheduleItem() = UIScheduleItem(id = ID,
            trainer = masters.find { it.ID == MasterID }?.Name.orEmpty(),
            place = rooms.find { it.ID == RoomID }?.Name.orEmpty(),
            startTime = BeginTime,
            title = lessonTypes.find { it.ID == LessonTypeID }?.Name.orEmpty(),
            requirePayment = lessonTypes.find { it.ID == LessonTypeID }?.RequirePayment
                    ?: false)

    private fun timeTableObservable(source: Single<Array<Lesson>>) = source.map {
        return@map if (it.isEmpty()) emptyList() else
            it.groupBy { DateTime(DATE_FORMATTER.parse(it.Date)).withTimeAtStartOfDay() }.map {
                UIWeek(it.key, it.value.map { it.toUIScheduleItem() }.also { it.groupBy { it.startTime }.map { it.value.first() }.forEach { it.showStartTime = true } })
            }.sortedBy { it.dateTime }
    }

    fun loadSchedule(personSchedule: Boolean = false): Single<List<UIWeek>> {
        var obs = if (!personSchedule) api.getClubTimetable(clubInteractor.selectedClubId).unbox() else api.getPersonTimetable(clubInteractor.selectedClubId, personInteractor.personId).unbox()
        obs = obs.doOnSuccess {
            if (it.isNotEmpty()) {
                if (personSchedule) {
                    personLessons.clear()
                    personLessons.addAll(it)
                } else {
                    lessons.clear()
                    lessons.addAll(it)
                }
            }

        }
        if (rooms.isEmpty() || masters.isEmpty() || lessonTypes.isEmpty()) {
            return Single.zip(
                    loadRooms(),
                    loadLessonTypes(),
                    loadMasters(),
                    Function3<Array<Room>, Array<LessonType>, Array<Master>, Boolean> { t1, t2, t3 -> true }).flatMap {
                timeTableObservable(obs)
            }
        }
        return timeTableObservable(obs)
    }

    fun getTodayTrainings(): Single<List<UIScheduleItem>> {
        if (clubInteractor.selectedClubId.isEmpty()) return Single.just(emptyList())
        return api.getPersonTimetable(clubInteractor.selectedClubId, personInteractor.personId).unbox().map {
            it.filter { DateUtils.isToday(DATE_FORMATTER.parse(it.Date).time) }.map { it.toUIScheduleItem() }
        }
    }

    fun getFavTrainings(): Single<List<UIScheduleItem>> {
        if (clubInteractor.selectedClubId.isEmpty()) return Single.just(emptyList())
        return api.getClubTimetable(clubInteractor.selectedClubId).unbox().map {
            it.map { it.toUIScheduleItem() }.subList(0, 3)
        }
    }

    val rooms = mutableListOf<Room>()
    val masters = mutableListOf<Master>()
    val lessonTypes = mutableListOf<LessonType>()
    val lessons = mutableListOf<Lesson>()
    val personLessons = mutableListOf<Lesson>()
    val availableBlocks = mutableListOf<AvailableBlock>()

    fun loadAvailableBlocks(): Single<Array<AvailableBlock>> {
        return api.getAvailableBlocks(clubInteractor.selectedClubId).unbox().doOnSuccess {
            if (it.isNotEmpty()) {
                availableBlocks.clear()
                availableBlocks.addAll(it)
            }
        }
    }

    private fun loadRooms(): Single<Array<Room>> {
        return api.getRooms(clubInteractor.selectedClubId).unbox().doOnSuccess {
            if (it.isNotEmpty()) {
                rooms.clear()
                rooms.addAll(it)
            }
        }
    }

    private fun loadMasters(): Single<Array<Master>> {
        return api.getMasters(clubInteractor.selectedClubId).unbox().doOnSuccess {
            if (it.isNotEmpty()) {
                masters.clear()
                masters.addAll(it)
            }
        }
    }

    private fun loadLessonTypes(): Single<Array<LessonType>> {
        return api.getLessons(clubInteractor.selectedClubId).unbox().doOnSuccess {
            if (it.isNotEmpty()) {
                lessonTypes.clear()
                lessonTypes.addAll(it)
            }
        }
    }

    fun getDetailTraining(id: String, personLesson: Boolean): UILessonDetail {
        return (if (personLesson) personLessons else lessons).find { it.ID == id }?.let { lesson ->
            UILessonDetail(lesson, lessonTypes.find { it.ID == lesson.LessonTypeID }, masters.find { it.ID == lesson.MasterID }, rooms.find { it.ID == lesson.RoomID })
        }!!
    }


}

