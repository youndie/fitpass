package ru.workinprogress.fitpass.domain

import io.reactivex.Observable
import io.reactivex.Single
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.PaymentCard
import ru.workinprogress.fitpass.api.serializers.unbox
import ru.workinprogress.fitpass.di.ApplicationScope
import javax.inject.Inject

/**
 * Created by panic on 21.03.2018.
 */
@ApplicationScope
class CardInteractor @Inject constructor(val api: Api) {

    val cached = mutableListOf<PaymentCard>()

    @Inject
    lateinit var personInteractor: PersonInteractor

    @Inject
    lateinit var clubInteractor: ClubInteractor

    fun load(): Single<Array<PaymentCard>> {
        if (clubInteractor.selectedClubId.isEmpty()) return Single.just(emptyArray())
        return api.getPaymentCards(clubInteractor.selectedClubId).unbox().doOnSuccess {
            cached.clear()
            cached.addAll(it)
        }
    }

    fun deleteCard(card: PaymentCard) {
        //todo delete
    }

    fun cardsByLegalBankInfo(legalBankInfo: String?): Observable<CardsInfo> {
        return clubInteractor.selectedClub.map { club ->
            CardsInfo(
                    list = cached.filter { card ->
                        card.LegalBankInfo == legalBankInfo || (legalBankInfo == null && (club.LegalEntities.any { it.IsDefaultForAccountPayment }))
                    }, newCard = (legalBankInfo != null || (club.LegalEntities.any { it.IsDefaultForAccountPayment })))
        }

    }

    fun reset() {
        cached.clear()
    }

}

class CardsInfo(val list: List<PaymentCard>, val newCard: Boolean)