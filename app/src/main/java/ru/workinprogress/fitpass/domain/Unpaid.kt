package ru.workinprogress.fitpass.domain

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.UnpaidService
import ru.workinprogress.fitpass.api.serializers.unbox
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.ApplicationScope
import javax.inject.Inject

/**
 * Created by panic on 23.03.2018.
 */

@ApplicationScope
class UnpaidInteractor @Inject constructor(private val api: Api) {

    @Inject
    lateinit var clubInteractor: ClubInteractor


    private val subject = BehaviorSubject.create<Array<UnpaidService>>()
    val updateEventSubject = BehaviorSubject.create<Boolean>()
    val observeUnpaidServices: Observable<Array<UnpaidService>> = subject

    fun load(): Single<Array<UnpaidService>> {
        updateEventSubject.onNext(true)
        return api.getUnpaidServices(clubInteractor.selectedClubId).unbox().map {
            it.map {
                UnpaidService(it.ID, it.Amount, it.BonusAmount, it.ItemID, it.LegalBankInfo, it.Qty, it.Price, it.Code, it.Name, it.SaleDate, it.CanBePaid, it.BonusPrice, it.ScopeID.orEmpty(), it.ScopeName.orEmpty())
            }.toTypedArray()
        }.doOnSuccess {
            updateEventSubject.onNext(false)
            subject.onNext(it)
        }
    }


}