package ru.workinprogress.fitpass.domain

import android.content.SharedPreferences
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.Account
import ru.workinprogress.fitpass.api.serializers.Club
import ru.workinprogress.fitpass.api.serializers.unbox
import ru.workinprogress.fitpass.di.ApplicationScope
import javax.inject.Inject

@ApplicationScope
class AccountsInteractor @Inject constructor(private val api: Api) {

    @Inject
    lateinit var clubInteractor: ClubInteractor

    fun update(): Single<Array<Account>> {
        return api.getAccounts(clubInteractor.selectedClubId).unbox().doOnSuccess {
            accountsSubject.onNext(it)
        }
    }
    private val accountsSubject by lazy {
        BehaviorSubject.create<Array<Account>>()
    }

    val observeAccount: Observable<Array<Account>> get() = accountsSubject


}