package ru.workinprogress.fitpass.domain

import android.app.Application
import android.content.Intent
import com.google.gson.Gson
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.newTask
import retrofit2.HttpException
import ru.workinprogress.fitpass.api.serializers.ApiError
import ru.workinprogress.fitpass.api.serializers.ApiResponseError
import ru.workinprogress.fitpass.di.ApplicationScope
import ru.workinprogress.fitpass.ui.activity.SplashActivity
import javax.inject.Inject


@ApplicationScope
class ErrorInteractor @Inject constructor(val app: Application, val gson: Gson) {

    fun toApiError(throwable: Throwable): ApiError? {
        val error = (throwable as? HttpException)?.response()?.errorBody()?.string()?.let {
            gson.fromJson<ApiResponseError>(it, ApiResponseError::class.java)
        }?.Error

//        if (throwable is HttpException) {
//            if (throwable.code() == 403 && error?.ErrorCode == "INVALID_AUTHENTICATION_TOKEN") {
//                app.startActivity(Intent(app, SplashActivity::class.java).newTask().clearTop())
//            }
//        }
        return error
    }

}
