package ru.workinprogress.fitpass.domain

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import ru.workinprogress.fitpass.di.ApplicationScope
import javax.inject.Inject

/**
 * Created by panic on 19.03.2018.
 */

@ApplicationScope
class ToolbarInteractor @Inject constructor() {

    private val subject = BehaviorSubject.create<String>()
    private val toolbarShowing = BehaviorSubject.create<Boolean>()
    private val toolbarAlpha = BehaviorSubject.create<Boolean>()
    fun observeTitle(): Observable<String> = subject
    fun observeShowing(): Observable<Boolean> = toolbarShowing
    fun observeAlpha(): Observable<Boolean> = toolbarAlpha
    var showing: Boolean
        get() = toolbarShowing.value
        set(value) {
            toolbarShowing.onNext(value)
        }

    var title: String
        get() = subject.value
        set(value) {
            subject.onNext(value)
        }

    var alpha: Boolean
        get() = toolbarAlpha.value
        set(value) {
            toolbarAlpha.onNext(value)
        }

}