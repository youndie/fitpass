package ru.workinprogress.fitpass.domain

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.SuspendParams
import ru.workinprogress.fitpass.api.serializers.ContractStatus
import ru.workinprogress.fitpass.api.serializers.unbox
import ru.workinprogress.fitpass.di.ApplicationScope
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 06.03.2018.
 */

@ApplicationScope
class ContractsInteractor @Inject constructor(val clubInteractor: ClubInteractor, val api: Api) {

    @Inject
    lateinit var personInteractor: PersonInteractor


    private val contractsSubject by lazy { BehaviorSubject.create<Array<ContractStatus>>() }

    val observeContracts: Observable<Array<ContractStatus>> get() = contractsSubject

    fun update(): Single<Array<ContractStatus>> {
        if (clubInteractor.selectedClubId.isEmpty()) return Single.just(emptyArray())
        return api.getContracts(clubInteractor.selectedClubId).unbox().doOnSuccess {
            contractsSubject.onNext(it)
        }
    }

    fun suspend(id: String, begin: String, end: String): Single<ContractStatus> {
        return api.suspendContract(id, clubInteractor.selectedClubId, personInteractor.personId, SuspendParams(begin, end)).unbox()
    }

}