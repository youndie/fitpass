package ru.workinprogress.fitpass.domain

import android.content.SharedPreferences
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.Club
import ru.workinprogress.fitpass.api.serializers.unbox
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.ApplicationScope
import java.util.*
import javax.inject.Inject


/**
 * Created by panic on 06.03.2018.
 */

@ApplicationScope
class ClubInteractor @Inject constructor(private val api: Api, private val sharedPreferences: SharedPreferences) {

    companion object {
        const val selectedClubTag = "selected:club:id"
        const val linkCodeTag = "link:code:tag"
        const val linkCreatedTag = "link:code:created:tag"
    }

    fun update(): Single<Array<Club>> {
        return api.getUserClubs().unbox().doOnSuccess {
                clubsSubject.onNext(it)
        }
    }

    var selectedClubId: String
        get() = sharedPreferences.getString(selectedClubTag, "")
        set(value) {
            sharedPreferences.edit().putString(selectedClubTag, value).apply()
            selectedClubIdSubject.onNext(value)
        }

    private val selectedClubIdSubject by lazy {
        BehaviorSubject.create<String>().apply { onNext(selectedClubId) }
    }
    private val clubsSubject by lazy {
        BehaviorSubject.create<Array<Club>>()
    }

    val observeSelectedClubId: Observable<String> get() = selectedClubIdSubject.distinctUntilChanged()
    val observeClubs: Observable<Array<Club>> get() = clubsSubject
    val selectedClub get() = clubsSubject.map { it.firstOrNull { it.ID == selectedClubId } }

    fun requestLinkCode(): Single<String> {
        return api.requestLinkingCode().unbox().doOnSuccess {
            saveLinkCode(it)
        }
    }

    private fun saveLinkCode(code: String) {
        sharedPreferences.edit().putString(linkCodeTag, code).putLong(linkCreatedTag, Date().time).apply()
    }

    fun reset() {
        sharedPreferences.edit().remove(selectedClubTag).remove(linkCreatedTag).remove(linkCodeTag).apply()
    }

    val linkCode: String
        get() {
            val mins = (Date().time - sharedPreferences.getLong(linkCreatedTag, Date().time)) / (1000 * 60).toFloat()
            if (mins in 0..29) {
                return sharedPreferences.getString(linkCodeTag, "")
            }
            return ""
        }

}