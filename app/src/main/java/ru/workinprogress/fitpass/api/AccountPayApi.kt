package ru.workinprogress.fitpass.api

import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query
import ru.workinprogress.fitpass.api.params.PayParams
import ru.workinprogress.fitpass.api.params.QueryPaymentParams
import ru.workinprogress.fitpass.api.serializers.ApiSingle
import ru.workinprogress.fitpass.api.serializers.CardPaymentResponse
import ru.workinprogress.fitpass.api.serializers.QueryPaymentResponse

interface AccountPayApi {
    @POST("account/queryPayment")
    fun queryAccountPayment(@Query("club") clubId: String, @Query("account") accountId: String, @Body queryPaymentParams: QueryPaymentParams): ApiSingle<QueryPaymentResponse>

    @POST("account/paymentCard")
    fun cardAccountPayment(@Query("club") clubId: String, @Query("account") accountId: String, @Query("cardID") cardID: String, @Body payParams: PayParams): ApiSingle<CardPaymentResponse>
}