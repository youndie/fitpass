package ru.workinprogress.fitpass.api

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.workinprogress.fitpass.data.API_URL
import ru.workinprogress.fitpass.di.ApplicationScope

@Module
class ApiModule {

    @Provides
    @ApplicationScope
    fun providesApi(retrofit: Retrofit): Api =
            retrofit.create(Api::class.java)

    @Provides
    @ApplicationScope
    fun providesAccountPayApi(okHttpClient: OkHttpClient): AccountPayApi =
            Retrofit.Builder()
                    .client(okHttpClient)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder().serializeNulls().create())).baseUrl(API_URL).build().create(AccountPayApi::class.java)

    @Provides
    @ApplicationScope
    fun provideRetrofit(retrofitBuilder: Retrofit.Builder): Retrofit = retrofitBuilder.baseUrl(API_URL).build()

    @Provides
    @ApplicationScope
    fun providesAuthService(sharedPreferences: SharedPreferences): AuthService = AuthService(sharedPreferences)

}