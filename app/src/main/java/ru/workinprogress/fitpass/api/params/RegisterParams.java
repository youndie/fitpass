package ru.workinprogress.fitpass.api.params;

/**
 * Created by panic on 22.02.2018.
 */

public class RegisterParams {
    public String Login;
    public String NickName;
    public String DateOfBirth;
    public String MainPhone;
    public String MainEmail;

    public RegisterParams(String login, String nickName,String dateOfBirth, String mainPhone, String mainEmail, String password, String gender, String residenceCountryCode, int residenceCityID, String languageCode, String phoneConfirmationCode, boolean offerAccepted, boolean processingOfPersonalDataAllowed) {
        Login = login;
        NickName = nickName;
        DateOfBirth = dateOfBirth;
        MainPhone = mainPhone;
        MainEmail = mainEmail;
        Password = password;
        Gender = gender;
        ResidenceCountryCode = residenceCountryCode;
        ResidenceCityID = residenceCityID;
        LanguageCode = languageCode;
        PhoneConfirmationCode = phoneConfirmationCode;
        OfferAccepted = offerAccepted;
        ProcessingOfPersonalDataAllowed = processingOfPersonalDataAllowed;
    }

    public String Password;
    public String Gender;
    public String ResidenceCountryCode;
    public int ResidenceCityID;
    public String LanguageCode;
    public String PhoneConfirmationCode;
    public boolean OfferAccepted;
    public boolean ProcessingOfPersonalDataAllowed;


}
