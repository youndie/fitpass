package ru.workinprogress.fitpass.api.params

import ru.workinprogress.fitpass.data.API_KEY
import java.io.Serializable

/**
 * Created by panic on 16.03.2018.
 */

class RegisterImpressionParams(val ID: String)

class LoginParams(val SessionType: Int, val APIKey: String = API_KEY, val Login: String? = null, val Phone: String? = null, val Email: String? = null, val Password: String? = null, val SingleUsePassword: String? = null ) : Serializable
class SaleParams(val ItemID: String, val Qty: Int, val Price: Int)
class QueryPaymentParams(val Amount: Float, val LegalBankInfo: String?, val SendInvitation: Boolean = false)
class BuyContractParams(val TempatePriceID: String)
class PayParams(val Amount: Float, val LegalBankInfo: String?)
class JoinParams(val ItemID: String? = null, val BlockID: String? = null)
class EditPersonParams(val LanguageCode: String, val MainEmail: String, val ResidenceCityID: Int, val ResidenceCountryCode: String, val Gender: Int? = null, val DateOfBirth: String? = null)
class ConfirmationParams(val PhoneConfirmationCode: String)
class ChangePasswordParams(val Login: String? = null, val Phone: String? = null, val Email: String? = null, val Code: String? = null, val OldPassword: String? = null, val NewPassword: String)
class PasswordCodeParams(val Login: String? = null, val Phone: String? = null, val Email: String? = null)
class ConditionParams(val OfferAccepted: Boolean, val ProcessingOfPersonalDataAllowed: Boolean)
class CheckAttributesParams(val Login: String? = null, val NickName: String? = null, val MainPhone: String? = null, val MainEmail: String? = null, val DateOfBirth: String? = null)
class SuspendParams(val BeginDate: String, val EndDate: String)
class AccountRemovalParams(val Phone: String)
class PhoneConfirmationParams(val Phone: String)
class EmailConfirmationParams(val Login: String, val Email: String)
class PhoneConfirmParams(val Phone: String, val Login: String, val Code: String)
