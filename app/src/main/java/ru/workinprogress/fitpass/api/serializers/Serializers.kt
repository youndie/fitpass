package ru.workinprogress.fitpass.api.serializers

import android.content.res.Resources
import io.reactivex.Single
import retrofit2.HttpException
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.UIScheduleItem
import ru.workinprogress.fitpass.ui.activity.async
import java.io.Serializable
import java.math.BigDecimal

/**
 * Created by panic on 22.02.2018.
 */
class Login(val Scheme: String, val Token: String) : Serializable

class Person(var PersonID: String, var DateOfBirth: String, var Gender: Int, var Language: Language, var MainEmail: String, var MainEmailConfirmed: Boolean, var MainPhone: String, var MainPhoneConfirmed: Boolean, var NickName: String, var FirstName: String, var Patronymic: String, var LastName: String, var Login: String, var ResidenceCity: City, var ResidenceCountry: Country, var OfferAccepted: Boolean, var ProcessingOfPersonalDataAllowed: Boolean) : Serializable
class SendedConfirmation(var MessageSent: Boolean, var CodeValidBeforeTime: String, var CodeValidityTimeSec: Int, var NextAttemptMinTime: String) : Serializable
class Country(val Code: String, val DefaultLanguageCode: String, val Name: String) : Serializable {
    override fun toString(): String {
        return Name
    }
}

class City(val CountryCode: String, val ID: Int, val Name: String) : Serializable {
    override fun toString(): String {
        return Name
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as City

        if (ID != other.ID) return false
        if (Name != other.Name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = ID
        result = 31 * result + Name.hashCode()
        return result
    }
}

class Language(val Code: String, val Name: String) : Serializable {
    override fun toString(): String {
        return Name
    }
}

class PersonStatus(val PersonID: String, val Status: Int) : Serializable
class Club(val ID: String, val Code: String, val Name: String, val Phone: String, val Email: String, val Address: String, val Lat: Double, val Lon: Double, val City: City, val Country: Country, val LegalEntities: Array<LegalEntity>, val Media: Array<Media>, val ExternalSystemURIs: Array<ExternalSystemURI>, var OfferAccepted: Boolean, var ProcessingOfPersonalDataAllowed: Boolean, var PersonalDataProcessingAgreementPDF: String, var OfferPDF: String, var Description: String, var Site: String) : Serializable {
    override fun toString(): String {
        return Name
    }
}

val Array<Media>.icon
    get() =
        firstOrNull { it.LayoutName == "Icon" }?.Uri

val Array<Media>.image
    get() =
        firstOrNull { it.LayoutName != "Icon" }?.Uri

val Club.logo get() = Media.firstOrNull { it.LayoutName == "Logo" }
val Club.background get() = Media.firstOrNull { it.LayoutName == "Background_Square" }
val Club.icon get() = Media.firstOrNull { it.LayoutName == "Icon" }

val Room.photos get() = Media.filter { it.LayoutName == "Photos" }
val Club.photos get() = Media
val clubPhotosLayoutNames = arrayOf("Logo", "Location", "Interior", "Equipement", "Staff")
val Club.aboutPhotos
    get() = Media.filter { clubPhotosLayoutNames.contains(it.LayoutName) || it.LayoutName.startsWith("Scope") }.sortedBy {
        if (clubPhotosLayoutNames.contains(it.LayoutName)) {
            clubPhotosLayoutNames.indexOf(it.LayoutName)
        } else {
            clubPhotosLayoutNames.size + 1
        }
    }
val Club.contractsImage get() = Media.firstOrNull { it.LayoutName == "FPAndroid_Contracts" }
val Club.blocksImage get() = Media.firstOrNull { it.LayoutName == "FPAndroid_Blocks" }
val Club.servicesImage get() = Media.firstOrNull { it.LayoutName == "FPAndroid_Services" }
val Master.avatar get() = Media.firstOrNull { it.LayoutName == "Icon" }?.Uri

class ExpectedPayment(val Amount: BigDecimal, val DateDue: String, val LegalBankInfo: String, val PaymentID: String) : Serializable

class Account(val AccountID: String, val Amount: Int, val LegalBankInfo: String, val ScopeID: String?, val ScopeName: String) : Serializable
class ContractStatus(val CanExtend: Boolean, val CanSuspend: Boolean, val CanSuspendDays: Int, val Contract: Contract, val DateBegin: String, val DateEnd: String, val ExpectedPayments: Array<ExpectedPayment>, val ID: String, val Interval: String, val Status: String, val StatusCode: Int, val SuspendInfo: SuspendInfo?, val Template: String) : Serializable {
    companion object {
        const val openStatusCode = 1
        const val freezeStatusCode = 5
    }

    val IntervalName: String get() = Interval.filter { it.isLetter() }
    val IntervalNumber: Int get() = Interval.filter { it.isDigit() }.toInt()

    fun interval(resources: Resources): String {
        return resources.getQuantityString(when (IntervalName) {
            "Y" -> R.plurals.years
            "M" -> R.plurals.months
            "Q" -> R.plurals.quarters
            "W" -> R.plurals.weeks
            else -> {
                R.plurals.days
            }
        }, IntervalNumber, IntervalNumber)
    }

}

class Visit(val ID: String, val StartTime: String, val EndTime: String, val UsedKeys: Array<String>?) : Serializable
class SuspendInfo(val BeginDate: String, val EndDate: String) : Serializable
class Contract(val DateEstablished: String, val ID: String, val Number: String) : Serializable
class LegalEntity(val BankInfo: String?, val ID: String, val IsDefaultForAccountPayment: Boolean) : Serializable
class Media(val ID: String, val LayoutName: String, val Priority: Int, val Uri: String) : Serializable
class ExternalSystemURI(val DisplayName: String, val Name: String, val PlatformID: Int, val URI: String) : Serializable
class HistoryPayment(val ID: String, val Amount: Float, val Date: String) : Serializable
class SaleItem(val ID: String, val Date: String, val Details: Array<Product>) : Serializable
class Product(val ID: String, val Name: String, val Quantity: Int, val Price: Float, val Amount: Float) : Serializable
class PricelistItem(val Code: String, val ID: String, val ItemType: Int, val Name: String, val Price: Int, val ValidDays: Int?, val AvailableLessonTypeIDs: Array<String>, val PriceListSectionCode: String, val PriceListSectionName: String) : Serializable
class AvailableBlock(val ID: String, val Name: String, val ValidBefore: String?, val SeancesTotal: Int, val SeancesLeft: Int, val PriceListItemID: String, val AvailableLessonTypeIDs: Array<String>, val DateOfCreation: String) : Serializable
class UnpaidService(val ID: String, val Amount: Int, val BonusAmount: Int, val ItemID: String, val LegalBankInfo: String, val Qty: String, val Price: Int, val Code: String, val Name: String, val SaleDate: String, val CanBePaid: Boolean, val BonusPrice: String?, val ScopeID: String?, val ScopeName: String?) : Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UnpaidService

        if (ID != other.ID) return false

        return true
    }

    override fun hashCode(): Int {
        return ID.hashCode()
    }
}

class Lesson(val ID: String, val LessonTypeID: String, val Date: String, val BeginTime: String, val DurationMinutes: Int, val MasterID: String, val RoomID: String, val Capacity: Int, val Color: String) : Serializable
class Room(val ID: String, val Name: String, val Description: String, val Media: Array<ClubMedia>) : Serializable
class ClubMedia(val LayoutName: String, val Uri: String) : Serializable
class Master(val ID: String, val Name: String, val Description: String, val Media: Array<Media>) : Serializable
class LessonType(val ID: String, val Name: String, val Description: String, val Group: String, val RequirePayment: Boolean, val CategoryID: String, val CategoryName: String, val DifficultyCategory: DifficultyCategory, val Media: Array<Media>) : Serializable
class DifficultyCategory(val ID: String, val MaxDifficultyLevel: Int, val MinDifficultyLevel: Int, val Name: String) : Serializable
class LessonRemainingCapacity(val ID: String, val RemainingCapacity: Int) : Serializable
class ApiResponse<out T>(val ResponseDate: String, val Data: T) : Serializable
class ApiError(val ClientMessage: String, val ErrorCode: String) : Serializable
class ApiResponseError(val Error: ApiError) : Serializable
class BonusBalance(val Value: Int) : Serializable
class AdvertiserType(val FPTypeID: String?, val ID: String, val Name: String) : Serializable
class Advertiser(val Enabled: Boolean, val ID: String, val LinkedObjectID: String, val Name: String, val TypeID: String) : Serializable
class Link(val Caption: String, val Media: Array<Media>, val Name: String, val Uri: String) : Serializable
open class SpecialOffer(val UniqueID: String, val ID: String, val Header: String, val Body: String, val CountPerDay: Int, val CountTotal: Int, val StartDate: String, val EndDate: String, val LanguageCode: String, val Links: Array<Link>, val Media: Array<NewsMedia>, val SourceID: String, var Accepted: Boolean?) : Serializable
class Advertisement(UniqueID: String, ID: String, Header: String, Body: String, CountPerDay: Int, CountTotal: Int, StartDate: String, EndDate: String, LanguageCode: String, Links: Array<Link>, Media: Array<NewsMedia>, SourceID: String, Accepted: Boolean?) : SpecialOffer(UniqueID, ID, Header, Body, CountPerDay, CountTotal, StartDate, EndDate, LanguageCode, Links, Media, SourceID, Accepted)
class News(UniqueID: String, ID: String, Header: String, Body: String, CountPerDay: Int, CountTotal: Int, StartDate: String, EndDate: String, LanguageCode: String, Links: Array<Link>, Media: Array<NewsMedia>, SourceID: String, Accepted: Boolean?, val Date: String) : SpecialOffer(UniqueID, ID, Header, Body, CountPerDay, CountTotal, StartDate, EndDate, LanguageCode, Links, Media, SourceID, Accepted)
class NewsMedia(val ID: String, val LayoutName: String, val Uri: String) : Serializable
class ContractPrice(val ID: String, val Name: String, val Value: Float, val AllowIndependentSale: Boolean, val AllowExtentSale: Boolean) : Serializable
class ContractPriceItem(val ID: String, val GroupCode: String, val GroupName: String, val Name: String, val IntervalName: String, val IntervalNumber: Int, val Prices: Array<ContractPrice>) : Serializable {
    fun interval(resources: Resources): String {
        return resources.getQuantityString(when (IntervalName) {
            "Y" -> R.plurals.years
            "M" -> R.plurals.months
            "Q" -> R.plurals.quarters
            "W" -> R.plurals.weeks
            else -> {
                R.plurals.days
            }
        }, IntervalNumber, IntervalNumber)
    }
}

class QueryPaymentResponse(val DocumentID: String, val DocumentNumber: String, val ExpireDate: String, val Data: Map<String, String>, val PaymentProcessorID: String) : Serializable
class PaymentCard(val ID: String, val Name: String, val ExpireDate: String, val LegalBankInfo: String?) : Serializable
class CardPaymentResponse(val Success: Boolean, val Message: String, val PaymentProcessorID: String, val ID: String) : Serializable
class Reservation(val SeanceID: String, val ParticipationID: String, val PaymentDue: String) : Serializable
class BlockSeance(val ID: String) : Serializable
class JoinResponse(val Reservation: Reservation, val Sale: UnpaidService, val BlockSeance: BlockSeance) : Serializable
class Operation(val ID: String, val Amount: Float, val Date: String) : Serializable
class BonusHowToResponse(val HowToEarn: String?, val HowToSpend: String?) : Serializable
class BonusDocument(val ID: String, val Date: String, val Amount: Int, val Description: String) : Serializable
class LayoutItem(val LayoutName: String, val DisplayName: String, val ObjectType: String, val MaxHeight: Int, val MaxWidth: Int, val MinHeight: Int, val MinWidth: Int, val MaxNumberOfImages: Int) : Serializable
class LegacyDocuments(val OfferURL: String, val PersonalDataProcessingAgreementURL: String) : Serializable
class CheckAttributesResponse(val LoginNotUnique: Boolean, val PhoneNotUnique: Boolean, val SuggestedLogins: Array<String>?)

fun <T> Single<ApiResponse<T>>.unbox() = map { it.Data }
fun <T> Single<ApiResponse<T>>.unboxAsync() = unbox().async()

typealias ApiSingle<T> = Single<ApiResponse<T>>
