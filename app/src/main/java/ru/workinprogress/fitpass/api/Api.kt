package ru.workinprogress.fitpass.api

import io.reactivex.Completable
import retrofit2.http.*
import ru.workinprogress.fitpass.api.params.*
import ru.workinprogress.fitpass.api.serializers.*

/**
 * Created by panic on 05.10.2017.
 */
interface Api {

    @POST("Login")
    fun login(@Body loginParams: LoginParams): ApiSingle<Login>

    @POST("person/register")
    fun register(@Body registerParams: RegisterParams): ApiSingle<Person>

    @POST("phone/sendConfirmation")
    fun sendConfirmation(@Body sendConfirmationParams: SendConfirmationParams): ApiSingle<SendedConfirmation>

    @POST("person/sendSingleUsePassword")
    fun sendSingleUserPassword(@Body sendConfirmationParams: SendConfirmationParams): ApiSingle<SendedConfirmation>

    @GET("directory/countries")
    fun getCountries(): ApiSingle<Array<Country>>

    @GET("directory/cities")
    fun getCities(@Query("country") countryCode: String): ApiSingle<Array<City>>

    @GET("directory/languages")
    fun getLanguages(): ApiSingle<Array<Language>>

    @GET("person")
    fun getPerson(@Query("person") personId: String? = null): ApiSingle<Person>

    @GET("person/FitPassLegalDocuments")
    fun getLegalDocuments(): ApiSingle<LegacyDocuments>

    @POST("AcceptConditions")
    fun acceptConditions(@Body conditionParams: ConditionParams): ApiSingle<ConditionParams>

    @POST("person")
    fun editPerson(@Body editPersonParams: EditPersonParams): ApiSingle<Person>

    @GET("person/extendedInfo")
    fun getClientStatus(): ApiSingle<PersonStatus>

    @GET("account/documents")
    fun getOperations(@Query("club") clubId: String): ApiSingle<Array<Operation>>

    @GET("account/bonus")
    fun getBonusBalance(@Query("club") clubId: String, @Query("personID") personId: String): ApiSingle<BonusBalance>

    @GET("account/bonus/documents")
    fun getBonusDocuments(@Query("club") clubId: String): ApiSingle<Array<BonusDocument>>

    @GET("facility/linkedFacilities")
    fun getUserClubs(): ApiSingle<Array<Club>>

    @GET("account")
    fun getAccounts(@Query("club") clubId: String): ApiSingle<Array<Account>>

    @POST("account/queryPayment")
    fun queryAccountPayment(@Query("club") clubId: String, @Query("account") accountId: String, @Body queryPaymentParams: QueryPaymentParams): ApiSingle<QueryPaymentResponse>

    @POST("account/paymentCard")
    fun cardAccountPayment(@Query("club") clubId: String, @Query("account") accountId: String, @Query("cardID") cardID: String, @Body payParams: PayParams): ApiSingle<CardPaymentResponse>

    @GET("contract")
    fun getContracts(@Query("club") clubId: String): ApiSingle<Array<ContractStatus>>

    @POST("contract/suspend")
    fun suspendContract(@Query("id") contractId: String, @Query("club") clubId: String, @Query("person") personID: String, @Body suspendParams: SuspendParams): ApiSingle<ContractStatus>

    @POST("contract/resume")
    fun resumeContract(@Query("id") contractId: String, @Query("club") clubId: String, @Query("person") personID: String): ApiSingle<ContractStatus>

    @GET("visits")
    fun getVisits(@Query("club") clubId: String): ApiSingle<Array<Visit>>

    @GET("sale/documents")
    fun getSales(@Query("club") clubId: String): ApiSingle<Array<SaleItem>>

    @GET("lesson/timetable")
    fun getClubTimetable(@Query("club") clubId: String): ApiSingle<Array<Lesson>>

    @GET("schedule/timetable")
    fun getPersonTimetable(@Query("club") clubId: String, @Query("personID") personId: String): ApiSingle<Array<Lesson>>

    @GET("lesson/availability")
    fun getLessonAvailability(@Query("club") clubId: String, @Query("lessonID") lessonId: String): ApiSingle<Array<LessonRemainingCapacity>>

    @GET("lesson/rooms")
    fun getRooms(@Query("club") clubId: String): ApiSingle<Array<Room>>

    @GET("lesson/masters")
    fun getMasters(@Query("club") clubId: String): ApiSingle<Array<Master>>

    @GET("lesson/lessons")
    fun getLessons(@Query("club") clubId: String): ApiSingle<Array<LessonType>>

    @GET("sale/priceList")
    fun getPricelist(@Query("club") clubId: String): ApiSingle<Array<PricelistItem>>

    @GET("contract/priceList")
    fun getContractPricelist(@Query("club") clubId: String): ApiSingle<Array<ContractPriceItem>>

    @POST("contract/queryPayment")
    fun queryPayment(@Query("contract") contractId: String, @Query("club") clubId: String, @Body queryPaymentParams: QueryPaymentParams): ApiSingle<QueryPaymentResponse>

    @GET("sale/availableBlocks")
    fun getAvailableBlocks(@Query("club") clubId: String): ApiSingle<Array<AvailableBlock>>

    @GET("sale/unpaid")
    fun getUnpaidServices(@Query("club") clubId: String): ApiSingle<Array<UnpaidService>>

    @POST("sale/queryPayment")
    fun querySalePayment(@Query("club") clubId: String, @Query("sale") sales: String, @Body queryPaymentParams: QueryPaymentParams): ApiSingle<QueryPaymentResponse>

    @POST("sale/paymentCard")
    fun paySaleItems(@Query("club") clubId: String, @Query("sale") sales: String, @Query("cardID") cardID: String, @Body payParams: PayParams): ApiSingle<CardPaymentResponse>

    @POST("sale/payFromDeposit")
    fun paySaleItemsFromAccount(@Query("club") clubId: String, @Query("sale") sales: String, @Query("account") accountId: String): ApiSingle<Boolean>

    @POST("facility/createFacilityLinkingCode")
    fun requestLinkingCode(): ApiSingle<String>

    @Deprecated("")
    @POST("person/createLinkToFacility")
    fun createLink(@Query("club") clubId: String): Completable

    @GET("ANA/AdvertiserTypes")
    fun getAdvTypes(): ApiSingle<Array<AdvertiserType>>

    @GET("ANA/Advertisers")
    fun getAdvertisers(): ApiSingle<Array<Advertiser>>

    @POST("phone/sendConfirmation")
    fun sendPhoneConfirmation(@Body phoneConfirmationParams: PhoneConfirmationParams): ApiSingle<SendedConfirmation>

    @POST("phone/confirm")
    fun phoneConfirm(@Body phoneConfirmParams: PhoneConfirmParams): Completable

    @POST("email/sendConfirmation")
    fun sendEmailConfirmation(@Body emailConfirmationParams: EmailConfirmationParams): ApiSingle<SendedConfirmation>

    @GET("News")
    fun getNews(@Query("club") clubId: String? = null, @Query("offset") offset: Int, @Query("limit") limit: Int = 20): ApiSingle<Array<News>>

    @GET("SpecialOffers")
    fun getSpecialOffers(@Query("club") clubId: String? = null, @Query("offset") offset: Int, @Query("limit") limit: Int = 20): ApiSingle<Array<SpecialOffer>>

    @POST("News/RegisterImpression")
    fun registerNewsImpression(@Body registerImpressionParams: RegisterImpressionParams): ApiSingle<News>

    @POST("SpecialOffers/RegisterImpression")
    fun registerSpecialOfferImpression(@Body registerImpressionParams: RegisterImpressionParams): ApiSingle<SpecialOffer>

    @POST("Advertisements/RegisterImpression")
    fun registerAdvImpression(@Body registerImpressionParams: RegisterImpressionParams): ApiSingle<SpecialOffer>

    @POST("SpecialOffers/Decline")
    fun declineSpecialOffer(@Body registerImpressionParams: RegisterImpressionParams): ApiSingle<Boolean>

    @POST("SpecialOffers/Accept")
    fun acceptSpecialOffer(@Body registerImpressionParams: RegisterImpressionParams): ApiSingle<Boolean>

    @GET("Advertisements")
    fun getAds(@Query("club") clubId: String? = null): ApiSingle<Array<Advertisement>>

    @POST("sale/new")
    fun saleNew(@Query("queryID") queryID: String, @Query("person") personId: String, @Query("club") clubId: String, @Body saleParams: SaleParams): ApiSingle<UnpaidService>

    @POST("contract/new")
    fun buyContract(@Query("queryID") queryID: String, @Query("person") personId: String, @Query("club") clubId: String, @Body saleParams: BuyContractParams): ApiSingle<ContractStatus>

    @POST("contract/paymentCard")
    fun payContract(@Query("contract") contractId: String, @Query("club") clubId: String, @Query("cardID") cardID: String, @Body contractPayParams: PayParams): ApiSingle<CardPaymentResponse>

    @GET("person/paymentCards")
    fun getPaymentCards(@Query("club") clubId: String): ApiSingle<Array<PaymentCard>>

    @POST("reservation/join")
    fun reservationJoin(@Query("club") clubId: String, @Query("seance") lessonId: String, @Body joinParams: JoinParams): ApiSingle<JoinResponse>

    @POST("phone/sendRemovalConfirmation")
    fun sendRemovalConfirmation(@Body removalParams: AccountRemovalParams): ApiSingle<SendedConfirmation>

    @POST("person/delete")
    fun personDelete(@Body confirmationParams: ConfirmationParams): ApiSingle<Boolean>

    @POST("person/changePassword")
    fun changePassword(@Body changePasswordParams: ChangePasswordParams): ApiSingle<Boolean>

    @POST("person/sendPasswordChangeCode2")
    fun requestPasswordChangeCode(@Body params: PasswordCodeParams): ApiSingle<SendedConfirmation>

    @GET("account/bonus/howto")
    fun bonusHowTo(@Query("club") clubId: String): ApiSingle<BonusHowToResponse>

    @GET("directory/LayoutItems")
    fun getLayoutItems(@Query("objectTypeID") objectTypeID: String): ApiSingle<Array<LayoutItem>>

    @POST("person/register/checkAttributes")
    fun checkAttributes(@Body checkAttributesParams: CheckAttributesParams): ApiSingle<CheckAttributesResponse>

}