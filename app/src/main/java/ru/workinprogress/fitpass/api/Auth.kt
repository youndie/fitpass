package ru.workinprogress.fitpass.api

import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import ru.workinprogress.fitpass.api.params.LoginParams
import ru.workinprogress.fitpass.api.serializers.Login

/**
 * Created by panic on 22.02.2018.
 */

class AuthService(val sharedPreferences: SharedPreferences) {

    companion object {
        val schemeTag = "scheme:prefs"
        val tokenTag = "token:prefs"
        val credentialsTag = "prefs:credentials"
        val pincodeTag = "pincode:tag"
    }

    fun save(login: Login) {
        sharedPreferences.edit().putString(schemeTag, login.Scheme).putString(tokenTag, login.Token).apply()
    }

    fun get() = Login(sharedPreferences.getString(schemeTag, ""), sharedPreferences.getString(tokenTag, ""))


    fun save(loginParams: LoginParams) {
        sharedPreferences.edit().putString(credentialsTag, Gson().toJson(loginParams)).apply()
    }


    var pin
        get() =
            sharedPreferences.getString(pincodeTag, "")
        set(value) {
            sharedPreferences.edit().putString(pincodeTag, value).apply()
        }

    fun reset() {
        sharedPreferences.edit().remove(credentialsTag).remove(tokenTag).remove(schemeTag).remove(pincodeTag).apply()
    }

    val params: LoginParams? get() = Gson().fromJson<LoginParams>(sharedPreferences.getString(credentialsTag, ""), LoginParams::class.java)

}