package ru.workinprogress.fitpass.data

import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides

import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.newTask
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.workinprogress.fitpass.BuildConfig
import ru.workinprogress.fitpass.api.AuthService
import ru.workinprogress.fitpass.api.serializers.ApiResponseError
import ru.workinprogress.fitpass.di.ApplicationScope
import ru.workinprogress.fitpass.ui.activity.SplashActivity
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit
import javax.xml.datatype.DatatypeConstants.SECONDS
import android.support.v4.content.ContextCompat.startActivity
import java.nio.charset.Charset


@Module(includes = arrayOf())
class DataModule {
    private val SHARED_PREFERENCES_NAME = "app:preference:name"

    @Provides
    @ApplicationScope
    fun providesSharedPreferences(app: Application):
            SharedPreferences = app.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)


    @Provides
    @ApplicationScope
    fun provideRetrofitBuilder(converterFactory: Converter.Factory, okHttpClient: OkHttpClient): Retrofit.Builder = Retrofit.Builder()
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .addConverterFactory(converterFactory)


    @Provides
    @ApplicationScope
    fun provideConverterFactory(gson: Gson): Converter.Factory = GsonConverterFactory.create(gson)

    @Provides
    @ApplicationScope
    fun provideGson(): Gson =
            GsonBuilder().create()


    @Provides
    @ApplicationScope
    fun provideOkHttpClient(app: Application, authService: AuthService, gson: Gson): OkHttpClient {

        val builder = OkHttpClient.Builder()
        builder.connectTimeout(35, TimeUnit.SECONDS)
        builder.readTimeout(35, TimeUnit.SECONDS)
        builder.addInterceptor { chain ->
            val newBuilder = chain.request().newBuilder()
            authService.get().let { login ->
                if (login.Scheme.isNotEmpty()) {
                    newBuilder.addHeader("Authorization", "${login.Scheme} ${login.Token}")
                }
            }

            val response = chain.proceed(newBuilder.build())

            if (response.code() == 403) {
                val responseBodyString = response.body()?.source()?.apply {
                    request(Long.MAX_VALUE)
                }?.buffer()?.clone()?.readString(Charset.forName("UTF-8"))

                if ((responseBodyString?.let {
                            gson.fromJson<ApiResponseError>(it, ApiResponseError::class.java)
                        }?.Error?.ErrorCode == "INVALID_AUTHENTICATION_TOKEN")) {
                    app.startActivity(Intent(app, SplashActivity::class.java).newTask().clearTop())
                }
            }
            response
        }


        if (BuildConfig.DEBUG) {
            builder.addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
        }
        return builder.build()
    }

}



