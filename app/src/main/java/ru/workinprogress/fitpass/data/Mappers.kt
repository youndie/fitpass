package gov.magadan.news.data

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by panic on 18.10.2017.
 */
@Mapper
interface DataMapper {

}

class DateMapper {
	
	companion object {
		val sdf = SimpleDateFormat("yyyyMMddhhmmss")
	}

	public fun asDate(string: String?): Date? {
		if (string.isNullOrBlank()) return null
		return sdf.parse(string)
	}
}

object Mapper :
		DataMapper by _mappers.newsMapper


object _mappers {
	val newsMapper: DataMapper = Mappers.getMapper(DataMapper::class.java)
}


