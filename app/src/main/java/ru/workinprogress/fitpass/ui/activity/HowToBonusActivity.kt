package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import kotlinx.android.synthetic.main.how_to_bonus_activity.*
import ru.workinprogress.fitpass.R
import java.io.Serializable

class HowToBonusActivity : AppCompatActivity() {

    data class HowToBox(val title: String, val header: String, val text: String) : Serializable

    companion object {
        const val howToTag = "how:to"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.how_to_bonus_activity)
        val howToBox = intent.getSerializableExtra(howToTag) as HowToBox
        supportActionBar?.title = howToBox.title
        titleTextView.text = howToBox.header
        textTextView.text = howToBox.text
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}