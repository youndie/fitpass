package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.news_detail_activity.*
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.News
import ru.workinprogress.fitpass.api.serializers.photos
import ru.workinprogress.fitpass.presentation.NewsDetailPresenter
import ru.workinprogress.fitpass.presentation.NewsDetailView
import ru.workinprogress.fitpass.utils.Format
import java.text.SimpleDateFormat

/**
 * Created by panic on 16.03.2018.
 */

class NewsDetailActivity : MvpAppCompatActivity(), NewsDetailView {
    override fun showNews(newsItem: News) {
        if (newsItem.Media.isEmpty()) {
            viewPager.visibility = View.GONE
        } else {
            viewPager.adapter = object : PagerAdapter() {

                override fun instantiateItem(container: ViewGroup, position: Int): Any {
                    val imageView = layoutInflater.inflate(R.layout.news_image_item, null, false) as ImageView
                    Picasso.get().load(newsItem.Media[position].Uri).into(imageView)
                    container.addView(imageView)
                    imageView.setOnClickListener{
                        startActivity<PhotosActivity>(PhotosActivity.photosTag to  newsItem.Media.map { it.Uri }.toTypedArray(), PhotosActivity.defaultTag to position)
                    }
                    return imageView
                }

                override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
                    container.removeView(`object` as View)
                }

                override fun isViewFromObject(view: View, `object`: Any): Boolean {
                    return view === `object`
                }

                override fun getCount() = newsItem.Media.size
            }
            indicator.setViewPager(viewPager)
        }
        headerTextView.text = newsItem.Header
        textTextView.text = newsItem.Body
        dateTextView.text = format.format(Format.sourceYMDHMSFormat.parse(newsItem.Date))
    }
    private val format = SimpleDateFormat("dd.MM.yyyy")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.news_detail_activity)
        setSupportActionBar(toolbar)

        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = true
            internal var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.title = "Новости"
                    isShow = true
                } else if (isShow) {
                    collapsingToolbarLayout.title = " "
                    isShow = false
                }
            }
        })
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }


    companion object {
        const val newsDetailTag = "news:detail:tag"
    }

    @InjectPresenter
    lateinit var newsDetailPresenter: NewsDetailPresenter

    @ProvidePresenter
    fun providesNewsDetailPresenter(): NewsDetailPresenter = NewsDetailPresenter(intent.getSerializableExtra(newsDetailTag) as News)

}