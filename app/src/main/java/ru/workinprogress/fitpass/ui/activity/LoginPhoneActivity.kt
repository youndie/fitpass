package ru.workinprogress.fitpass.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.jakewharton.rxbinding2.view.enabled
import com.jakewharton.rxbinding2.widget.textChanges
import kotlinx.android.synthetic.main.login_phone_activity.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.newTask
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.LoginPhonePresenter
import ru.workinprogress.fitpass.presentation.LoginPhoneView
import ru.workinprogress.fitpass.utils.Validation

/**
 * Created by panic on 27.02.2018.
 */

class LoginPhoneActivity : MvpAppCompatActivity(), LoginPhoneView {
    override fun finishLogin() {
    }

    override fun showLoginError() {
        pinEntryEditText.requestFocus()
        pinEntryEditText.text.clear()
        Snackbar.make(parentLayout, "Неверный код", Snackbar.LENGTH_LONG).show()
    }

    override fun success() {
        startActivity(Intent(this, MainActivity::class.java).newTask().clearTask())
        finish()
    }

    override fun startLogin() {
        this.currentFocus?.let {
            (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(it.windowToken, 0)
        }
    }

    override fun showPhoneLayout() {
        phoneLayout.visibility = View.VISIBLE
        codeLayout.visibility = View.GONE
    }

    override fun showCodeLayout() {
        phoneLayout.visibility = View.GONE
        pinEntryEditText.requestFocus()
        codeLayout.visibility = View.VISIBLE
    }

    override fun showInvalidPhone() {

    }

    override fun showError() {

    }

    @InjectPresenter
    lateinit var loginPhonePresenter: LoginPhonePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_phone_activity)
        setSupportActionBar(toolbar)
        supportActionBar?.elevation = 0f
        title = "Вход"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        phoneEditText.textChanges().map { Validation.isValidPhone(it.toString()) }.subscribe(sendButton.enabled())

        sendButton.setOnClickListener {
            loginPhonePresenter.onLogin(phoneEditText.text.toString())
        }
        pinEntryEditText.setOnPinEnteredListener {
            if (it.length == 6) {
                loginPhonePresenter.onPinEntered(it.toString())
            }
        }
        recoverButton.setOnClickListener {
            startActivity<PasswordRecoveryActivity>(PasswordRecoveryActivity.RESTORE_TAG to true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}