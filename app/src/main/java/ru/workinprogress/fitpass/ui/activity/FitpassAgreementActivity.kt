package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.text.Html
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.URLSpan
import android.widget.TextView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.jakewharton.rxbinding2.view.enabled
import com.jakewharton.rxbinding2.widget.checkedChanges
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import kotlinx.android.synthetic.main.fitpass_agreement_activity.*
import ru.workinprogress.fitpass.R

/**
 * Created by panic on 12.03.2018.
 */

class FitpassAgreementActivity : MvpAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fitpass_agreement_activity)
        contractTextView.text = Html.fromHtml("Согласен с " +
                "<a href='ru.workinprogress.fitpass.ui.activity.fitpassagreementactivity://contract'>договором-офертой</a>");
        contractTextView.isClickable = true;
        contractTextView.movementMethod = LinkMovementMethod.getInstance()
        stripUnderlines(contractTextView)
        processingTextView.text = Html.fromHtml("Даю " +
                "<a href='ru.workinprogress.fitpass.ui.activity.fitpassagreementactivity://processing'>согласие на обработку персональных данных</a>");
        processingTextView.isClickable = true;
        processingTextView.movementMethod = LinkMovementMethod.getInstance()
        stripUnderlines(processingTextView)
        Observable.combineLatest(contractCheckBox.checkedChanges(), processingCheckBox.checkedChanges(), BiFunction<Boolean, Boolean, Boolean> { t1, t2 -> t1 && t2 }).subscribe(acceptButton.enabled())
    }

    private fun stripUnderlines(textView: TextView) {
        val s = SpannableString(textView.text)
        val spans = s.getSpans(0, s.length, URLSpan::class.java)
        for (span in spans) {
            val start = s.getSpanStart(span)
            val end = s.getSpanEnd(span)
            s.removeSpan(span)
            s.setSpan(URLSpanNoUnderline(span.url), start, end, 0)
        }
        textView.text = s
    }

    private inner class URLSpanNoUnderline(url: String) : URLSpan(url) {
        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }
}