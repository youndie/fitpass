package ru.workinprogress.fitpass.ui.activity.settings

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.login_phone_activity.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.toast
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.settings.ConfirmPhonePresenter
import ru.workinprogress.fitpass.presentation.settings.ConfirmPhoneView

class ConfirmPhoneActivity : MvpAppCompatActivity(), ConfirmPhoneView {
    override fun showError(error: String) {
        toast(error).show()
    }

    private var progressDialog: Dialog? = null

    override fun startLoading() {
        progressDialog = indeterminateProgressDialog(title = "Загрузка").apply {
            show()
        }
    }

    override fun finishLoading() {
        progressDialog?.dismiss()
    }

    override fun success() {
        toast("Телефон подтверждён").show()
        setResult(Activity.RESULT_OK)
        finish()
    }

    companion object {
        const val PHONE_TAG = "phonetag"
        const val LOGIN_TAG = "logintag"
    }

    @InjectPresenter
    lateinit var confirmPhonePresenter: ConfirmPhonePresenter

    @ProvidePresenter
    fun provideConfirmPhonePresenter() = ConfirmPhonePresenter(intent.getStringExtra(PHONE_TAG), intent.getStringExtra(LOGIN_TAG))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.delete_account_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.elevation = 0f
        supportActionBar?.title = "Подтверждение телефона"
        pinEntryEditText.setOnPinEnteredListener {
            if (it.length == 6) {
                confirmPhonePresenter.onPinEntered(it.toString())
            }
        }
    }
}