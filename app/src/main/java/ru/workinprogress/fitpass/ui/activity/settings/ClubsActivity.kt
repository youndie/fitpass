package ru.workinprogress.fitpass.ui.activity.settings

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.clubs_activity.*
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.Club
import ru.workinprogress.fitpass.presentation.settings.ClubsPresenter
import ru.workinprogress.fitpass.presentation.settings.ClubsView
import ru.workinprogress.fitpass.ui.activity.AboutClubActivity
import ru.workinprogress.fitpass.ui.activity.AddClubActivity
import ru.workinprogress.fitpass.ui.adapter.ClubsAdapter
import ru.workinprogress.fitpass.ui.fragment.AboutClubFragment

/**
 * Created by panic on 15.03.2018.
 */

class ClubsActivity : MvpAppCompatActivity(), ClubsView {
    override fun finishLoading() {
        swiperefresh.isRefreshing = false
    }

    override fun showClubs(clubs: Array<Club>) {
        clubsAdapter.data.clear()
        clubsAdapter.data.addAll(clubs)
        clubsAdapter.notifyDataSetChanged()
    }

    @InjectPresenter
    lateinit var clubsPresenter: ClubsPresenter

    val clubsAdapter get() = recyclerView.adapter as ClubsAdapter

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.clubs_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Добавленные клубы"
        swiperefresh.setColorSchemeResources(R.color.colorPrimary)
        swiperefresh.setOnRefreshListener {
            clubsPresenter.refresh()
        }
        addClubButton.setOnClickListener { startActivity<AddClubActivity>() }
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ClubsAdapter {
            startActivity<AboutClubActivity>(AboutClubFragment.clubTag to it)
        }
    }

}