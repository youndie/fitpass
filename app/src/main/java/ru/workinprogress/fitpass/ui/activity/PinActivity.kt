package ru.workinprogress.fitpass.ui.activity

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.pin_activity.*
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.PinPresenter
import ru.workinprogress.fitpass.presentation.PinView

class PinActivity : MvpAppCompatActivity(), PinView {
    override fun success() {
        startActivity<MainActivity>()
        finish()
    }

    override fun error() {
        pinEntryEditText.text.clear()
        val v = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            v.vibrate(100)
        }
    }

    @InjectPresenter
    lateinit var pinPresenter: PinPresenter

    override fun onResume() {
        super.onResume()
        codeLayout.setBackgroundColor(Color.parseColor("#2470B8"))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pin_activity)
        pinEntryEditText.requestFocus()
        pinEntryEditText.setOnPinEnteredListener {
            if (it.length == 4) {
                pinPresenter.onPinEntered(it.toString())
            }
        }
    }

}