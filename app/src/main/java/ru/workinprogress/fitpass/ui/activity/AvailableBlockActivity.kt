package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.view.MenuItem
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.available_block_activity.*
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.AvailableBlock
import ru.workinprogress.fitpass.presentation.AvailableBlockPresenter
import ru.workinprogress.fitpass.presentation.AvailableBlockView
import ru.workinprogress.fitpass.utils.toReadableYMDDate
import java.text.SimpleDateFormat

class AvailableBlockActivity : MvpAppCompatActivity(), AvailableBlockView {
    private val sourceFormat = SimpleDateFormat("yyyy-MM-dd")
    private val format = SimpleDateFormat("dd.MM.yyyy")

    override fun showBlock(availableBlock: AvailableBlock) {
        titleTextView.text = availableBlock.Name
        countTextView.text = "${availableBlock.SeancesLeft} из ${availableBlock.SeancesTotal}"
        if (availableBlock.ValidBefore != null) {
            untilContainer.visibility = View.VISIBLE
            untilTextView.text = "до ${availableBlock.ValidBefore.toReadableYMDDate()}"
        }
        else {
            untilContainer.visibility = View.GONE
        }
        descriptionTextView.text = "???"
        createdTextView.text = format.format(sourceFormat.parse(availableBlock.DateOfCreation))
    }

    companion object {
        const val blockTag = "blockkk"
    }

    @InjectPresenter
    lateinit var availableBlockPresenter: AvailableBlockPresenter

    @ProvidePresenter
    fun provideAvailableBlockPresenter(): AvailableBlockPresenter = AvailableBlockPresenter(intent.getSerializableExtra(blockTag) as AvailableBlock)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.available_block_activity)
        setSupportActionBar(toolbar)
        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = true
            internal var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.title = "Услуги"
                    isShow = true
                } else if (isShow) {
                    collapsingToolbarLayout.title = " "
                    isShow = false
                }
            }
        })
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}