package ru.workinprogress.fitpass.ui.activity

import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import ru.workinprogress.fitpass.presentation.ProfileContractPresenter
import ru.workinprogress.fitpass.presentation.ProfileContractView

/**
 * Created by panic on 06.03.2018.
 */

class ProfileContractActivity : MvpAppCompatActivity(), ProfileContractView {

    @InjectPresenter
    lateinit var profileContractPresenter: ProfileContractPresenter

}