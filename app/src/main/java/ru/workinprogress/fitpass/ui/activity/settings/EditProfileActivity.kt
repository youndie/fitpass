package ru.workinprogress.fitpass.ui.activity.settings

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.edit_profile_activity.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.startActivityForResult
import org.jetbrains.anko.toast
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.City
import ru.workinprogress.fitpass.api.serializers.Country
import ru.workinprogress.fitpass.api.serializers.Language
import ru.workinprogress.fitpass.api.serializers.Person
import ru.workinprogress.fitpass.presentation.settings.EditProfilePresenter
import ru.workinprogress.fitpass.presentation.settings.EditProfileView
import java.util.*

/**
 * Created by panic on 15.03.2018.
 */

class EditProfileActivity : MvpAppCompatActivity(), EditProfileView {
    override fun phoneCodeSend(phone: String, login: String) {
        startActivityForResult<ConfirmPhoneActivity>(3, ConfirmPhoneActivity.LOGIN_TAG to login, ConfirmPhoneActivity.PHONE_TAG to phone)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 3 && resultCode == Activity.RESULT_OK) {
            phoneConfirmButton.visibility = View.GONE
            editProfilePresenter.refresh()
        }
    }

    override fun emailCodeSend() {
        toast("Для подтверждения адреса перейдите по ссылке, отправленной на Email")
    }

    override fun finishLoading() {
        progressDialog?.dismiss()
    }

    override fun showError(clientMessage: String) {
        toast(clientMessage).show()
    }


    private var progressDialog: Dialog? = null

    override fun startLoading() {
        progressDialog = indeterminateProgressDialog(title = "Загрузка").apply {
            show()
        }
    }

    override fun showDate(date: String?) {
        if (date.isNullOrBlank()) {
            birthDayEditText.setOnClickListener {
                val c = Calendar.getInstance()
                DatePickerDialog(this,
                        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth -> editProfilePresenter.onBirthDaySelected(year, monthOfYear, dayOfMonth) },
                        c.get(Calendar.YEAR) - 18,
                        c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH)).show()
            }
        } else {
            birthDayEditText.setText(date)
            birthDayEditText.isEnabled = false
        }
    }

    override fun showLanguages(languages: Array<Language>, code: String) {
        languageSpinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, languages).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        languageSpinner.setSelection(languages.indexOfFirst { it.Code == code })
    }

    override fun showCities(cities: Array<City>, cityId: Int) {
        citySpinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, cities).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        citySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, v: View?, p2: Int, p3: Long) {
                editProfilePresenter.onCitySelected(cities[p2])
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        }
        citySpinner.setSelection(cities.indexOfFirst { it.ID == cityId })
    }

    override fun showCountries(countries: Array<Country>, code: String) {
        countrySpinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, countries).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        countrySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, v: View?, p2: Int, p3: Long) {
                editProfilePresenter.onCountrySelected(countries[p2])
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }
        countrySpinner.setSelection(countries.indexOfFirst { it.Code == code })
    }


    override fun showPerson(person: Person) {
        nameEditText.setText("${person.NickName}")
        nameEditText.isEnabled = false
        loginEditText.setText(person.Login)
        loginEditText.isEnabled = false
        phoneEditText.setText("+7${person.MainPhone}")
        phoneEditText.isEnabled = false

        emailEditText.setText(person.MainEmail)
        if (person.Gender != 0) {
            sexRadioGroup.check(if (person.Gender == 1) R.id.male else R.id.female)
            sexRadioGroup.isEnabled = false
            male.isEnabled = false
            female.isEnabled = false
        } else {
            sexRadioGroup.isEnabled = true
            sexRadioGroup.setOnCheckedChangeListener { group, checkedId ->
                editProfilePresenter.onSexChanged(checkedId == R.id.male)
            }
        }
        emailConfirmButton.visibility = if (person.MainEmailConfirmed) View.GONE else View.VISIBLE
        phoneConfirmButton.visibility = if (person.MainPhoneConfirmed) View.GONE else View.VISIBLE
    }

    @InjectPresenter
    lateinit var editProfilePresenter: EditProfilePresenter

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.edit_profile, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_profile_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Личные данные"
        emailConfirmButton.setOnClickListener {
            editProfilePresenter.onEmailConfirmClicked()
        }
        phoneConfirmButton.setOnClickListener {
            editProfilePresenter.onPhoneConfirmClicked()
        }
    }

    fun validateFields(): Boolean {
        return true
    }

    val email get() = emailEditText.text.toString()

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        if (item?.itemId == R.id.done && validateFields()) {
            editProfilePresenter.onEditProfile(email)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}