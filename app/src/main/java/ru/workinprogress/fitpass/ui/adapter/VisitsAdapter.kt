package ru.workinprogress.fitpass.ui.adapter

import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.util.TimeUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.find
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.Visit
import ru.workinprogress.fitpass.utils.Format.sourceYMDHMSFormat
import java.text.SimpleDateFormat
import java.time.Duration
import java.time.LocalDate
import java.util.*


/**
 * Created by panic on 26.03.2018.
 */

class VisitsAdapter : RecyclerView.Adapter<VisitViewHolder>() {

    val data = mutableListOf<Visit>()

    val df = SimpleDateFormat("dd.MM.yyyy");
    val ef = SimpleDateFormat("EEEE", Locale("ru", "RU"))
    val tf = SimpleDateFormat("H:mm")
    val tfNoZone = SimpleDateFormat("H:mm").apply {
        timeZone = TimeZone.getTimeZone("UTC")
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VisitViewHolder = VisitViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.visit_profile_item, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: VisitViewHolder, position: Int) {
        with(data[position]) {

            val start = sourceYMDHMSFormat.parse(StartTime)
            val end = sourceYMDHMSFormat.parse(EndTime)

            val diff = end.time - start.time



            holder.titleTextView.text = "${df.format(start)} (${ef.format(start).capitalize()})"
            holder.dateTextView.text = if (diff < 43200000)
                "${tf.format(start)} - ${tf.format(end)} (длительность ${tfNoZone.format(Date(diff))})" else
                "${tf.format(start)} (длительность более 12 часов)"
            holder.keysTextView.text = "ключи: ${UsedKeys?.joinToString(separator = ", ")}"
            holder.keysTextView.visibility = if (UsedKeys == null || UsedKeys.isEmpty()) {
                View.GONE
            } else View.VISIBLE
        }
    }
}


class VisitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val titleTextView by lazy { itemView.find<TextView>(R.id.titleTextView) }
    val dateTextView by lazy { itemView.find<TextView>(R.id.dateTextView) }
    val keysTextView by lazy { itemView.find<TextView>(R.id.keysTextView) }

}