package ru.workinprogress.fitpass.ui.activity.settings

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.login_phone_activity.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.settings.DeleteAccountPresenter
import ru.workinprogress.fitpass.presentation.settings.DeleteAccountView
import ru.workinprogress.fitpass.ui.activity.SplashActivity

class DeleteAccountActivity : MvpAppCompatActivity(), DeleteAccountView {
    override fun showError(error: String) {
        finish()
        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
    }


    override fun startLoading() {
        progressDialog = indeterminateProgressDialog(title = "Удаление").apply {
            show()
        }
        this.currentFocus?.let {
            (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(it.windowToken, 0)
        }
    }

    var progressDialog: Dialog? = null

    override fun finishLoading() {
        progressDialog?.dismiss()
    }

    override fun success() {
        finish()
        startActivity<SplashActivity>()
    }

    @InjectPresenter
    lateinit var deleteAccountPresenter: DeleteAccountPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.delete_account_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.elevation = 0f
        supportActionBar?.title = "Удаление профиля"
        pinEntryEditText.setOnPinEnteredListener {
            if (it.length == 6) {
                deleteAccountPresenter.onPinEntered(it.toString())
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}