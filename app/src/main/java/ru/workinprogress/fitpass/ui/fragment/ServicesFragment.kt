package ru.workinprogress.fitpass.ui.fragment

import android.app.Fragment
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.available_blocks_layout.view.*
import kotlinx.android.synthetic.main.contract_detail_activity.*
import kotlinx.android.synthetic.main.sale_services_layout.*
import kotlinx.android.synthetic.main.services_fragment.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.AvailableBlock
import ru.workinprogress.fitpass.api.serializers.UnpaidService
import ru.workinprogress.fitpass.presentation.ServicesPresenter
import ru.workinprogress.fitpass.presentation.ServicesView
import ru.workinprogress.fitpass.ui.activity.AvailableBlockActivity
import ru.workinprogress.fitpass.ui.activity.ContractPricelistActivity
import ru.workinprogress.fitpass.ui.activity.PricelistActivity
import ru.workinprogress.fitpass.ui.adapter.AvailableBlocksAdapter
import ru.workinprogress.fitpass.ui.view.UnpaidGroupLayout
import kotlin.math.roundToInt

/**
 * Created by panic on 06.03.2018.
 */

class ServicesFragment : MvpFragment(), ServicesView {
    override fun showBackgrounds(contracts: String?, blocks: String?, services: String?) {

        fun ImageView.showImage(s: String?) {
            if (s == null) {
                visibility = View.GONE

            } else {
                visibility = View.VISIBLE
                Picasso.get().load(s).into(this)
            }
        }
        saleServicesLayout.find<ImageView>(R.id.contractsImage).showImage(contracts)
        saleServicesLayout.find<ImageView>(R.id.blocksImage).showImage(blocks)
        saleServicesLayout.find<ImageView>(R.id.servicesImage).showImage(services)

    }

    override fun unpaidLoading(it: Boolean) {
        if (it) {
            unpaidContainer.removeAllViews()
            unpaidSwipeRefreshLayout.isRefreshing = true
        }
    }

    override fun showUnpaid(unpaids: Array<UnpaidService>) {
        unpaidContainer.removeAllViews()
        unpaidSwipeRefreshLayout.isRefreshing = false
        if (unpaids != null && unpaids.isNotEmpty())
            unpaids.groupBy { it.ScopeID!! }.toSortedMap().map { entry ->
                UnpaidGroupLayout(activity).apply {
                    tag = entry.key
                    init(this@ServicesFragment.mvpDelegate, entry.value.firstOrNull()?.ScopeName.orEmpty())
                }
            }.forEach {
                unpaidContainer.addView(it)
            }
    }

    override fun showBlocks(blocks: Array<AvailableBlock>) {
        blocksRefreshLayout.isRefreshing = false
        availableBlocksAdapter.apply {
            data.clear()
            data.addAll(blocks)
            notifyDataSetChanged()
        }
    }

    companion object {
        const val unpaidTag = "unpaid:tag"
        const val blocksTag = "blocks:tag"
    }

    @InjectPresenter
    lateinit var servicesPresenter: ServicesPresenter

    val unpaidServicesLayout: View by lazy {
        LayoutInflater.from(activity).inflate(R.layout.unpaid_services_layout, null, false).apply {
            find<SwipeRefreshLayout>(R.id.swiperefresh).apply {
                setColorSchemeResources(R.color.colorPrimary)
                setOnRefreshListener {
                    servicesPresenter.refreshUnpaid()
                }
            }
        }
    }

    val availableBlocksLayout: View by lazy {
        LayoutInflater.from(activity).inflate(R.layout.available_blocks_layout, null, false).apply {
            find<SwipeRefreshLayout>(R.id.swiperefresh).apply {
                setColorSchemeResources(R.color.colorPrimary)
                setOnRefreshListener {
                    servicesPresenter.refreshAvailableBlocks()
                }
            }
        }
    }

    val saleServicesLayout: View by lazy {
        LayoutInflater.from(activity).inflate(R.layout.sale_services_layout, null, false).apply {
            find<View>(R.id.contracts).setOnClickListener {
                startActivity<ContractPricelistActivity>()
            }
            find<View>(R.id.blocks).setOnClickListener {
                startActivity<PricelistActivity>(PricelistActivity.typeTag to PricelistActivity.blocksType)
            }
            find<View>(R.id.services).setOnClickListener {
                startActivity<PricelistActivity>(PricelistActivity.typeTag to PricelistActivity.servicesType)
            }
        }
    }

    private val availableBlocksAdapter by lazy {
        AvailableBlocksAdapter {
            startActivity<AvailableBlockActivity>(AvailableBlockActivity.blockTag to it)
        }.also {
            availableBlocksLayout.find<RecyclerView>(R.id.recyclerView).apply {
                layoutManager = LinearLayoutManager(activity)
                adapter = it
            }
        }
    }


    private val unpaidContainer by lazy { unpaidServicesLayout.find<LinearLayout>(R.id.contentContainer) }
    private val unpaidSwipeRefreshLayout by lazy { unpaidServicesLayout.find<SwipeRefreshLayout>(R.id.swiperefresh) }
    private val blocksRefreshLayout by lazy { availableBlocksLayout.find<SwipeRefreshLayout>(R.id.swiperefresh) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.services_fragment, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewPager.offscreenPageLimit = 3
        viewPager.adapter = object : android.support.v4.view.PagerAdapter() {
            override fun isViewFromObject(view: View, `object`: Any): Boolean {
                return view === `object`
            }

            override fun getCount(): Int = 3

            override fun instantiateItem(container: ViewGroup, position: Int): Any {
                val view = when (position) {
                    0 -> saleServicesLayout
                    1 -> availableBlocksLayout
                    else -> unpaidServicesLayout
                }
                container.addView(view)
                return view
            }

            override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
                container.removeView(`object` as View)
            }

            override fun getPageTitle(position: Int): CharSequence? {
                return when (position) {
                    0 -> "Приобрести"
                    1 -> "Неоказанные услуги"
                    else -> "Неоплаченные услуги"
                }
            }

        }
        tabLayout.setupWithViewPager(viewPager)

        if (arguments?.getBoolean(unpaidTag, false) == true) {
            viewPager.currentItem = 2
        }
        if (arguments?.getBoolean(blocksTag, false) == true) {
            viewPager.currentItem = 1
        }
    }


}