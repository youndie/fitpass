package ru.workinprogress.fitpass.ui.activity.settings

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.change_password_activity.*
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.settings.ChangePasswordPresenter
import ru.workinprogress.fitpass.presentation.settings.ChangePasswordView
import ru.workinprogress.fitpass.utils.Validation

/**
 * Created by panic on 15.03.2018.
 */

class ChangePasswordActivity : MvpAppCompatActivity(), ChangePasswordView {
    override fun showLoading() {

    }

    override fun showError() {

    }

    override fun success() {
        finish()
    }

    @InjectPresenter
    lateinit var changePasswordPresenter: ChangePasswordPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_password_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Изменить пароль"
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.edit_profile, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        if (item?.itemId == R.id.done) {
            val old = oldPassword.text.toString()
            val new = newPassword.text.toString()
            val repeat = repeatPassword.text.toString()

            if (!Validation.isValidPassword(new)) {
                newPassword.error = "Слишком простой пароль"
                return true
            }

            if (new != repeat) {
                repeatPassword.error = "Пароли отличаются"
                return true
            }

            changePasswordPresenter.changePassword(old, new)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}