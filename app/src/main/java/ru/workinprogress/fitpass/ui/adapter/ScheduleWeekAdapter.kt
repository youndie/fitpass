package ru.workinprogress.cityfitness.ui.adapter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import me.henrytao.recyclerpageradapter.RecyclerPagerAdapter

import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.UIScheduleItem
import ru.workinprogress.fitpass.presentation.UIWeek


/**
 * Created by Pavel Votyakov on 20.04.17.
 * Work in progress © 2017
 */
class ScheduleWeekAdapter(val context: Context, val weeks: List<UIWeek>, val listener: ((UIScheduleItem) -> Unit)) : RecyclerPagerAdapter<ItemViewHolder>() {
    override fun getItemCount(): Int = weeks.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder = ItemViewHolder(createRecyclerView())

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(weeks[position].items)
    }

    val viewPool = RecyclerView.RecycledViewPool()
    val padding by lazy {
        context.resources.getDimension(R.dimen.main_recycler_padding).toInt()
    }

    private fun createRecyclerView(): RecyclerView {
        val recyclerView = RecyclerView(context)
        recyclerView.clipToPadding = false
        recyclerView.clipChildren = false
        recyclerView.setPadding(0, padding, 0, padding)
        recyclerView.adapter = ScheduleDaysAdapter()
        (recyclerView.adapter as ScheduleDaysAdapter).listener = listener
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.recycleChildrenOnDetach = true
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.recycledViewPool = viewPool
        return recyclerView
    }

}

class ItemViewHolder(val recyclerView: RecyclerView) : RecyclerPagerAdapter.ViewHolder(recyclerView) {

    fun bind(items: List<UIScheduleItem>) {
        val scheduleDaysAdapter = recyclerView.adapter as ScheduleDaysAdapter
        scheduleDaysAdapter.days.clear()
        scheduleDaysAdapter.days.addAll(items)
        scheduleDaysAdapter.notifyDataSetChanged()
    }
}
