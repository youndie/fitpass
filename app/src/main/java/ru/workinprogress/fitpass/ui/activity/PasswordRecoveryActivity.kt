package ru.workinprogress.fitpass.ui.activity

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.jakewharton.rxbinding2.view.enabled
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.Observable
import kotlinx.android.synthetic.main.password_recovery_activity.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.toast
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.PasswordRecoverPresenter
import ru.workinprogress.fitpass.presentation.PasswordRecoverView
import ru.workinprogress.fitpass.utils.Validation

/**
 * Created by panic on 28.02.2018.
 */

class PasswordRecoveryActivity : MvpAppCompatActivity(), PasswordRecoverView {
    override fun startChanging() {
        progressDialog = indeterminateProgressDialog(title = "Изменение пароля").apply {
            show()
        }
    }

    override fun finishChanging() {
        progressDialog?.dismiss()
    }

    override fun success() {
        toast("Пароль успешно изменен. Можете воспользоваться им для входа").show()
        finish()
    }

    var progressDialog: Dialog? = null


    companion object {
        const val RESTORE_TAG = "restore"
    }

    override fun showFields() {
        this.currentFocus?.let {
            (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(it.windowToken, 0)
        }
        supportActionBar?.elevation = 4f
        scrollView.visibility = View.GONE
        codeLayout.visibility = View.GONE
        fieldsLayout.visibility = View.VISIBLE
    }

    override fun showRecoverError(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
    }

    override fun startRecover() {
        arrayOf(emailEditText, loginEditText, phoneEditText, emailLoginButton, loginLoginButton, phoneLoginButton).forEach { it.isEnabled = false }

    }

    override fun finishRecover() {
        arrayOf(emailEditText, loginEditText, phoneEditText, emailLoginButton, loginLoginButton, phoneLoginButton).forEach { it.isEnabled = true }
    }

    override fun codeSended() {
        scrollView.visibility = View.GONE
        codeLayout.visibility = View.VISIBLE
        fieldsLayout.visibility = View.GONE
        pinEntryEditText.requestFocus()
    }

    @InjectPresenter
    lateinit var passwordRecoverPresenter: PasswordRecoverPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.password_recovery_activity)
        setSupportActionBar(toolbar)
        supportActionBar?.elevation = 0f
        title = "Восстановление пароля"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        arrayOf(emailExpandLayout, loginExpandLayout, phoneExpandLayout).forEach { it.visibility = View.GONE }
        arrayOf(emailExpandIcon, loginExpandIcon, phoneExpandIcon).forEach { it.setImageResource(R.drawable.ic_expand_more_white_24dp) }

        if (intent?.getBooleanExtra(RESTORE_TAG, false) == true) {
            expand(emailExpandIcon, emailExpandLayout, true)
        } else
            expand(phoneExpandIcon, phoneExpandLayout, true)

        phoneEditText.textChanges().map { it.isNotEmpty() }.subscribe(phoneLoginButton.enabled())
        loginEditText.textChanges().map { it.isNotEmpty() }.subscribe(loginLoginButton.enabled())
        emailEditText.textChanges().map { Validation.isEmailValid(it.toString()) }.subscribe(emailLoginButton.enabled())

        phoneExpandClicker.setOnClickListener {
            expand(phoneExpandIcon, phoneExpandLayout, true)
            expand(emailExpandIcon, emailExpandLayout, false)
            expand(loginExpandIcon, loginExpandLayout, false)
        }
        loginExpandClicker.setOnClickListener {
            expand(phoneExpandIcon, phoneExpandLayout, false)
            expand(emailExpandIcon, emailExpandLayout, false)
            expand(loginExpandIcon, loginExpandLayout, true)
        }
        emailExpandClicker.setOnClickListener {
            expand(phoneExpandIcon, phoneExpandLayout, false)
            expand(emailExpandIcon, emailExpandLayout, true)
            expand(loginExpandIcon, loginExpandLayout, false)
        }

        loginLoginButton.setOnClickListener {
            passwordRecoverPresenter.onLoginRecover(loginEditText.text.toString())
        }
        emailLoginButton.setOnClickListener {
            passwordRecoverPresenter.onEmailRecover(emailEditText.text.toString())
        }
        phoneLoginButton.setOnClickListener {
            passwordRecoverPresenter.onPhoneRecover(phoneEditText.text.toString())
        }
        pinEntryEditText.setOnPinEnteredListener {
            if (it.length == 6) {
                passwordRecoverPresenter.onPinEntry(it.toString())
            }
        }
        changePasswordButton.setOnClickListener {
            this.currentFocus?.let {
                (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(it.windowToken, 0)
            }
            val new = newPassword.text.toString()
            val repeat = repeatPassword.text.toString()
            if (new != repeat) {
                repeatPassword.error = "Пароли должны совпадать"
            } else
                passwordRecoverPresenter.onChangePassword(new)
        }
    }


    fun expand(iconView: ImageView, layout: View, expand: Boolean) {
        iconView.setImageResource(if (expand) R.drawable.ic_expand_less_white_24dp else R.drawable.ic_expand_more_white_24dp)
        layout.visibility = if (expand) View.VISIBLE else View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}