package ru.workinprogress.fitpass.ui.activity

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputLayout
import android.support.transition.TransitionManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.accounts_activity.*
import org.jetbrains.anko.browse
import org.jetbrains.anko.find
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.toast
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.Account
import ru.workinprogress.fitpass.domain.CardsInfo
import ru.workinprogress.fitpass.presentation.AccountsPresenter
import ru.workinprogress.fitpass.presentation.AccountsView
import ru.workinprogress.fitpass.ui.adapter.AccountsAdapter

/**
 * Created by panic on 26.03.2018.
 */

class AccountsActivity : MvpAppCompatActivity(), AccountsView {

    var progressDialog: Dialog? = null

    override fun startPay() {
        globalBottomContainer.visibility = View.GONE
        globalDim.visibility = View.GONE
        this.currentFocus?.let {
            (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(it.windowToken, 0)
        }
        progressDialog = indeterminateProgressDialog(title = "Оплата").apply {
            show()
        }
    }

    override fun finishPay() {
        progressDialog?.dismiss()
    }

    override fun paySuccess() {
        toast("Пополнение счёта прошло успешно").show()
    }

    override fun showPaymentPage(url: String) {
        browse(url)
    }

    override fun onBackPressed() {
        if (globalDim.visibility == View.VISIBLE) {
            android.support.transition.TransitionManager.beginDelayedTransition(mainConstraintLayout)
            globalBottomContainer.visibility = View.GONE
            globalDim.visibility = View.GONE
        } else super.onBackPressed()
    }

    override fun showPayVariants(cardsByLegalBankInfo: CardsInfo) {
        globalBottomContainer.removeAllViews()
        val sumLayout = LayoutInflater.from(this).inflate(R.layout.sum_input_layout, globalBottomContainer, false)
        globalBottomContainer.addView(sumLayout)
        cardsByLegalBankInfo.list.map { card ->
            LayoutInflater.from(this).inflate(R.layout.button_panel_item, globalBottomContainer, false).apply {
                find<TextView>(R.id.button).text = "Используя карту ${card.Name}"
                setOnClickListener {
                    accountsPresenter.onCardPayClicked(card, sumLayout.find<TextInputLayout>(R.id.sumInputLayout).editText?.text.toString().toIntOrNull()
                            ?: 0)
                }
            }
        }.forEach {
            globalBottomContainer.addView(it)
        }
        if (cardsByLegalBankInfo.newCard)
            globalBottomContainer.addView(LayoutInflater.from(this).inflate(R.layout.button_panel_item, globalBottomContainer, false).apply {
                find<TextView>(R.id.button).text = "Используя новую карту"
                setOnClickListener {
                    accountsPresenter.onNewCardPayClicked(sumLayout.find<TextInputLayout>(R.id.sumInputLayout).editText?.text.toString().toIntOrNull()
                            ?: 0)
                }
            })
        TransitionManager.beginDelayedTransition(globalDim.parent as ViewGroup)
        globalBottomContainer.visibility = View.VISIBLE
        globalDim.visibility = View.VISIBLE
    }

    override fun startLoading() {
        snackbar?.dismiss()
        swipeRefresh.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    override fun finishLoading() {
        swipeRefresh.isRefreshing = false
        swipeRefresh.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }

    var snackbar: Snackbar? = null

    override fun showError(clientMessage: String) {
        snackbar = Snackbar.make(parentLayout, clientMessage, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(R.string.retry, {
                accountsPresenter.load()
            })
            show()
        }
    }

    override fun showAccounts(accounts: Array<Account>) {
        adapter.data.clear()
        adapter.data.addAll(accounts)
        adapter.notifyDataSetChanged()
    }

    @InjectPresenter
    lateinit var accountsPresenter: AccountsPresenter

    override fun onResume() {
        super.onResume()
        accountsPresenter.load()
    }

    val adapter get() = recyclerView.adapter as AccountsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.accounts_activity)
        toolbar?.background?.alpha = 255
        globalDim.setOnClickListener {
            android.support.transition.TransitionManager.beginDelayedTransition(mainConstraintLayout)
            globalBottomContainer.visibility = View.GONE
            globalDim.visibility = View.GONE
        }
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Счета"
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = AccountsAdapter {
            accountsPresenter.onPayAccountClicked(it)
        }
        swipeRefresh.apply {
            setColorSchemeResources(R.color.colorPrimary)
            setOnRefreshListener {
                accountsPresenter.load()
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }


}