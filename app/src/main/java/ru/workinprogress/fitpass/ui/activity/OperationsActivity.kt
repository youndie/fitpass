package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.operations_activity.*
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.Operation
import ru.workinprogress.fitpass.api.serializers.SaleItem
import ru.workinprogress.fitpass.presentation.OperationsPresenter
import ru.workinprogress.fitpass.presentation.OperationsView
import ru.workinprogress.fitpass.ui.adapter.AccountsAdapter
import ru.workinprogress.fitpass.ui.adapter.OperationsAdapter

/**
 * Created by panic on 26.03.2018.
 */

class OperationsActivity : MvpAppCompatActivity(), OperationsView {

    @InjectPresenter
    lateinit var operationsPresenter: OperationsPresenter

    override fun showOperations(it: Array<SaleItem>) {
        adapter.data.clear()
        adapter.data.addAll(it)
        adapter.notifyDataSetChanged()
    }

    override fun startLoading() {
        snackbar?.dismiss()
        recyclerView.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    override fun finishLoading() {
        recyclerView.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }

    var snackbar: Snackbar? = null

    override fun showError() {
        snackbar = Snackbar.make(parentLayout, R.string.no_internet_error, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(R.string.retry, {
                operationsPresenter.load()
            })
        }
    }

    val adapter: OperationsAdapter get() = recyclerView.adapter as OperationsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.operations_activity)
        setSupportActionBar(toolbar)
        toolbar?.background?.alpha = 255
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = OperationsAdapter()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "История операций"
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}