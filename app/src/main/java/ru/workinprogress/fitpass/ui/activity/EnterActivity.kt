package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.enter_activity.*
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.di.AppComponent

/**
 * Created by panic on 22.02.2018.
 */


class EnterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.enter_activity)
        loginButton.setOnClickListener {
            startActivity<LoginActivity>()
        }
        registrationButton.setOnClickListener {
            startActivity<RegistrationActivity>()
        }
    }

}