package ru.workinprogress.fitpass.ui.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.CalendarView
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.lesson_detail_activity.*
import org.jetbrains.anko.*
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.AvailableBlock
import ru.workinprogress.fitpass.api.serializers.avatar
import ru.workinprogress.fitpass.api.serializers.photos
import ru.workinprogress.fitpass.presentation.LessonPresenter
import ru.workinprogress.fitpass.presentation.LessonView
import ru.workinprogress.fitpass.presentation.UILessonDetail
import ru.workinprogress.fitpass.utils.Format.sourceYMDFormat
import ru.workinprogress.fitpass.utils.toReadableYMDDate
import ru.workinprogress.fitpass.utils.toWeekdayYMDDate
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by panic on 14.03.2018.
 */

class LessonDetailActivity : MvpAppCompatActivity(), LessonView {
    override fun showError(s: String) {
        toast(s).show()
    }

    override fun startJoining() {
        dialog?.dismiss()
        dialog = indeterminateProgressDialog(title = "Запись").apply {
            show()
        }
    }

    override fun finishJoining() {
        dialog?.dismiss()
        TransitionManager.beginDelayedTransition(dim.parent as ViewGroup)
        dim.visibility = View.GONE
        paymentsVariantsContainer.visibility = View.GONE
    }

    private var dialog: Dialog? = null


    override fun showBlocks(availableBlocks: MutableList<AvailableBlock>) {
        TransitionManager.beginDelayedTransition(dim.parent as ViewGroup)
        paymentsVariantsContainer.removeAllViews()
        LayoutInflater.from(this).inflate(R.layout.back_layout, paymentsVariantsContainer, true).apply {
            setOnClickListener {
                lessonPresenter.onActionClicked()
            }
        }
        availableBlocks.map { block ->
            LayoutInflater.from(this).inflate(R.layout.block_item, paymentsVariantsContainer, false).apply {
                find<TextView>(R.id.nameTextView).text = block.Name
                find<TextView>(R.id.descriptionTextView).text = "Доступно ${block.SeancesLeft} из ${block.SeancesTotal}"
                block.ValidBefore?.toReadableYMDDate()?.let {
                    find<TextView>(R.id.countTextView).text = "до $it"
                }
                setOnClickListener {
                    lessonPresenter.onBlockSelected(block)
                }
            }
        }.forEach {
            paymentsVariantsContainer.addView(it)
        }
    }

    override fun success() {

    }

    override fun showJoinVariants(blocks: Boolean, items: Boolean, lessonId: String, typeId: String) {
        paymentsVariantsContainer.removeAllViews()
        TransitionManager.beginDelayedTransition(dim.parent as ViewGroup)
        dim.visibility = View.VISIBLE
        paymentsVariantsContainer.visibility = View.VISIBLE
        if (items)
            paymentsVariantsContainer.addView(LayoutInflater.from(this).inflate(R.layout.button_panel_item, paymentsVariantsContainer, false).apply {
                find<TextView>(R.id.button).text = "Приобрести услугу"
                setOnClickListener {
                    startActivityForResult<LessonPricelistActivity>(42, LessonPricelistActivity.lessonIdTag to lessonId, LessonPricelistActivity.lessonTypeIdTag to typeId)
                }
            })
        if (blocks)
            paymentsVariantsContainer.addView(LayoutInflater.from(this).inflate(R.layout.button_panel_item, paymentsVariantsContainer, false).apply {
                find<TextView>(R.id.button).text = "Записаться из блока"
                setOnClickListener {
                    lessonPresenter.onBlockClicked()
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 42 && resultCode == Activity.RESULT_OK) {
            TransitionManager.beginDelayedTransition(dim.parent as ViewGroup)
            dim.visibility = View.GONE
            paymentsVariantsContainer.visibility = View.GONE
            lessonPresenter.onJoined()
        }
    }

    override fun showJoined(joined: Boolean) {
        actionButton.visibility = if (joined) View.GONE else View.VISIBLE
    }

    override fun reloadPlaces() {
        TransitionManager.beginDelayedTransition(dim.parent as ViewGroup)
        freePlacesTextView.visibility = View.GONE
        placesProgressBar.visibility = View.VISIBLE
    }

    override fun showFreeplaces(capacity: Int) {
        TransitionManager.beginDelayedTransition(contentLayout)
        placesProgressBar.visibility = View.GONE
        freePlacesTextView.text = resources.getQuantityString(R.plurals.free_places, capacity, capacity)
        freePlacesTextView.visibility = View.VISIBLE
    }

    override fun onBackPressed() {
        if (dim.visibility == View.VISIBLE) {
            if (paymentsVariantsContainer.findViewById<View>(R.id.back_clicker) != null) {
                lessonPresenter.onActionClicked()
            } else {
                dim.visibility = View.GONE
                paymentsVariantsContainer.visibility = View.GONE
            }
        } else {
            super.onBackPressed()
        }
    }


    override fun showLesson(detailTraining: UILessonDetail) {
        titleTextView.text = detailTraining.lessonType?.Name

        dateTextView.text = detailTraining.lesson.Date.toWeekdayYMDDate().capitalize() + " — " + detailTraining.lesson.BeginTime
        placeTextView.text = detailTraining.room?.Name

        if (detailTraining.lessonType?.RequirePayment == true) {
            priceContainer.visibility = View.VISIBLE
        }

        Picasso.get().load(detailTraining.master?.avatar).into(masterAvatar)
        masterNameTextView.text = detailTraining.master?.Name
        masterTypeTextView.visibility = View.GONE
        detailTraining.master?.Description?.let {
            masterTypeTextView.text = it
            masterTypeTextView.visibility = View.VISIBLE
        }

        descriptionContainer.visibility = if (detailTraining.lessonType?.Description.isNullOrBlank()) View.GONE else View.VISIBLE
        descriptionTextView.text = detailTraining.lessonType?.Description
        photosContainer.removeAllViews()
        photosContainer.visibility = if (detailTraining.room?.photos?.isNotEmpty() == true) View.VISIBLE else View.GONE
        detailTraining.room?.photos?.forEachIndexed { index, media ->
            photosContainer.addView(LayoutInflater.from(this).inflate(R.layout.lesson_media_item, photosContainer, false).let { it as ImageView }.also {
                Picasso.get().load(media.Uri).into(it)
                it.setOnClickListener {
                    startActivity<PhotosActivity>(PhotosActivity.photosTag to detailTraining.room.photos.map { it.Uri }.toTypedArray(), PhotosActivity.defaultTag to index)
                }
            })
        }
    }

    @InjectPresenter
    lateinit var lessonPresenter: LessonPresenter

    @ProvidePresenter
    fun provideLessonPresenter(): LessonPresenter = LessonPresenter(intent.getStringExtra(lessonIdTag), intent.getBooleanExtra(personTag, false))

    companion object {
        const val lessonIdTag = "lesson:id"
        const val personTag = "person:tag"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lesson_detail_activity)
        setSupportActionBar(toolbar)
        title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        actionButton.setOnClickListener {
            lessonPresenter.onActionClicked()
        }
        dim.setOnClickListener {
            TransitionManager.beginDelayedTransition(dim.parent as ViewGroup)
            dim.visibility = View.GONE
            paymentsVariantsContainer.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}