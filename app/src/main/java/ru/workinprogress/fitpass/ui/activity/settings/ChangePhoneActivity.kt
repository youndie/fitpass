package ru.workinprogress.fitpass.ui.activity.settings

import android.os.Bundle
import android.view.MenuItem
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.jakewharton.rxbinding2.view.enabled
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import kotlinx.android.synthetic.main.change_phone_activity.*
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.settings.ChangePhonePresenter
import ru.workinprogress.fitpass.presentation.settings.ChangePhoneView
import ru.workinprogress.fitpass.ui.activity.async
import ru.workinprogress.fitpass.utils.Validation
import java.util.*

class ChangePhoneActivity : MvpAppCompatActivity(), ChangePhoneView {

    @InjectPresenter
    lateinit var changePhonePresenter: ChangePhonePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_phone_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Номер телефона"
        Observable.combineLatest(
                passwordEditText.textChanges().map { Validation.isValidPassword(it.toString()) },
                phoneEditText.textChanges().map { Validation.isValidPhone(it.toString()) },
                BiFunction<Boolean, Boolean, Boolean> { t1, t2 -> t1 && t2 }
        ).async().subscribe(acceptButton.enabled())
        acceptButton.setOnClickListener {
            changePhonePresenter.changePhone(passwordEditText.text.toString(), phoneEditText.text.toString())
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }


}