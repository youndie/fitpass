package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.pricelist_detail_activity.*
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.PricelistItem
import ru.workinprogress.fitpass.presentation.PricelistItemPresenter
import ru.workinprogress.fitpass.presentation.PricelistItemView

/**
 * Created by panic on 07.03.2018.
 */

open class BasePricelistDetailActivity : MvpAppCompatActivity(), PricelistItemView {
    override fun showItem(item: PricelistItem) {
        titleTextView.text = item.Name
        descriptionTextView.text = "???"
        countTextView.text = item.Code
        untilTextView.text = "${item.ValidDays ?: "Бессрочно"}"
        if (item.ItemType == 0) {
            untilTextView.visibility = View.GONE
            untilHintTextView.visibility = View.GONE
        }
    }

    override fun startLoading() {
        buyButton.isEnabled = false
    }

    override fun finishLoading() {
        buyButton.isEnabled = true
    }

    override fun showError(clientMessage: String) {
        Toast.makeText(applicationContext, clientMessage, Toast.LENGTH_SHORT).show()
    }

    override fun success() {
        Toast.makeText(applicationContext, "Вы приобрели ${titleTextView.text}, можете перейти к оплате.", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pricelist_detail_activity)
        setSupportActionBar(toolbar)
        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = true
            internal var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.title = "Покупка"
                    isShow = true
                } else if (isShow) {
                    collapsingToolbarLayout.title = " "
                    isShow = false
                }
            }
        })
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}


class PricelistDetailActivity : BasePricelistDetailActivity(), PricelistItemView {


    companion object {
        const val pricelistItemTag = "pricelist:item"
    }

    @InjectPresenter
    lateinit var pricelistItemPresenter: PricelistItemPresenter

    @ProvidePresenter
    fun providePricelistItemPresenter(): PricelistItemPresenter = PricelistItemPresenter(intent.extras.getSerializable(pricelistItemTag) as PricelistItem)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        buyButton.setOnClickListener {
            pricelistItemPresenter.onBuyClicked()
        }
    }


}