package ru.workinprogress.fitpass.ui.fragment

import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import ru.workinprogress.fitpass.presentation.NotificationsPresenter
import ru.workinprogress.fitpass.presentation.NotificationsView

/**
 * Created by panic on 06.03.2018.
 */

class NotificationsFragment : MvpFragment(),NotificationsView {
    @InjectPresenter
    lateinit var notificationsPresenter: NotificationsPresenter
}