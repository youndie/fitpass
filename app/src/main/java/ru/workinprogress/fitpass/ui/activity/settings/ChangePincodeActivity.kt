package ru.workinprogress.fitpass.ui.activity.settings

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.change_pincode_activity.*
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.CreatePincodePresenter
import ru.workinprogress.fitpass.presentation.settings.ChangePincodePresenter
import ru.workinprogress.fitpass.presentation.settings.ChangePincodeView
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import ru.workinprogress.fitpass.ui.activity.PricelistActivity


class ChangePincodeActivity : MvpAppCompatActivity(), ChangePincodeView {
    override fun showRepeat() {
        hintTextView.text = "Повторите новый пин-код"
        pinEntryEditText.text.clear()
    }

    override fun showNewPin() {
        hintTextView.text = "Придумайте новый пин-код"
        pinEntryEditText.text.clear()
    }

    override fun showInvalidOldPin() {
        this.currentFocus?.let {
            (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(it.windowToken, 0)
        }
        hintTextView.text = "Введите старый пин-код"
        pinEntryEditText.text.clear()
        Snackbar.make(constraintLayout, "Неверный пинкод", Snackbar.LENGTH_LONG).show()
    }

    override fun success() {
        finish()
    }

    override fun showInvalidNewPin() {
        this.currentFocus?.let {
            (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(it.windowToken, 0)
        }
        Snackbar.make(constraintLayout, "", Snackbar.LENGTH_LONG).show()
    }

    @InjectPresenter
    lateinit var changePincodePresenter: ChangePincodePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_pincode_activity)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Пин-код"

        pinEntryEditText.setOnPinEnteredListener {
            if (it.length == 4) {
                changePincodePresenter.onPinEntered(it.toString())
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}