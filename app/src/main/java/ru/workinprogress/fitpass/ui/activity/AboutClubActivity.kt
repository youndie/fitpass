package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.arellomobile.mvp.MvpAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.withArguments
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.ui.fragment.AboutClubFragment

class AboutClubActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.about_club_activity)
        setSupportActionBar(toolbar)
        toolbar?.background?.alpha = 255
        supportActionBar?.title = "О клубе"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (savedInstanceState == null) {
            fragmentManager.beginTransaction().add(R.id.contentContainer, AboutClubFragment().withArguments(AboutClubFragment.clubTag to intent.getSerializableExtra(AboutClubFragment.clubTag))).commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}