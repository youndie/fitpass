package ru.workinprogress.fitpass.ui.activity

import android.Manifest
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.Html
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.URLSpan
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.jakewharton.rxbinding2.view.focusChanges
import com.jakewharton.rxbinding2.widget.textChanges
import kotlinx.android.synthetic.main.registration_activity.*
import org.jetbrains.anko.dip
import org.jetbrains.anko.find
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.*
import ru.workinprogress.fitpass.presentation.RegistrationPresenter
import ru.workinprogress.fitpass.presentation.RegistrationView
import ru.workinprogress.fitpass.utils.Validation
import java.util.*
import android.location.LocationManager
import android.location.LocationListener
import android.content.Context.LOCATION_SERVICE
import android.location.Geocoder
import android.location.Location
import com.tbruyelle.rxpermissions2.RxPermissions


/**
 * Created by panic on 22.02.2018.
 */

class RegistrationActivity : MvpAppCompatActivity(), RegistrationView {
    override fun showCity(city: City) {
        (citySpinner.adapter as? CitiesAdapter)?.let {
            citySpinner.setSelection(it.getPosition(city))
        }
    }

    override fun showError(clientMessage: String) {
        pinEntryEditText.text.clear()
        Toast.makeText(applicationContext, clientMessage, Toast.LENGTH_SHORT).show()
    }

    override fun showCodeLayout() {
        codeLayout.visibility = View.VISIBLE
        fieldsLayout.visibility = View.GONE
        supportActionBar?.elevation = 0f
        pinEntryEditText.text.clear()
        pinEntryEditText.requestFocus()
    }

    private var dialog: Dialog? = null

    override fun startLoading() {
        dialog = indeterminateProgressDialog(title = "Регистрация").apply {
            show()
        }
    }

    override fun showCheckResult(it: CheckAttributesResponse) {
        with(it) {
            when {
                LoginNotUnique -> {
                    loginInputLayout.error = "Логин уже занят"
                    loginInputLayout.editText?.requestFocus()
                }
                PhoneNotUnique -> {
                    phoneInputLayout.editText?.requestFocus()
                    phoneInputLayout.error = "Номер телефона уже занят"
                }
                else -> {
                }
            }
        }
    }

    override fun finishLoading() {
        dialog?.dismiss()
    }

    override fun showDocs(docs: LegacyDocuments) {
        contractTextView.text = Html.fromHtml("Согласен с " +
                "<a href='${docs.OfferURL}'>договором-офертой</a>");
        contractTextView.isClickable = true;
        contractTextView.movementMethod = LinkMovementMethod.getInstance()
        stripUnderlines(contractTextView)
        processingTextView.text = Html.fromHtml("Даю " +
                "<a href='${docs.PersonalDataProcessingAgreementURL}'>согласие на обработку персональных данных</a>");
        processingTextView.isClickable = true;
        processingTextView.movementMethod = LinkMovementMethod.getInstance()
        stripUnderlines(processingTextView)
    }

    private fun stripUnderlines(textView: TextView) {
        val s = SpannableString(textView.text)
        val spans = s.getSpans(0, s.length, URLSpan::class.java)
        for (span in spans) {
            val start = s.getSpanStart(span)
            val end = s.getSpanEnd(span)
            s.removeSpan(span)
            s.setSpan(URLSpanNoUnderline(span.url), start, end, 0)
        }
        textView.text = s
    }

    private inner class URLSpanNoUnderline(url: String) : URLSpan(url) {
        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }

    override fun success(needPincode: Boolean) {
        if (needPincode) {
            startActivity<CreatePincodeActivity>()
        } else {
            startActivity<SplashActivity>()
        }
        finish()
    }

    override fun showDate(date: String) {
        birthDayEditText.setText(date)
        dateExist = true
    }

    private var dateExist = false

    @InjectPresenter
    lateinit var registrationPresenter: RegistrationPresenter

    val name get() = nameEditText.text.toString()
    val login get() = loginInputLayout.editText?.text.toString()
    val password get() = passwordInputLayout.editText?.text.toString()
    val password2 get() = repeatPasswordEditText.text.toString()
    val email get() = emailInputLayout.editText?.text?.toString().orEmpty()
    val phone get() = phoneInputLayout.editText?.text.toString()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registration_activity)
        title = "Регистрация"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                makeUseOfNewLocation(location)
            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}

            override fun onProviderEnabled(provider: String) {}

            override fun onProviderDisabled(provider: String) {}
        }
        RxPermissions(this).request(Manifest.permission.ACCESS_FINE_LOCATION).async().subscribe({
            if (it) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, locationListener)
            }
        }, {})

        loginInputLayout.editText?.textChanges()?.skipInitialValue()?.async()?.subscribe({
            loginInputLayout.error = null
        })
        phoneInputLayout.editText?.textChanges()?.skipInitialValue()?.async()?.subscribe({
            phoneInputLayout.error = null
        })
        passwordInputLayout.editText?.focusChanges()?.skipInitialValue()?.async()?.subscribe({
            if (it) {
                passwordInputLayout.error = null
            } else {
                passwordInputLayout.error = if (Validation.isValidPassword(password)) null else getString(R.string.registration_easy_pass)
            }
        })
        repeatPasswordEditText?.textChanges()?.skipInitialValue()?.subscribe({
            repeatPasswordInputLayout.error = null
        })
        repeatPasswordEditText.focusChanges().skipInitialValue().async().subscribe({
            if (!it)
                if (password != password2) {
                    repeatPasswordInputLayout.error = getString(R.string.registration_diff_pass)
                }
        })
        birthDayEditText.setOnClickListener {
            val c = Calendar.getInstance()
            DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth -> registrationPresenter.onBirthDaySelected(year, monthOfYear, dayOfMonth) },
                    c.get(Calendar.YEAR) - 18,
                    c.get(Calendar.MONTH),
                    c.get(Calendar.DAY_OF_MONTH)).show()
        }

        pinEntryEditText.setOnPinEnteredListener {
            if (it.length == 6) {
                registrationPresenter.onCodeEntered(it.toString())
            }
        }

    }

    private fun makeUseOfNewLocation(location: Location) {
        val gcd = Geocoder(this, Locale("ru", "RU"))
        val addresses = gcd.getFromLocation(location.latitude, location.longitude, 1)
        if (addresses.size > 0) {
            registrationPresenter.onLocation(addresses.get(0).getLocality())
        } else {
            // do your stuff
        }
    }

    override fun onBackPressed() {
        if (codeLayout.visibility == View.VISIBLE) {
            codeLayout.visibility = View.GONE
            fieldsLayout.visibility = View.VISIBLE
            supportActionBar?.elevation = dip(4).toFloat()
        } else
            super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        if (item?.itemId == R.id.done && validateFields()) {
            registrationPresenter.onRegister(name, login, password, sexRadioGroup.checkedRadioButtonId == R.id.male, phone, email, pincodeSwitch.isChecked)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun validateFields(): Boolean {
        if (login.isEmpty()) {
            loginInputLayout.error = getString(R.string.registration_empty_login)
            return false
        }
        if (!Validation.isValidPassword(password)) {
            passwordInputLayout.error = getString(R.string.registration_easy_pass)
            return false
        }

        if (password != password2) {
            repeatPasswordInputLayout.error = getString(R.string.registration_diff_pass)
            return false
        }
        if (phone.isEmpty()) {
            phoneInputLayout.error = getString(R.string.registration_empty_phone)
            return false
        }
        if (sexRadioGroup.checkedRadioButtonId == -1) {
            Toast.makeText(applicationContext, "Укажите пол", Toast.LENGTH_SHORT).show()
            return false
        }
        if (!dateExist) {
            Toast.makeText(applicationContext, "Укажите дату рождения", Toast.LENGTH_SHORT).show()
            return false
        }

        if (!contractCheckBox.isChecked) {
            Toast.makeText(applicationContext, "Необходимо согласие с договором-офертой", Toast.LENGTH_SHORT).show()
            return false
        }
        if (!processingCheckBox.isChecked) {
            Toast.makeText(applicationContext, "Необходимо согласие на обработку персональных данных", Toast.LENGTH_SHORT).show()
            return false
        }


        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.registration, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun showLanguages(languages: Array<Language>) {
        languageSpinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, languages).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        languageSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, v: View?, p2: Int, p3: Long) {
                registrationPresenter.onLanguageSelected(languages[p2])
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }
    }

    override fun showCities(cities: Array<City>) {
        citySpinner.adapter = CitiesAdapter(cities)
        citySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, v: View?, p2: Int, p3: Long) {
                registrationPresenter.onCitySelected(cities[p2])
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }
    }


    private inner class CitiesAdapter(cities: Array<City>) : ArrayAdapter<City>(this, R.layout.city_spinner_item, cities) {
        private val specialCities = arrayOf("Москва", "Санкт-Петербург")
        private val specialCityCount = cities.filter { it.Name in specialCities }.size

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
            return onCreateViewHolder(convertView, parent, position)
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            return onCreateViewHolder(convertView, parent, position).apply {
                find<View>(R.id.divider).visibility = View.GONE
            }
        }

        fun onCreateViewHolder(convertView: View?, parent: ViewGroup?, position: Int): View {
            val itemView = convertView
                    ?: layoutInflater.inflate(R.layout.city_spinner_item, parent, false)

            itemView.find<TextView>(android.R.id.text1).text = getItem(position).Name
            itemView.find<View>(R.id.divider).visibility = if (position == specialCityCount) View.VISIBLE else View.GONE
            return itemView
        }
    }

    override fun showCountries(countries: Array<Country>) {
        countrySpinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, countries).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        countrySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, v: View?, p2: Int, p3: Long) {
                registrationPresenter.onCountrySelected(countries[p2])
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }
    }

}