package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.price_list_activity.*
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.ContractPriceItem
import ru.workinprogress.fitpass.api.serializers.PricelistItem
import ru.workinprogress.fitpass.presentation.ContractPriceListPresenter
import ru.workinprogress.fitpass.presentation.ContractPriceListView
import ru.workinprogress.fitpass.ui.adapter.ContractPricelistAdapter
import ru.workinprogress.fitpass.ui.adapter.PricelistAdapter

/**
 * Created by panic on 20.03.2018.
 */


class ContractPricelistActivity : MvpAppCompatActivity(), ContractPriceListView {
    override fun startLoading() {
        snackbar?.dismiss()
        recyclerView.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    override fun finishLoading() {
        recyclerView.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }

    override fun showPricelist(pricelist: Array<ContractPriceItem>) {
        adapter.data.clear()
        adapter.data.addAll(pricelist)
        adapter.notifyDataSetChanged()
    }

    var snackbar: Snackbar? = null

    override fun showError() {
        snackbar = Snackbar.make(parentLayout, R.string.no_internet_error, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(R.string.retry, {
                contractPriceListPresenter.load()
            })
        }
    }


    @InjectPresenter
    lateinit var contractPriceListPresenter: ContractPriceListPresenter

    val adapter get() = recyclerView.adapter as ContractPricelistAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.price_list_activity)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ContractPricelistAdapter {
            startActivity<ContractPricelistDetailActivity>(ContractPricelistDetailActivity.itemTag to it)
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Приобретение контрактов"
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}