package ru.workinprogress.fitpass.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.price_list_activity.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.startActivityForResult
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.PricelistItem
import ru.workinprogress.fitpass.presentation.LessonPricelistPresenter
import ru.workinprogress.fitpass.presentation.LessonPricelistView
import ru.workinprogress.fitpass.ui.adapter.PricelistAdapter

/**
 * Created by panic on 26.03.2018.
 */

class LessonPricelistActivity : MvpAppCompatActivity(), LessonPricelistView {

    companion object {
        const val lessonIdTag = "lessonId"
        const val lessonTypeIdTag = "lessonTypeId"
    }

    @InjectPresenter
    lateinit var lessonPricelistPresenter: LessonPricelistPresenter

    @ProvidePresenter
    fun providesLessonPricelistPresenter(): LessonPricelistPresenter {
        return LessonPricelistPresenter(intent.getStringExtra(lessonIdTag), intent.getStringExtra(lessonTypeIdTag))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Приобрести услугу"
        setContentView(R.layout.lesson_pricelist_activity)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = PricelistAdapter({
            startActivityForResult<LessonPricelistDetailActivity>(42, LessonPricelistDetailActivity.pricelistItemTag to it,
                    LessonPricelistDetailActivity.lessonIdTag to intent.getStringExtra(lessonIdTag),
                    LessonPricelistDetailActivity.lessonTypeIdTag to intent.getStringExtra(lessonTypeIdTag)
            )
        }, true)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 42 && resultCode == Activity.RESULT_OK) {
            setResult(resultCode)
            finish()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    val adapter get() = recyclerView.adapter as PricelistAdapter

    override fun showItems(items: List<PricelistItem>) {
        adapter.show(items)
    }

    override fun showError() {
    }

    override fun startLoading() {
    }

    override fun finishLoading() {
    }

}