package ru.workinprogress.fitpass.ui.fragment

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.settings_fragment.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.browse
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.LegacyDocuments
import ru.workinprogress.fitpass.presentation.SettingsPresenter
import ru.workinprogress.fitpass.presentation.SettingsView
import ru.workinprogress.fitpass.ui.activity.SplashActivity
import ru.workinprogress.fitpass.ui.activity.settings.*

/**
 * Created by panic on 15.03.2018.
 */

class SettingsFragment : MvpFragment(), SettingsView {
    override fun startPinActivity(pinEnabled: Boolean) {
        if (pinEnabled) {
            startActivity<PincodeMenuActivity>()
        } else {
            startActivity<ChangePincodeActivity>()
        }
    }

    override fun showDocs(it: LegacyDocuments) {
        val d = it
        tvOffer.setOnClickListener {
            browse(d.OfferURL)
        }
        tvProcessing.setOnClickListener {
            browse(d.PersonalDataProcessingAgreementURL)
        }
    }

    @InjectPresenter
    lateinit var settingsPresenter: SettingsPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.settings_fragment, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clubsClicker.setOnClickListener {
            startActivity<ClubsActivity>()
        }
        profileClicker.setOnClickListener {
            startActivity<ProfileSettingsActivity>()
        }
        pincodeClicker.setOnClickListener {
            settingsPresenter.onPinClicked()
        }
        cardsClicker.setOnClickListener {
            startActivity<CardsActivity>()
        }
        exitClicker.setOnClickListener {
            alert {
                title = "Выход"
                message = "Вы уверены, что хотите выйти?"
                positiveButton("Выход", {
                    it.dismiss()
                    settingsPresenter.onExit()
                    activity.finish()
                    activity.startActivity<SplashActivity>()

                })
                negativeButton("Отмена", {
                    it.dismiss()
                })
            }.show()
        }

    }
}