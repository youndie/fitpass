package ru.workinprogress.fitpass.ui.fragment

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.schedule_fragment.*
import kotlinx.android.synthetic.main.schedule_week_layout.view.*
import noman.weekcalendar.WeekCalendar
import noman.weekcalendar.adapter.PagerAdapter
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import org.joda.time.DateTime
import ru.workinprogress.cityfitness.ui.adapter.ScheduleWeekAdapter
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.SchedulePresenter
import ru.workinprogress.fitpass.presentation.ScheduleView
import ru.workinprogress.fitpass.presentation.UIScheduleItem
import ru.workinprogress.fitpass.presentation.UIWeek
import ru.workinprogress.fitpass.ui.activity.LessonDetailActivity
import java.util.concurrent.ScheduledExecutorService

/**
 * Created by panic on 06.03.2018.
 */

class ScheduleFragment : MvpFragment(), ScheduleView {
    override fun finishLoading() {
        swipeRefresh.isRefreshing = false
        progressBar.visibility = View.GONE
    }

    override fun startLoading() {
        viewPager.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    override fun showCurrentDay(date: DateTime, page: Int?) {
        weekCalendar.setSelectedDate(date)
        page?.takeIf { it != -1 }?.let { viewPager.currentItem = it }
    }

    override fun showSchedule(weeks: List<UIWeek>) {
        weekCalendar.setOnWeekChangeListener { firstDayOfTheWeek, _ ->
        }
        weekCalendar.eventsDays?.clear()
        weekCalendar.eventsDays?.addAll(weeks.map { it.dateTime })
        weekCalendar.updateUi()
        viewPager.adapter = ScheduleWeekAdapter(activity, weeks, {
            startActivity<LessonDetailActivity>(LessonDetailActivity.lessonIdTag to it.id, LessonDetailActivity.personTag to (tabLayout.selectedTabPosition == 1))
        })
        viewPager.visibility = View.VISIBLE
        weekCalendar.visibility = View.VISIBLE
    }

    @InjectPresenter
    lateinit var schedulePresenter: SchedulePresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.schedule_fragment, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipeRefresh.setColorSchemeResources(R.color.colorPrimary)
        swipeRefresh.setOnRefreshListener {
            schedulePresenter.update()
        }
        tabLayout.addTab(tabLayout.newTab().setText("Все тренировки"))
        tabLayout.addTab(tabLayout.newTab().setText("Запланированные"))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                schedulePresenter.onModeChanged(tab?.position == 0)
            }

        })
        weekCalendar.apply { setOnDateClickListener { schedulePresenter.onDateChanged(it.withTimeAtStartOfDay()) } }
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    (viewPager.adapter as ScheduleWeekAdapter).weeks[viewPager.currentItem].dateTime.let {
                        schedulePresenter.onDateChanged(it)
                    }
                }
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {

            }
        }
        )
    }

}