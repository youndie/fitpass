package ru.workinprogress.fitpass.ui.fragment

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.club_status_layout.*
import kotlinx.android.synthetic.main.clubs_activity.*
import kotlinx.android.synthetic.main.main_club_layout.view.*
import kotlinx.android.synthetic.main.main_fragment.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.*
import ru.workinprogress.fitpass.presentation.MainPresenter
import ru.workinprogress.fitpass.presentation.MainView
import ru.workinprogress.fitpass.presentation.UIScheduleItem
import ru.workinprogress.fitpass.ui.activity.*
import ru.workinprogress.fitpass.ui.adapter.NewsItemViewHolder

/**
 * Created by panic on 06.03.2018.
 */

class MainFragment : MvpFragment(), MainView {
    override fun finishAllUpdate() {
        fitpassSwipeRefresh.isRefreshing = false
    }

    override fun showLogo(uri: String?) {
        if (uri.isNullOrBlank()) {
            toolbarImageView.setImageDrawable(ColorDrawable(resources.getColor(R.color.colorPrimary)))
        } else {
            Picasso.get().load(uri).into(toolbarImageView)
        }
        toolbarLogoImageView.setImageDrawable(null)
    }


    override fun showLogo(uri: String?, logo: String?, fitpass: Boolean) {
        if (uri.isNullOrBlank()) {
            toolbarImageView.setImageDrawable(ColorDrawable(resources.getColor(R.color.colorPrimary)))
        } else {
            Picasso.get().load(uri).into(toolbarImageView)
        }
        if (logo.isNullOrBlank()) {
            if (!fitpass)
                toolbarLogoImageView.setImageDrawable(null)
            else
                toolbarLogoImageView.setImageResource(R.drawable.white_logo)

        } else {
            Picasso.get().load(logo).into(toolbarLogoImageView)
        }
    }


    override fun showClubNews(subList: List<SpecialOffer>) {
        clubSwipeRefresh.isRefreshing = false
        clubNewsContainer.visibility = View.VISIBLE
        clubNewsProgressBar.visibility = View.GONE

        subList.map { item ->
            LayoutInflater.from(activity).inflate(R.layout.news_item, clubNewsContainer, false).also {
                NewsItemViewHolder(it).bind(item)
                it.setOnClickListener {
                    startActivity<NewsDetailActivity>(NewsDetailActivity.newsDetailTag to item)
                }
            }
        }.forEach {
            clubNewsContainer.addView(it)
        }

        showClubMoreButton.visibility = View.GONE
    }

    override fun showContracts(contracs: Array<ContractStatus>) {
        contractsProgressBar.visibility = View.GONE
        contractsCard.visibility = View.VISIBLE
        contractsContainer.removeAllViews()

        contracs.map { contract ->
            LayoutInflater.from(activity).inflate(R.layout.contract_profile_item, contractsContainer, false).apply {
                find<TextView>(R.id.titleTextView).text = contract.Template
                find<TextView>(R.id.statusTextView).text = contract.Status
                find<ImageView>(R.id.imageView).setImageResource(when (contract.StatusCode) {
                    ContractStatus.openStatusCode -> R.drawable.ic_check_circle_black_24dp
                    ContractStatus.freezeStatusCode -> R.drawable.freeze
                    else -> R.drawable.alert
                })
                setOnClickListener {
                    startActivity<ContractDetailActivity>(ContractDetailActivity.contractTag to contract)
                }
            }
        }.forEach {
            contractsContainer.addView(it)
        }
    }


    override fun showFavTrainings(trainings: List<UIScheduleItem>) {
        trainingsContainer.visibility = View.VISIBLE
        trainingProgressBar.visibility = View.GONE

    }

    override fun showTodayTrainings(trainings: List<UIScheduleItem>) {
        trainingsContainer.removeAllViews()
        trainingsContainer.visibility = View.VISIBLE
        trainingProgressBar.visibility = View.GONE
//        if (trainings.isEmpty()) {
//            todayTrainingsHint.visibility = View.GONE
//        } else {
//            todayTrainingsHint.visibility = View.VISIBLE
//            trainings.map { item ->
//                LayoutInflater.from(activity).inflate(R.layout.day_item, trainingsContainer, false).apply {
//                    DayViewHolder(this).bind(item, {
//                        startActivity<LessonDetailActivity>(LessonDetailActivity.lessonIdTag to it.id, LessonDetailActivity.personTag to true)
//                    })
//                }
//            }.forEach {
//                trainingsContainer.addView(it)
//            }
//        }
    }

    override fun showContractsLoading() {
        contractsCard.visibility = View.GONE
        contractsProgressBar.visibility = View.VISIBLE
    }

    override fun showStatusLoading(b: Boolean) {
        if (b) clubStatusContainer.visibility = View.GONE
        clubStatusProgressBar.visibility = if (b) View.GONE else View.VISIBLE
    }

    override fun showClubs(clubs: Array<Club>, selectedClubId: String) {
        needUpdate = false

        val selectedTab = viewPager.currentItem

        configureAdapter(clubs.isEmpty().not())
        myClubsSpinner.onItemSelectedListener = null
        myClubsProgressBar.visibility = View.GONE
        myClubsContainer.removeAllViews()
        clubs.map { club ->
            activity.layoutInflater.inflate(R.layout.main_club_item, myClubsContainer, false).apply {
                find<TextView>(R.id.textView).text = club.Name
                if (club.logo != null) {
                    Picasso.get().load(club.logo?.Uri).into(find<ImageView>(R.id.imageView))
                    find<ImageView>(R.id.icon).setImageDrawable(null)
                } else {
                    find<ImageView>(R.id.imageView).setImageDrawable(ColorDrawable(resources.getColor(R.color.colorPrimary)))
                    club.background?.Uri?.let {
                        Picasso.get().load(it).into(find<ImageView>(R.id.imageView))
                    }
                    Picasso.get().load(club.icon?.Uri).into(find<ImageView>(R.id.icon))
                }
                setOnClickListener {
                    startActivity<AboutClubActivity>(AboutClubFragment.clubTag to club)
                }
            }
        }.forEach {
            myClubsContainer.addView(it)
        }
        myClubsContainer.visibility = View.VISIBLE

        myClubsSpinner.adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, clubs).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        myClubsSpinner.setSelection(clubs.indexOfFirst { it.ID == selectedClubId })
        myClubsSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                mainPresenter.onClubChanged(clubs[p2])
            }

        }
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                mainPresenter.onPageChanged(position == 0)
            }

        })
        viewPager.currentItem = selectedTab
    }

    override fun showClubsLoading() {
        myClubsProgressBar.visibility = View.VISIBLE
        myClubsContainer.visibility = View.GONE
    }

    override fun showFitpassNewsLoading() {
        fitPassNewsProgressBar.visibility = View.VISIBLE
        fitPassNewsContainer.visibility = View.GONE
    }

    override fun showNews(it: MutableList<SpecialOffer>) {
        fitPassNewsProgressBar.visibility = View.GONE
        fitPassNewsContainer.removeAllViews()
        fitPassNewsContainer.visibility = View.VISIBLE

        it.map { item ->
            LayoutInflater.from(activity).inflate(R.layout.news_item, fitPassNewsContainer, false).also {
                NewsItemViewHolder(it).bind(item)
                it.setOnClickListener {
                    startActivity<NewsDetailActivity>(NewsDetailActivity.newsDetailTag to item)
                }
            }
        }.forEach {
            fitPassNewsContainer.addView(it)
        }

        showFitPassMoreButton.visibility = View.GONE
    }

    override fun showTrainingsLoading() {
        trainingProgressBar.visibility = View.VISIBLE
        trainingsContainer.visibility = View.GONE
    }

    override fun showClubNewsLoading() {
        clubNewsProgressBar.visibility = View.VISIBLE
        clubNewsContainer.visibility = View.GONE
    }

    @InjectPresenter
    lateinit var mainPresenter: MainPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    companion object {
        var needUpdate = false
    }

    override fun onResume() {
        super.onResume()
        if (needUpdate) {
            mainPresenter.refreshAll()
        }
    }

    val fitpassView by lazy { LayoutInflater.from(activity).inflate(R.layout.main_fitpass_layout, null) }
    val clubView by lazy { LayoutInflater.from(activity).inflate(R.layout.main_club_layout, null) }

    val fitpassSwipeRefresh by lazy { fitpassView.swipeRefresh }
    val clubSwipeRefresh by lazy { clubView.swipeRefresh }

    val myClubsContainer by lazy { fitpassView.find<LinearLayout>(R.id.myClubsContainer) }
    val myClubsProgressBar by lazy { fitpassView.find<ProgressBar>(R.id.myClubsProgressBar) }
    val addClubButton by lazy { fitpassView.find<Button>(R.id.addClubButton) }
    val fitPassNewsContainer by lazy { fitpassView.find<LinearLayout>(R.id.fitPassNewsContainer) }
    val fitPassNewsProgressBar by lazy { fitpassView.find<ProgressBar>(R.id.fitPassNewsProgressBar) }
    val showFitPassMoreButton by lazy { fitpassView.find<Button>(R.id.showFitPassMoreButton) }

    val myClubsSpinner by lazy { clubView.find<Spinner>(R.id.myClubsSpinner) }
    val notificationsContainer by lazy { clubView.find<LinearLayout>(R.id.notificationsContainer) }
    val trainingProgressBar by lazy { clubView.find<ProgressBar>(R.id.trainingsProgressBar) }
    val trainingsContainer by lazy { clubView.find<LinearLayout>(R.id.trainingsContainer) }
    val todayTrainingsContainer by lazy { clubView.find<LinearLayout>(R.id.todayTrainingsContainer) }
    val favTrainingsContainer by lazy { clubView.find<LinearLayout>(R.id.favTrainingsContainer) }
    val todayTrainingsHint by lazy { clubView.find<TextView>(R.id.todayTrainingsHint) }
    val favTrainingsHint by lazy { clubView.find<TextView>(R.id.favTrainingsHint) }
    val clubNewsContainer by lazy { clubView.find<LinearLayout>(R.id.clubNewsContainer) }
    val clubNewsProgressBar by lazy { clubView.find<ProgressBar>(R.id.clubNewsProgressBar) }
    val showClubMoreButton by lazy { clubView.find<Button>(R.id.showClubMoreButton) }
    val contractsContainer by lazy { clubView.find<LinearLayout>(R.id.clubContractsContainer) }
    val contractsCard by lazy { clubView.find<CardView>(R.id.clubContractsCardContainer) }

    val contractsProgressBar by lazy { clubView.find<ProgressBar>(R.id.contractsProgressBar) }
    val clubStatusContainer by lazy { clubView.find<LinearLayout>(R.id.clubStatusContainer) }
    val clubStatusProgressBar by lazy { clubView.find<ProgressBar>(R.id.clubStatusProgressBar) }
    val statusContractsTextView by lazy { clubView.find<TextView>(R.id.statusContractsTextView) }
    val statusAccessTextView by lazy { clubView.find<TextView>(R.id.statusAccessTextView) }
    val statusDebtsTextView by lazy { clubView.find<TextView>(R.id.statusDebtsTextView) }
    val statusBlocksTextView by lazy { clubView.find<TextView>(R.id.statusBlocksTextView) }
    val statusPayTextView by lazy { clubView.find<TextView>(R.id.statusPayTextView) }

    override fun showClubStatus(contracts: Boolean, access: Boolean, debts: Boolean, blocks: Boolean, needPay: ContractStatus?) {
        clubStatusContainer.visibility = View.VISIBLE
        fun TextView.configure(boolean: Boolean, right: Boolean = true) {
            setCompoundDrawablesWithIntrinsicBounds(if (boolean) R.drawable.ic_check_circle_blue_24dp else R.drawable.ic_error_red_24dp, 0, if (right) R.drawable.chevron_right else 0, 0)
        }

        statusContractsTextView.text = if (contracts) "Есть активные контракт" else "Нет активных контрактов"
        statusContractsTextView.configure(contracts, false)

        statusAccessTextView.text = if (access) "Доступ в клуб открыт" else "Доступ в клуб закрыт"
        statusAccessTextView.configure(access, false)

        statusDebtsTextView.text = if (debts) "Имеются задолженности" else "Нет задолженностей"
        statusDebtsTextView.configure(!debts, debts)
        statusDebtsTextView.setOnClickListener {
            if (debts) {
                MainActivity.MainNavigation.router.navigateTo(MainActivity.MainNavigation.debts)
            }
        }

        statusBlocksTextView.text = if (blocks) "Имеются приобретенные блоки" else "Приобретенные блоки отстутсвуют"
        statusBlocksTextView.configure(blocks, blocks)
        statusBlocksTextView.setOnClickListener {
            if (blocks) {
                MainActivity.MainNavigation.router.navigateTo(MainActivity.MainNavigation.blocks)
            }
        }

        statusPayTextView.text = if (needPay != null) "Требуется оплата контракта" else "Оплата контрактов не требуется"
        statusPayTextView.configure(needPay == null, needPay != null)
        statusPayTextView.setOnClickListener {
            if (needPay != null) {
                startActivity<ContractDetailActivity>(ContractDetailActivity.contractTag to needPay)
            }
        }
        clubStatusProgressBar.visibility = View.GONE

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addClubButton.setOnClickListener {
            activity.startActivity<AddClubActivity>()
        }

        showFitPassMoreButton.setOnClickListener {
            fitPassNewsProgressBar.visibility = View.VISIBLE
            mainPresenter.onFitPassNewsMoreClicked()
        }
        toolbarImageView.setImageDrawable(ColorDrawable(resources.getColor(R.color.colorPrimary)))
        fitpassSwipeRefresh.setColorSchemeResources(R.color.colorPrimary)
        fitpassSwipeRefresh.setOnRefreshListener {
            mainPresenter.refreshAll()
        }
        clubSwipeRefresh.setColorSchemeResources(R.color.colorPrimary)
        clubSwipeRefresh.setOnRefreshListener {
            mainPresenter.refreshClub()
        }
    }

    fun configureAdapter(hasClub: Boolean) {
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
            }
        })

        viewPager.adapter = object : PagerAdapter() {

            override fun instantiateItem(container: ViewGroup, position: Int): Any {
                val inflated = if (position == 0) fitpassView else clubView
                container.addView(inflated)
                return inflated
            }

            override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
                container.removeView(`object` as View)
            }

            override fun isViewFromObject(view: View, `object`: Any): Boolean {
                return `object` === view
            }

            override fun getCount(): Int = if (hasClub) 2 else 1

            override fun getPageTitle(position: Int): CharSequence? {
                return if (position == 0) "Fitpass" else "Активный клуб"
            }

        }
        tabs.setupWithViewPager(viewPager)
    }


}