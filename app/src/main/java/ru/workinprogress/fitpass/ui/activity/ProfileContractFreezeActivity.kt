package ru.workinprogress.fitpass.ui.activity

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.contract_freeze_activity.*
import org.jetbrains.anko.indeterminateProgressDialog
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.ContractStatus
import ru.workinprogress.fitpass.presentation.ProfileContractFreezePresenter
import ru.workinprogress.fitpass.presentation.ProfileContractFreezeView
import ru.workinprogress.fitpass.ui.activity.ContractDetailActivity.Companion.contractTag
import ru.workinprogress.fitpass.utils.Format.targetYMDFormat
import java.util.*

/**
 * Created by panic on 06.03.2018.
 */

class ProfileContractFreezeActivity : MvpAppCompatActivity(), ProfileContractFreezeView {
    override fun success() {
        Toast.makeText(applicationContext,"Заявка на заморозку успешна принята",Toast.LENGTH_LONG).show()
        finish()
    }

    var progressDialog: Dialog? = null


    override fun endFreeze() {
        progressDialog?.dismiss()
    }

    override fun showError(clientMessage: String) {
        Toast.makeText(applicationContext, clientMessage, Toast.LENGTH_LONG).show()
    }

    override fun startFreeze() {
        progressDialog = indeterminateProgressDialog(title = "Заморозка").apply {
            show()
        }
    }

    override fun show(start: Date, end: Date, canSuspendDays: Int) {
        dateStartTextView.text = targetYMDFormat.format(start)
        dateEndTextView.text = targetYMDFormat.format(end)
        daysTextView.text = canSuspendDays.toString()
        dateStartTextView.setOnClickListener {
            val c = Calendar.getInstance().apply { time = start }
            DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth -> profileContractFreezePresenter.onStartDate(year, monthOfYear, dayOfMonth) },
                    c.get(Calendar.YEAR),
                    c.get(Calendar.MONTH),
                    c.get(Calendar.DAY_OF_MONTH)).apply {
                datePicker.minDate = Calendar.getInstance().apply { add(Calendar.DAY_OF_YEAR, 1) }.time.time
            }.show()
        }
        dateEndTextView.setOnClickListener {
            val c = Calendar.getInstance().apply { time = end }
            DatePickerDialog(this,
                    DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth -> profileContractFreezePresenter.onEndDate(year, monthOfYear, dayOfMonth) },
                    c.get(Calendar.YEAR),
                    c.get(Calendar.MONTH),
                    c.get(Calendar.DAY_OF_MONTH)).apply {
                datePicker.minDate = Calendar.getInstance().apply { time = start; add(Calendar.WEEK_OF_YEAR, 1) }.time.time
                datePicker.maxDate = Calendar.getInstance().apply { time = start; add(Calendar.DAY_OF_YEAR, canSuspendDays ) }.time.time
            }.show()
        }
    }

    @InjectPresenter
    lateinit var profileContractFreezePresenter: ProfileContractFreezePresenter

    @ProvidePresenter
    fun provideProfileContractFreezePresenter(): ProfileContractFreezePresenter {
        return ProfileContractFreezePresenter(intent.getSerializableExtra(contractTag) as ContractStatus)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contract_freeze_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Заморозить"
        supportActionBar?.elevation = 0f
        freezeButton.setOnClickListener {
            profileContractFreezePresenter.onFreeze()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}