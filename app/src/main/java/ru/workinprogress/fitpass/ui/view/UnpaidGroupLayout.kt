package ru.workinprogress.fitpass.ui.view

import android.app.Dialog
import android.content.Context
import android.support.transition.TransitionManager
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.arellomobile.mvp.MvpDelegate
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.unpaid_group_layout.view.*
import org.jetbrains.anko.browse
import org.jetbrains.anko.find
import org.jetbrains.anko.indeterminateProgressDialog
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.Account
import ru.workinprogress.fitpass.api.serializers.UnpaidService
import ru.workinprogress.fitpass.domain.CardsInfo
import ru.workinprogress.fitpass.presentation.UnpaidGroupPresenter
import ru.workinprogress.fitpass.presentation.UnpaidGroupView
import ru.workinprogress.fitpass.ui.activity.MainActivity
import ru.workinprogress.fitpass.utils.rouble
import kotlin.math.roundToInt

/**
 * Created by panic on 23.03.2018.
 */

class UnpaidGroupLayout @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr), UnpaidGroupView {
    override fun accountsLoading(show: Boolean) {
        payButton.visibility = if (show) View.GONE else View.VISIBLE
        progressBar.visibility = if (show) View.VISIBLE else View.GONE
    }

    var progressDialog: Dialog? = null

    override fun startPay() {
        globalBottomContainer.visibility = View.GONE
        globalDim.visibility = View.GONE
        progressDialog = context.indeterminateProgressDialog(title = "Оплата").apply {
            show()
        }
    }

    override fun finishPay() {
        progressDialog?.dismiss()
    }

    override fun paySuccess() {

    }


    override fun showPaymentPage(page: String) {
        context.browse(page)
    }

    override fun showError(clientMessage: String) {
        Toast.makeText(context, clientMessage, Toast.LENGTH_LONG).show()
    }

    override fun showPayVariants(cards: CardsInfo, accounts: List<Account>) {
        globalBottomContainer.removeAllViews()
        accounts.map { account ->
            LayoutInflater.from(context).inflate(R.layout.button_panel_item, globalBottomContainer, false).apply {
                find<TextView>(R.id.button).text = "Используя счёт \"${account.ScopeName}\" (${account.Amount.rouble()})"
                setOnClickListener {
                    unpaidGroupPresenter.onAccountPayClicked(account)
                }
            }
        }.forEach {
            globalBottomContainer.addView(it)
        }
        cards.list.map { card ->
            LayoutInflater.from(context).inflate(R.layout.button_panel_item, globalBottomContainer, false).apply {
                find<TextView>(R.id.button).text = "Используя карту ${card.Name}"
                setOnClickListener {
                    unpaidGroupPresenter.onCardPayClicked(card)
                }
            }
        }.forEach {
            globalBottomContainer.addView(it)
        }
        if (cards.newCard)
            globalBottomContainer.addView(LayoutInflater.from(context).inflate(R.layout.button_panel_item, globalBottomContainer, false).apply {
                find<TextView>(R.id.button).text = "Используя новую карту"
                setOnClickListener {
                    unpaidGroupPresenter.onNewCardPayClicked()
                }
            })
        TransitionManager.beginDelayedTransition(globalDim.parent as ViewGroup)
        globalBottomContainer.visibility = View.VISIBLE
        globalDim.visibility = View.VISIBLE
    }

    override fun showSum(sum: Int) {
        sumTextView.text = sum.rouble()
        payButton.isEnabled = sum != 0
    }

    val globalDim by lazy { (context as MainActivity).find<View>(R.id.globalDim) }
    val globalBottomContainer by lazy { (context as MainActivity).find<LinearLayout>(R.id.globalBottomContainer) }

    override fun showItems(unpaid: List<UnpaidService>) {
        itemsContainer.removeAllViews()
        unpaid.map { item ->
            LayoutInflater.from(context).inflate(R.layout.unpaid_item, itemsContainer, false).apply {
                find<CheckBox>(R.id.checkbox).text = item.Name
                find<CheckBox>(R.id.checkbox).setOnCheckedChangeListener { compoundButton, b ->
                    unpaidGroupPresenter.onCheckChange(item, b)
                }
                find<CheckBox>(R.id.checkbox).isChecked = true
                find<TextView>(R.id.count).text = item.Qty.toFloat().roundToInt().toString()
                find<TextView>(R.id.price).text = item.Price.rouble()
            }
        }.forEach {
            itemsContainer.addView(it)
        }
    }

    @InjectPresenter
    lateinit var unpaidGroupPresenter: UnpaidGroupPresenter

    @ProvidePresenter
    fun providesUnpaidGroupPresenter() = UnpaidGroupPresenter(tag as String)

    private var mParentDelegate: MvpDelegate<*>? = null

    fun init(parentDelegate: MvpDelegate<*>, name: String) {
        View.inflate(context, R.layout.unpaid_group_layout, this)
        mParentDelegate = parentDelegate
        mvpDelegate.onCreate()
        mvpDelegate.onAttach()
        nameTextView.text = name
        expandClicker.setOnClickListener {
            if (expandLayout.visibility == View.VISIBLE) {
                expandLayout.visibility = View.GONE
                expandImageView.setImageResource(R.drawable.ic_expand_more_black_24dp)
            } else {
                expandLayout.visibility = View.VISIBLE
                expandImageView.setImageResource(R.drawable.ic_expand_less_black_24dp)
            }
        }
        payButton.setOnClickListener {
            unpaidGroupPresenter.onPayClicked()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        mvpDelegate.onSaveInstanceState()
        mvpDelegate.onDetach()
    }

    val mvpDelegate by lazy {
        MvpDelegate(this).apply {
            setParentDelegate(mParentDelegate, tag as String)
        }
    }


}

