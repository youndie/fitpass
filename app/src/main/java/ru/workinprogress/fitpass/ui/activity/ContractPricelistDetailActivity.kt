package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.view.MenuItem
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.contract_pricelist_detail_activity.*
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.ContractPriceItem
import ru.workinprogress.fitpass.presentation.ContractPricelistItemPresenter
import ru.workinprogress.fitpass.presentation.ContractPricelistItemView
import ru.workinprogress.fitpass.utils.rouble
import kotlin.math.roundToInt

/**
 * Created by panic on 20.03.2018.
 */

class ContractPricelistDetailActivity : MvpAppCompatActivity(), ContractPricelistItemView {
    override fun startBuying() {
        buyButton.isEnabled = false
    }

    override fun finishBuying() {
        buyButton.isEnabled = true
    }

    override fun success() {
        Toast.makeText(applicationContext, "Контракт приобретён", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun showError(clientMessage: String) {
        Toast.makeText(applicationContext, clientMessage, Toast.LENGTH_SHORT).show()
    }

    @InjectPresenter
    lateinit var contractPricelistDetailPresenter: ContractPricelistItemPresenter

    @ProvidePresenter
    fun provideContractPricelistItemPresenter(): ContractPricelistItemPresenter = ContractPricelistItemPresenter(intent.getSerializableExtra(itemTag) as ContractPriceItem)

    companion object {
        const val itemTag = "item:tag"
    }


    override fun showItem(contractPriceItem: ContractPriceItem) {
        titleTextView.text = contractPriceItem.GroupName
        priceTextView.text = contractPriceItem.Prices.first { it.AllowIndependentSale }.Value.roundToInt().rouble()
        untilTextView.text = contractPriceItem.interval(resources)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contract_pricelist_detail_activity)
        setSupportActionBar(toolbar)
        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = true
            internal var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.title = "Контракты"
                    isShow = true
                } else if (isShow) {
                    collapsingToolbarLayout.title = " "
                    isShow = false
                }
            }
        })
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        buyButton.setOnClickListener {
            contractPricelistDetailPresenter.onBuyClicked()
        }
    }

}