package ru.workinprogress.fitpass.ui.activity

import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import org.jetbrains.anko.newTask
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.presentation.SplashPresenter
import ru.workinprogress.fitpass.presentation.SplashView

class SplashActivity : MvpAppCompatActivity(), SplashView {
    override fun startPinActivity() {
        startActivity(Intent(this, PinActivity::class.java).newTask())
        finish()
    }

    override fun startEnterActivity() {
        startActivity(Intent(this, EnterActivity::class.java).newTask())
        finish()
    }

    override fun startMainActivity() {
        startActivity(Intent(this, MainActivity::class.java).newTask())
        finish()
    }

    @InjectPresenter
    lateinit var splashPresenter: SplashPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
    }

}