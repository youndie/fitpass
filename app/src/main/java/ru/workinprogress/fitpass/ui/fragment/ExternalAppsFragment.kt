package ru.workinprogress.fitpass.ui.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.external_apps_fragment.*
import org.jetbrains.anko.browse
import org.jetbrains.anko.toast
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.ExternalSystemURI
import ru.workinprogress.fitpass.presentation.ExternalAppsPresenter
import ru.workinprogress.fitpass.presentation.ExternalAppsView
import ru.workinprogress.fitpass.ui.adapter.ExternalAppsAdapter

class ExternalAppsFragment : MvpFragment(), ExternalAppsView {

    @InjectPresenter
    lateinit var externalAppsPresenter: ExternalAppsPresenter

    override fun startLoading() {
        progressBar.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
        empty.visibility = View.GONE
    }

    override fun finishLoading() {
        progressBar.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
    }

    override fun showLinks(externalSystemURI: Array<ExternalSystemURI>?) {
        if (externalSystemURI?.isNotEmpty() == true) {
            adapter.show(externalSystemURI)
        } else {
            recyclerView.visibility = View.GONE
            empty.visibility = View.VISIBLE
        }
    }

    private val adapter by lazy {
        ExternalAppsAdapter {
            if (it.URI.contains("http://") || it.URI.contains("https://")) {
                browse(it.URI)
            } else {
                activity.packageManager.getLaunchIntentForPackage(it.URI)?.let(::startActivity)
                        ?: toast("Приложение не установлено")
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.external_apps_fragment, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter
    }
}