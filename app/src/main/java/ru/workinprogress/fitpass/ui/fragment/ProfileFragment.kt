package ru.workinprogress.fitpass.ui.fragment

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.jakewharton.rxbinding2.widget.RxTextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.profile_fragment.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.*
import ru.workinprogress.fitpass.api.serializers.ContractStatus.Companion.freezeStatusCode
import ru.workinprogress.fitpass.api.serializers.ContractStatus.Companion.openStatusCode
import ru.workinprogress.fitpass.presentation.ProfilePresenter
import ru.workinprogress.fitpass.presentation.ProfileView
import ru.workinprogress.fitpass.ui.activity.AccountsActivity
import ru.workinprogress.fitpass.ui.activity.ContractDetailActivity
import ru.workinprogress.fitpass.ui.activity.OperationsActivity
import ru.workinprogress.fitpass.ui.adapter.VisitsAdapter
import ru.workinprogress.fitpass.utils.rouble

/**
 * Created by panic on 05.03.2018.
 */


class ProfileFragment : MvpFragment(), ProfileView {
    override fun accountsLoaded() {
        accountsProgressBar.visibility = View.GONE
    }

    override fun showEmptyClubs() {
        scrollView.visibility = View.INVISIBLE
        clubsHint.visibility = View.INVISIBLE
        clubsSpinner.visibility = View.INVISIBLE
        clubBackground.setImageDrawable(ColorDrawable(resources.getColor(R.color.colorPrimary)))
    }

    override fun showClub(clubs: Array<Club>, club: Club) {
        scrollView.visibility = View.VISIBLE
        clubsHint.visibility = View.VISIBLE
        clubsSpinner.visibility = View.VISIBLE
        clubsSpinner.onItemSelectedListener = null
        clubsSpinner.adapter = ArrayAdapter(activity, R.layout.spinner_item, clubs).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        clubsSpinner.setSelection(clubs.indexOfFirst { it.ID == club.ID })
        clubsSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                profilePresenter.onClubChanged(clubs[p2])
            }
        }
        Picasso.get().load(club.Media.firstOrNull { it.LayoutName == "Background_Square" }?.Uri).into(clubBackground)
    }

    override fun onResume() {
        super.onResume()
        profilePresenter.updateContracts()
    }

    override fun startLoading() {
        contractsContainer.removeAllViews()
        contractsProgressBar.visibility = View.VISIBLE
        billContainer.removeAllViews()
        accountsProgressBar.visibility = View.VISIBLE
        visitsProgressBar.progress = View.VISIBLE
    }

    override fun showContracts(contracts: Array<ContractStatus>) {
        contractsProgressBar.visibility = View.GONE
        contractsContainer.removeAllViews()
        contracts.map {
            val contract = it
            LayoutInflater.from(activity).inflate(R.layout.contract_profile_item, contractsContainer, false).apply {
                find<TextView>(R.id.titleTextView).text = it.Template
                find<TextView>(R.id.statusTextView).text = it.Status
                find<ImageView>(R.id.imageView).setImageResource(when (it.StatusCode) {
                    openStatusCode -> R.drawable.ic_check_circle_black_24dp
                    freezeStatusCode -> R.drawable.freeze
                    else -> R.drawable.alert
                })
                setOnClickListener {
                    startActivity<ContractDetailActivity>(ContractDetailActivity.contractTag to contract)
                }
            }
        }.forEach {
            contractsContainer.addView(it)
        }
    }

    override fun showVisits(visits: Array<Visit>) {
        visitsProgressBar.visibility = View.GONE
        visitsCardView.visibility = if (visits.isEmpty()) View.GONE else View.VISIBLE
        visitsAdapter.data.clear()
        visitsAdapter.data.addAll(visits)
        visitsAdapter.notifyDataSetChanged()
    }

    override fun showAccounts(accounts: Array<Account>) {
        billContainer.removeAllViews()
        billContainer.setOnClickListener {
            startActivity<AccountsActivity>()
        }
        accounts.map {
            val account = it
            LayoutInflater.from(activity).inflate(R.layout.bill_profile_item, billContainer, false).apply {
                find<TextView>(R.id.billNameTextView).text = account.ScopeName
                find<TextView>(R.id.billCountTextView).text = account.Amount.rouble()
            }
        }.forEach {
            billContainer.addView(it)
        }

    }

    override fun showPerson(person: Person) {
        nameTextView.text = person.NickName
    }

    @InjectPresenter
    lateinit var profilePresenter: ProfilePresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.profile_fragment, container, false)
    }

    val visitsAdapter get() = visitsRecyclerView.adapter as VisitsAdapter

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        operations.setOnClickListener {
            startActivity(Intent(activity, OperationsActivity::class.java))
        }
        visitsRecyclerView.layoutManager = LinearLayoutManager(activity)
        visitsRecyclerView.adapter = VisitsAdapter()

    }

}