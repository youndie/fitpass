package ru.workinprogress.fitpass.ui.activity

import android.app.Fragment
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.jakewharton.rxbinding2.view.enabled
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import kotlinx.android.synthetic.main.login_activity.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.newTask
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.withArguments
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.FragmentNavigator
import ru.terrakok.cicerone.commands.Command
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.LoginPresenter
import ru.workinprogress.fitpass.presentation.LoginView
import ru.workinprogress.fitpass.utils.Validation


/**
 * Created by panic on 22.02.2018.
 */

class LoginActivity : MvpAppCompatActivity(), LoginView {
    override fun showLoginError(clientMessage: String) {
        Toast.makeText(this, clientMessage, Toast.LENGTH_SHORT).show()
        arrayOf(loginPasswordEditText, phonePasswordEditText, emailPasswordEditText).forEach { it.text.clear() }
    }



    override fun success() {
        startActivity(Intent(this, MainActivity::class.java).newTask().clearTask())
        finish()
    }

    override fun startLogin() {
        arrayOf(emailEditText, loginEditText, phoneEditText, loginPasswordEditText, phonePasswordEditText, emailPasswordEditText, emailLoginButton, loginLoginButton, phoneLoginButton).forEach { it.isEnabled = false }
    }

    override fun finishLogin() {
        arrayOf(emailEditText, loginEditText, phoneEditText, loginPasswordEditText, phonePasswordEditText, emailPasswordEditText, emailLoginButton, loginLoginButton, phoneLoginButton).forEach { it.isEnabled = true }
    }

    @InjectPresenter
    lateinit var loginPresenter: LoginPresenter

    fun showRecoverPanel(visible: Boolean) {
        arrayOf(dim, recoverPasswordButton, enterSmsButton).forEach { it.visibility = if (visible) View.VISIBLE else View.GONE }
    }

    override fun onBackPressed() {
        if (dim.visibility == View.VISIBLE) {
            showRecoverPanel(false)
        } else
            super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
        setSupportActionBar(toolbar)
        supportActionBar?.elevation = 0f
        title = "Вход"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        arrayOf(emailExpandLayout, loginExpandLayout, phoneExpandLayout, dim, recoverPasswordButton, enterSmsButton).forEach { it.visibility = View.GONE }
        arrayOf(emailExpandIcon, loginExpandIcon, phoneExpandIcon).forEach { it.setImageResource(R.drawable.ic_expand_more_white_24dp) }
        expand(phoneExpandIcon, phoneExpandLayout, true)
        dim.setOnClickListener { showRecoverPanel(false) }
        arrayOf(emailRecoverButton, loginRecoverButton, phoneRecoverButton).forEach {
            it.setOnClickListener {
                showRecoverPanel(true)
            }
        }
        Observable.combineLatest(phoneEditText.textChanges().map { it.isNotEmpty() }, phonePasswordEditText.textChanges().map { it.isNotEmpty() }, BiFunction<Boolean, Boolean, Boolean> { t1, t2 -> t1 && t2 }).subscribe(phoneLoginButton.enabled())
        Observable.combineLatest(loginEditText.textChanges().map { it.isNotEmpty() }, loginPasswordEditText.textChanges().map { it.isNotEmpty() }, BiFunction<Boolean, Boolean, Boolean> { t1, t2 -> t1 && t2 }).subscribe(loginLoginButton.enabled())
        Observable.combineLatest(emailEditText.textChanges().map { it.isNotEmpty() }, emailPasswordEditText.textChanges().map { it.isNotEmpty() }, BiFunction<Boolean, Boolean, Boolean> { t1, t2 -> t1 && t2 }).subscribe(emailLoginButton.enabled())

        recoverPasswordButton.setOnClickListener {
            showRecoverPanel(false)
            startActivity<PasswordRecoveryActivity>()
        }
        enterSmsButton.setOnClickListener {
            showRecoverPanel(false)
            startActivity<LoginPhoneActivity>()
        }

        phoneExpandClicker.setOnClickListener {
            expand(phoneExpandIcon, phoneExpandLayout, true)
            expand(emailExpandIcon, emailExpandLayout, false)
            expand(loginExpandIcon, loginExpandLayout, false)
        }
        loginExpandClicker.setOnClickListener {
            expand(phoneExpandIcon, phoneExpandLayout, false)
            expand(emailExpandIcon, emailExpandLayout, false)
            expand(loginExpandIcon, loginExpandLayout, true)
        }
        emailExpandClicker.setOnClickListener {
            expand(phoneExpandIcon, phoneExpandLayout, false)
            expand(emailExpandIcon, emailExpandLayout, true)
            expand(loginExpandIcon, loginExpandLayout, false)
        }
        loginLoginButton.setOnClickListener {
            loginPresenter.onLoginLogin(loginEditText.text.toString(), loginPasswordEditText.text.toString())
        }
        emailLoginButton.setOnClickListener {
            loginPresenter.onEmailLogin(emailEditText.text.toString(), emailPasswordEditText.text.toString())
        }
        phoneLoginButton.setOnClickListener {
            loginPresenter.onPhoneLogin(phoneEditText.text.toString(), phonePasswordEditText.text.toString())
        }
    }

    fun expand(iconView: ImageView, layout: View, expand: Boolean) {
        iconView.setImageResource(if (expand) R.drawable.ic_expand_less_white_24dp else R.drawable.ic_expand_more_white_24dp)
        layout.visibility = if (expand) View.VISIBLE else View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }


}