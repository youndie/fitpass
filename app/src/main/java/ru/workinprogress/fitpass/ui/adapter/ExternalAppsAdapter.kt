package ru.workinprogress.fitpass.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.external_app_item.view.*
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.ExternalSystemURI

class ExternalAppsAdapter(private val clickListener: (ExternalSystemURI) -> Unit) : RecyclerView.Adapter<ExternalAppViewHolder>() {

    private val data = mutableListOf<ExternalSystemURI>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExternalAppViewHolder = ExternalAppViewHolder(parent)

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ExternalAppViewHolder, position: Int) {
        holder.itemView.textView.text = data[position].DisplayName
        holder.itemView.setOnClickListener {
            clickListener(data[position])
        }
    }

    fun show(externalSystemURI: Array<ExternalSystemURI>) {
        data.clear()
        data.addAll(externalSystemURI)
        notifyDataSetChanged()
    }


}


class ExternalAppViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.external_app_item, parent, false)) {

}