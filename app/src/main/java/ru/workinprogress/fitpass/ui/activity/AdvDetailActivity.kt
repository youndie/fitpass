package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v4.view.PagerAdapter
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.news_detail_activity.*
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.Advertisement
import ru.workinprogress.fitpass.presentation.AdvertisementPresenter
import ru.workinprogress.fitpass.presentation.AdvertisementView

class AdvDetailActivity : MvpAppCompatActivity(), AdvertisementView {
    override fun showAdv(advertisement: Advertisement) {
        if (advertisement.Media.isEmpty()) {
            viewPager.visibility = View.GONE
        } else {
            viewPager.adapter = object : PagerAdapter() {

                override fun instantiateItem(container: ViewGroup, position: Int): Any {
                    val imageView = layoutInflater.inflate(R.layout.news_image_item, null, false) as ImageView
                    Picasso.get().load(advertisement.Media[position].Uri).into(imageView)
                    imageView.setOnClickListener {
                        startActivity<PhotosActivity>(PhotosActivity.photosTag to advertisement.Media.map { it.Uri }.toTypedArray(), PhotosActivity.defaultTag to position)
                    }

                    container.addView(imageView)
                    return imageView
                }

                override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
                    container.removeView(`object` as View)
                }

                override fun isViewFromObject(view: View, `object`: Any): Boolean {
                    return view === `object`
                }

                override fun getCount() = advertisement.Media.size
            }
            indicator.setViewPager(viewPager)
        }
        headerTextView.text = advertisement.Header
        textTextView.text = advertisement.Body
    }

    companion object {
        const val advertisementTag = "adv:tag"
    }

    @InjectPresenter
    lateinit var advertisementPresenter: AdvertisementPresenter

    @ProvidePresenter
    fun provideAdvertisementPresenter(): AdvertisementPresenter {
        return AdvertisementPresenter(intent.getSerializableExtra(advertisementTag) as Advertisement)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.adv_detail_activity)
        setSupportActionBar(toolbar)
        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = true
            internal var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.title = "Реклама"
                    isShow = true
                } else if (isShow) {
                    collapsingToolbarLayout.title = " "
                    isShow = false
                }
            }
        })
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}