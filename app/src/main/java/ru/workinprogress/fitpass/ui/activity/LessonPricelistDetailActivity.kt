package ru.workinprogress.fitpass.ui.activity

import android.app.Activity
import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.pricelist_detail_activity.*
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.PricelistItem
import ru.workinprogress.fitpass.presentation.LessonPricelistDetailPresenter
import ru.workinprogress.fitpass.presentation.PricelistItemPresenter
import ru.workinprogress.fitpass.presentation.PricelistItemView

/**
 * Created by panic on 26.03.2018.
 */

class LessonPricelistDetailActivity : BasePricelistDetailActivity(), PricelistItemView {

    override fun success() {
        setResult(Activity.RESULT_OK)
        super.success()
    }

    companion object {
        const val pricelistItemTag = "pricelist:item"
        const val lessonIdTag = "lessonId"
        const val lessonTypeIdTag = "lessonTypeId"
    }

    @InjectPresenter
    lateinit var lessonPricelistDetailPresenter: LessonPricelistDetailPresenter

    @ProvidePresenter
    fun providePricelistItemPresenter(): LessonPricelistDetailPresenter = LessonPricelistDetailPresenter(intent.extras.getSerializable(pricelistItemTag) as PricelistItem, intent.getStringExtra(lessonIdTag), intent.getStringExtra(lessonTypeIdTag))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        buyButton.setOnClickListener {
            lessonPricelistDetailPresenter.onBuyClicked()
        }
    }

}