package ru.workinprogress.cityfitness.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.training_item.view.*
import org.jetbrains.anko.find
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.UIScheduleItem

/**
 * Created by Pavel Votyakov on 20.04.17.
 * Work in progress © 2017
 */

class ScheduleDaysAdapter : RecyclerView.Adapter<DayViewHolder>() {
    var listener: ((UIScheduleItem) -> Unit)? = null
    val days = mutableListOf<UIScheduleItem>()

    override fun onBindViewHolder(holder: DayViewHolder, position: Int) = holder.bind(days[position], listener)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayViewHolder {
        val dayViewHolder = DayViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.day_item, parent, false))
        return dayViewHolder
    }

    override fun getItemCount() = days.count()

}


class DayViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(uiScheduleItem: UIScheduleItem, listener: ((UIScheduleItem) -> Unit)?) {
        titleTextView.text = uiScheduleItem.title
        startTextView.text = if (uiScheduleItem.showStartTime) uiScheduleItem.startTime else ""
        trainerTextView.text = uiScheduleItem.trainer
        placeTextView.text = uiScheduleItem.place
        trainingClicker.setOnClickListener {
            listener?.invoke(uiScheduleItem)
        }
        needPayView.visibility = if (uiScheduleItem.requirePayment) View.VISIBLE else View.GONE
    }

    val titleTextView by lazy { itemView.find<TextView>(R.id.titleTextView) }
    val startTextView by lazy { itemView.find<TextView>(R.id.startTextView) }
    val trainerTextView by lazy { itemView.find<TextView>(R.id.trainerTextView) }
    val placeTextView by lazy { itemView.find<TextView>(R.id.placeTextView) }
    val trainingClicker by lazy { itemView.find<View>(R.id.trainerClicker) }
    val needPayView by lazy { itemView.find<View>(R.id.requiredPaymentImage) }
}
