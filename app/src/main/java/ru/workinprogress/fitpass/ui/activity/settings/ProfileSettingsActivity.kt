package ru.workinprogress.fitpass.ui.activity.settings

import android.app.Dialog
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.profile_settings_activity.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.settings.ProfileSettingsPresenter
import ru.workinprogress.fitpass.presentation.settings.ProfileSettingsView

/**
 * Created by panic on 15.03.2018.
 */

class ProfileSettingsActivity : MvpAppCompatActivity(), ProfileSettingsView {
    override fun codeSended() {
        startActivity<DeleteAccountActivity>()
        finish()
    }

    override fun showRecoverError(error: String) {
        Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
    }


    override fun startLoading() {
        progressDialog = indeterminateProgressDialog(title = "Отправка кода").apply {
            show()
        }
    }

    var progressDialog: Dialog? = null

    override fun finishLoading() {
        progressDialog?.dismiss()
    }

    @InjectPresenter
    lateinit var profileSettingsPresenter: ProfileSettingsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_settings_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Настройки профиля"
        changePersonal.setOnClickListener {
            startActivity<EditProfileActivity>()
        }
        changePassword.setOnClickListener {
            startActivity<ChangePasswordActivity>()
        }
        deleteProfile.setOnClickListener {
            alert {
                title = "Удаление учётной записи"
                message = "Вы уверены, что хотите удалить учётную запись"
                positiveButton("Удалить", {
                    profileSettingsPresenter.sendCode()
                    it.dismiss()
                })
                negativeButton("Отмена", {
                    it.dismiss()
                })
            }.show()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}