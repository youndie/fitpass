package ru.workinprogress.fitpass.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import org.jetbrains.anko.find
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.Account
import ru.workinprogress.fitpass.utils.rouble

/**
 * Created by panic on 26.03.2018.
 */

class AccountsAdapter(val listener: (Account) -> Unit) : RecyclerView.Adapter<AccountViewHolder>() {

    val data = mutableListOf<Account>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountViewHolder = AccountViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.account_item, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {
        with(data[position]) {
            holder.titleTextView.text = ScopeName
            holder.valueTextView.text = Amount.rouble()
            holder.payButton.setOnClickListener {
                listener(this)
            }
        }
    }

}


class AccountViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val titleTextView by lazy { itemView.find<TextView>(R.id.titleTextView) }
    val valueTextView by lazy { itemView.find<TextView>(R.id.valueTextView) }
    val payButton by lazy { itemView.find<Button>(R.id.payButton) }

}