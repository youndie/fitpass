package ru.workinprogress.fitpass.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.card_item.view.*
import org.jetbrains.anko.find
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.PaymentCard
import java.text.SimpleDateFormat
import java.util.*

class CardsAdapter(val deleteListener: (PaymentCard) -> Unit) : RecyclerView.Adapter<CardViewHolder>() {

    val data = mutableListOf<PaymentCard>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder = CardViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_item, parent, false))

    override fun getItemCount(): Int = data.size
    private val sourceDateFormat = SimpleDateFormat("yyyy-MM-dd")
    private val targetDateFormat = SimpleDateFormat("dd.MM.yyyy")

    private fun String.toDate() = sourceDateFormat.parse(this)
    private fun Date.formatted() = targetDateFormat.format(this)

    override
    fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        with(data[position]) {
            holder.itemView.titleTextView.text = Name
            holder.itemView.secondTextView.text = "Организация: $LegalBankInfo"
            holder.itemView.lastTextView.text = "Доступна до: ${ExpireDate.toDate().formatted()}"

        }
    }

}

class CardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

}