package ru.workinprogress.fitpass.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.about_club_fragment.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.Club
import ru.workinprogress.fitpass.api.serializers.aboutPhotos
import ru.workinprogress.fitpass.api.serializers.photos
import ru.workinprogress.fitpass.presentation.AboutClubPresenter
import ru.workinprogress.fitpass.presentation.AboutClubView
import ru.workinprogress.fitpass.presentation.AboutMyClubPresenter
import ru.workinprogress.fitpass.ui.activity.PhotosActivity

/**
 * Created by panic on 15.03.2018.
 */

class AboutClubFragment : BaseAboutClubFragment(), AboutClubView {

    companion object {
        const val clubTag = "club:tag"
    }

    @InjectPresenter
    lateinit var aboutClubPresenter: AboutClubPresenter

    @ProvidePresenter
    fun provideAboutClubPresenter(): AboutClubPresenter = AboutClubPresenter(arguments?.getSerializable(AboutClubFragment.clubTag) as Club)

}

class AboutMyClubFragment : BaseAboutClubFragment(), AboutClubView {


    @InjectPresenter
    lateinit var aboutClubPresenter: AboutMyClubPresenter

    @ProvidePresenter
    fun provideAboutClubPresenter(): AboutMyClubPresenter = AboutMyClubPresenter()

}

open class BaseAboutClubFragment : MvpFragment(), AboutClubView {
    override fun showError() {

    }

    override fun showClub(club: Club) {
        titleTextView.text = club.Name
        phoneTextView.text = club.Phone
        emailTextView.text = club.Email
        descriptionTextView.text = club.Description
        websiteTextView.text = club.Site
        Picasso.get().load("https://maps.googleapis.com/maps/api/staticmap?size=640x480&scale=2&center=${club.Lat},${club.Lon}&zoom=14&key=AIzaSyDjkidpp4cL_W3gukH9ILPnK92EwigNfSU&markers=${club.Lat},${club.Lon}").into(mapImageView)
        addressTextView.text = club.Address
        imagesContainer.removeAllViews()

        if (club.aboutPhotos.isEmpty()) {
            imagesContainer.visibility = View.GONE
        }

        club.aboutPhotos.forEachIndexed { index, media ->
            activity.layoutInflater.inflate(R.layout.news_image_item, imagesContainer, false).apply {
                Picasso.get().load(media.Uri).into(find<ImageView>(R.id.imageView))
                setOnClickListener {
                    startActivity<PhotosActivity>(PhotosActivity.photosTag to club.aboutPhotos.map { it.Uri }.toTypedArray(), PhotosActivity.defaultTag to index)
                }
                imagesContainer.addView(this)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.about_club_fragment, container, false)
    }
}