package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatActivity
import com.github.chrisbanes.photoview.PhotoView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.photos_activity.*
import org.jetbrains.anko.find
import ru.workinprogress.fitpass.R
import java.lang.Exception

class PhotosActivity : MvpAppCompatActivity() {

    companion object {
        const val photosTag = "photos"
        const val defaultTag = "default"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.photos_activity)
        setSupportActionBar(toolbar)

        val photos = intent.getStringArrayExtra(photosTag)
        val defaultPage = intent.getIntExtra(defaultTag, 0)

        viewPager.adapter = object : android.support.v4.view.PagerAdapter() {
            override fun isViewFromObject(view: View, `object`: Any): Boolean {
                return view == `object`
            }

            override fun getCount(): Int = photos.count()

            override fun instantiateItem(container: ViewGroup, position: Int): Any {
                val photoItem = layoutInflater.inflate(R.layout.photo_item, container, false)
                val photoView = photoItem.find<PhotoView>(R.id.photoView)
                Picasso.get().load(photos[position]).into(photoView, object : Callback {
                    override fun onSuccess() {
                        photoView.attacher.update()
                    }

                    override fun onError(e: Exception?) {
                    }
                })

                container.addView(photoItem)
                return photoItem
            }

            override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
                container.removeView(`object` as View)
            }

        }
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                supportActionBar?.title = "${position + 1} из ${photos.size}"
            }

        })
        viewPager.currentItem = defaultPage
        supportActionBar?.title = "${defaultPage + 1} из ${photos.size}"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}