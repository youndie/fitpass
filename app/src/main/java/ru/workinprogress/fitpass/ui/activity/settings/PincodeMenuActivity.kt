package ru.workinprogress.fitpass.ui.activity.settings

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import kotlinx.android.synthetic.main.pincode_menu_activity.*
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.AuthService

class PincodeMenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pincode_menu_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Пин-код"
        changePincode.setOnClickListener {
            startActivity<ChangePincodeActivity>()
        }
        turnOff.setOnClickListener {
            getSharedPreferences("app:preference:name", Context.MODE_PRIVATE).edit().remove(AuthService.pincodeTag).commit()
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}