package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.special_offer_activity.*
import org.jetbrains.anko.sp
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.SpecialOffer
import ru.workinprogress.fitpass.presentation.SpecialOfferPresenter
import ru.workinprogress.fitpass.presentation.SpecialOfferView

/**
 * Created by panic on 16.03.2018.
 */

class SpecialOfferActivity : MvpAppCompatActivity(), SpecialOfferView {
    override fun finishAction() {
        finish()
    }

    override fun showError() {
        offerActionButton.isEnabled = true
        offerRejectButton.isEnabled = true

    }

    override fun startAction() {
        offerActionButton.isEnabled = false
        offerRejectButton.isEnabled = false
    }


    override fun showOffer(specialOffer: SpecialOffer) {
        headerTextView.text = specialOffer.Header
        textTextView.text = specialOffer.Body
        offerActionButton.visibility = if (specialOffer.Accepted == true) View.GONE else View.VISIBLE
    }

    @InjectPresenter
    lateinit var specialOfferPresenter: SpecialOfferPresenter

    @ProvidePresenter
    fun providesSpecialOfferPresenter(): SpecialOfferPresenter = SpecialOfferPresenter(intent.getSerializableExtra(offerTag) as SpecialOffer)

    companion object {
        const val offerTag = "special:offer:tag"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.special_offer_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Новости, акции и реклама"
        offerActionButton.setOnClickListener {
            specialOfferPresenter.onOfferAction()
        }
        offerRejectButton.setOnClickListener {
            specialOfferPresenter.onOfferReject()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}