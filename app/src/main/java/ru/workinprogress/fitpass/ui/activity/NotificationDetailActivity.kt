package ru.workinprogress.fitpass.ui.activity

import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import ru.workinprogress.fitpass.presentation.NotificationDetailPresenter
import ru.workinprogress.fitpass.presentation.NotificationDetailView

/**
 * Created by panic on 06.03.2018.
 */

class NotificationDetailActivity : MvpAppCompatActivity(), NotificationDetailView {

    @InjectPresenter
    lateinit var notificationDetailPresenter: NotificationDetailPresenter

}