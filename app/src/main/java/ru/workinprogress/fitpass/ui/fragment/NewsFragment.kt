package ru.workinprogress.fitpass.ui.fragment

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.news_fragment.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.Advertisement
import ru.workinprogress.fitpass.api.serializers.News
import ru.workinprogress.fitpass.api.serializers.SpecialOffer
import ru.workinprogress.fitpass.presentation.NewsPresenter
import ru.workinprogress.fitpass.presentation.NewsView
import ru.workinprogress.fitpass.ui.activity.AdvDetailActivity
import ru.workinprogress.fitpass.ui.activity.NewsDetailActivity
import ru.workinprogress.fitpass.ui.activity.SpecialOfferActivity
import ru.workinprogress.fitpass.ui.adapter.NewsAdapter

/**
 * Created by panic on 15.03.2018.
 */

class NewsFragment : MvpFragment(), NewsView {

    private var snackbar: Snackbar? = null

    override fun showError() {
        if (snackbar == null) {
            snackbar = Snackbar.make(viewPager, getString(R.string.no_internet_error), Snackbar.LENGTH_INDEFINITE).apply {
                setAction(getString(R.string.retry), {
                    newsPresenter.refreshAds()
                    newsPresenter.refreshNews()
                    newsPresenter.refreshOffers()
                    dismiss()
                    snackbar = null
                })
            }
            snackbar?.show()
        }
    }


    override fun adsLoading(b: Boolean) {
        adsSwipeRefreshLayout.isRefreshing = b
    }

    override fun newsLoading(b: Boolean) {
        newsSwipeRefreshLayout.isRefreshing = b
    }

    override fun offersLoading(b: Boolean) {
        offersSwipeRefreshLayout.isRefreshing = b
    }

    override fun showAds(ads: Array<Advertisement>) {
        adsAdapter.show(ads.map { it as SpecialOffer }.toTypedArray())
    }

    override fun showSpecialOffers(offers: Array<SpecialOffer>) = offersAdapter.show(offers)

    override fun showNews(news: MutableList<News>) {
        newsAdapter.show(news.toTypedArray())
    }

    @InjectPresenter
    lateinit var newsPresenter: NewsPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.news_fragment, container, false)
    }

    val offersView by lazy {
        LayoutInflater.from(activity).inflate(R.layout.recycler_view, null, false).also {
            it.find<RecyclerView>(R.id.recyclerView).apply {
                layoutManager = LinearLayoutManager(activity)
                adapter = offersAdapter
            }
        }
    }
    val offersSwipeRefreshLayout by lazy {
        offersView.find<SwipeRefreshLayout>(R.id.swiperefresh).apply {
            setColorSchemeResources(R.color.colorPrimary)
            setOnRefreshListener {
                newsPresenter.refreshOffers()
            }
        }
    }
    val offersAdapter by lazy {
        NewsAdapter {
            startActivity<SpecialOfferActivity>(SpecialOfferActivity.offerTag to it)
        }
    }

    val newsView by lazy {
        LayoutInflater.from(activity).inflate(R.layout.recycler_view, null, false).apply {
            find<RecyclerView>(R.id.recyclerView).layoutManager = LinearLayoutManager(activity)
            find<RecyclerView>(R.id.recyclerView).adapter = newsAdapter
        }
    }
    val newsAdapter by lazy {
        NewsAdapter {
            startActivity<NewsDetailActivity>(NewsDetailActivity.newsDetailTag to it as News)
        }
    }
    val newsSwipeRefreshLayout by lazy {
        newsView.find<SwipeRefreshLayout>(R.id.swiperefresh).apply {
            setColorSchemeResources(R.color.colorPrimary)

            setOnRefreshListener {
                newsPresenter.refreshNews()
            }
        }
    }

    val adsView by lazy {
        LayoutInflater.from(activity).inflate(R.layout.recycler_view, null, false).also {
            it.find<RecyclerView>(R.id.recyclerView).apply {
                layoutManager = LinearLayoutManager(activity)
                adapter = adsAdapter
            }
        }
    }
    val adsSwipeRefreshLayout by lazy {
        adsView.find<SwipeRefreshLayout>(R.id.swiperefresh).apply {
            setColorSchemeResources(R.color.colorPrimary)

            setOnRefreshListener {
                newsPresenter.refreshAds()
            }
        }
    }
    val adsAdapter by lazy {
        NewsAdapter {
            startActivity<AdvDetailActivity>(AdvDetailActivity.advertisementTag to it as Advertisement)
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewPager.adapter = object : android.support.v4.view.PagerAdapter() {
            override fun isViewFromObject(view: View, `object`: Any): Boolean {
                return view === `object`
            }

            override fun getCount(): Int = 3

            override fun instantiateItem(container: ViewGroup, position: Int): Any {
                val view = when (position) {
                    0 -> newsView
                    1 -> offersView
                    else -> adsView
                }
                container.addView(view)
                return view
            }

            override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
                container.removeView(`object` as View)
            }

            override fun getPageTitle(position: Int): CharSequence? {
                return when (position) {
                    0 -> "Новости"
                    1 -> "Акции"
                    else -> "Архив рекламы"
                }
            }

        }
        tabLayout.setupWithViewPager(viewPager)

    }
}