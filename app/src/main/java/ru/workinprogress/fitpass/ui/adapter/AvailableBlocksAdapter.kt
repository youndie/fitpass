package ru.workinprogress.fitpass.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.find
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.AvailableBlock
import java.text.SimpleDateFormat

/**
 * Created by panic on 07.03.2018.
 */

class AvailableBlocksAdapter(private val listener: (AvailableBlock) -> Unit) : RecyclerView.Adapter<AvailableBlockViewHolder>() {

    val data = mutableListOf<AvailableBlock>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AvailableBlockViewHolder {
        return AvailableBlockViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.available_block_item, parent, false))
    }

   private val sourceFormat = SimpleDateFormat("yyyy-MM-dd")
private val format = SimpleDateFormat("dd.MM.yyyy")
    override fun getItemCount(): Int = data.size
    override fun onBindViewHolder(holder: AvailableBlockViewHolder, position: Int) {
        with(data[position]) {
            holder.titleTextView.text = Name

            holder.textTextView.text = "???"
            holder.remainingTextView.text = "Доступно $SeancesLeft из $SeancesTotal"
            holder.dateTextView.text =format.format(sourceFormat.parse(DateOfCreation))
            holder.itemView.setOnClickListener {
                listener(this)
            }
        }
    }

}

class AvailableBlockViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val titleTextView by lazy { itemView.find<TextView>(R.id.titleTextView) }
    val textTextView by lazy { itemView.find<TextView>(R.id.textTextView) }
    val remainingTextView by lazy { itemView.find<TextView>(R.id.remainingTextView) }
    val dateTextView by lazy { itemView.find<TextView>(R.id.dateTextView)}
}