package ru.workinprogress.fitpass.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.find
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.BonusDocument
import java.text.SimpleDateFormat


class BonusDocumentsAdapter : RecyclerView.Adapter<BonusDocumentViewHolder>() {

    val data = mutableListOf<BonusDocument>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BonusDocumentViewHolder = LayoutInflater.from(parent.context).inflate(R.layout.bonus_document_item, parent, false).let { BonusDocumentViewHolder(it) }

    override fun getItemCount(): Int = data.size

    val sourceSdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val targetSdf = SimpleDateFormat("dd.MM.yyyy HH:mm")

    override fun onBindViewHolder(holder: BonusDocumentViewHolder, position: Int) {
        with(data[position]) {
            holder.dateTextView.text = targetSdf.format(sourceSdf.parse(Date))
            holder.textTextView.text = Description
            holder.valueTextView.setTextColor(holder.itemView.resources.getColor(if (Amount > 0) R.color.md_green_500 else R.color.md_red_500))
            holder.valueTextView.text = if (Amount > 0) "+$Amount" else "$Amount"
        }
    }

}

class BonusDocumentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val dateTextView by lazy { itemView.find<TextView>(R.id.dateTextView) }
    val textTextView by lazy { itemView.find<TextView>(R.id.textTextView) }
    val valueTextView by lazy { itemView.find<TextView>(R.id.valueTextView) }


}