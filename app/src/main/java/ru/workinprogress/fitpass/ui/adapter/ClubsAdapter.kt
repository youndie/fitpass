package ru.workinprogress.fitpass.ui.adapter

import android.graphics.drawable.ColorDrawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.Club
import ru.workinprogress.fitpass.api.serializers.background
import ru.workinprogress.fitpass.api.serializers.icon
import ru.workinprogress.fitpass.api.serializers.logo

class ClubsAdapter(val listener: (Club) -> Unit) : RecyclerView.Adapter<ClubViewHolder>() {

    val data = mutableListOf<Club>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClubViewHolder = LayoutInflater.from(parent.context).inflate(R.layout.club_item, parent, false).let { ClubViewHolder(it) }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ClubViewHolder, position: Int) {
        with(data[position]) {
            holder.titleTextView.text = Name
            if (logo != null) {
                Picasso.get().load(logo?.Uri).into(holder.imageView)
                holder.iconImageView.setImageDrawable(null)
            } else {
                holder.imageView.setImageDrawable(ColorDrawable(holder.itemView.resources.getColor(R.color.colorPrimary)))
                background?.Uri?.let {
                    Picasso.get().load(it).into(holder.imageView)
                }
                Picasso.get().load(icon?.Uri).into(holder.iconImageView)
            }
            holder.itemView.setOnClickListener {
                listener(this)
            }
        }
    }

}


class ClubViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val imageView by lazy { itemView.find<ImageView>(R.id.imageView) }
    val titleTextView by lazy { itemView.find<TextView>(R.id.titleTextView) }
    val moreButton by lazy { itemView.find<View>(R.id.moreButton) }
    val iconImageView by lazy { itemView.find<ImageView>(R.id.icon) }
}