package ru.workinprogress.fitpass.ui.activity

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.create_pincode_activity.*
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.CreatePincodePresenter
import ru.workinprogress.fitpass.presentation.CreatePincodeView
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import org.jetbrains.anko.newTask
import org.jetbrains.anko.startActivity


/**
 * Created by panic on 28.02.2018.
 */

class CreatePincodeActivity : MvpAppCompatActivity(), CreatePincodeView {
    override fun success() {
        startActivity(Intent(this, MainActivity::class.java).newTask())
        finish()
    }

    override fun showError() {
        Snackbar.make(constraintLayout, "Пин-коды должны совпадать", Snackbar.LENGTH_LONG).show()
    }

    override fun showEnter() {
        hintTextView.text = "Создайте 4-х значный код для входа в приложение"
        pinEntryEditText.setText("")
    }

    override fun showRepeat() {
        hintTextView.text = "Повторите код"
        pinEntryEditText.setText("")
    }

    @InjectPresenter
    lateinit var createPincodePresenter: CreatePincodePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_pincode_activity)
        supportActionBar?.elevation = 0f
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Пин-код"

        pinEntryEditText.setOnPinEnteredListener {
            if (it.length == 4) {
                createPincodePresenter.onPinEntered(it.toString())
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}