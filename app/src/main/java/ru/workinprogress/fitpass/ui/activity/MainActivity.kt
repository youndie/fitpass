package ru.workinprogress.fitpass.ui.activity

import android.app.Fragment
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.transition.TransitionManager
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.DividerDrawerItem
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader
import com.mikepenz.materialdrawer.util.DrawerImageLoader
import com.squareup.picasso.Picasso
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.navigation_header.*
import kotlinx.android.synthetic.main.sale_services_layout.*
import org.jetbrains.anko.*
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.android.FragmentNavigator
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.AuthService
import ru.workinprogress.fitpass.api.params.LoginParams
import ru.workinprogress.fitpass.api.serializers.Club
import ru.workinprogress.fitpass.api.serializers.unbox
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.domain.PersonInteractor
import ru.workinprogress.fitpass.presentation.MainWindowPresenter
import ru.workinprogress.fitpass.presentation.MainWindowView
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.about
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.bonuses
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.debts
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.external
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.main
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.navigatorHolder
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.news
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.notifications
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.profile
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.router
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.schedule
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.services
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.settings
import ru.workinprogress.fitpass.ui.fragment.*
import javax.inject.Inject

class MainActivity : MvpAppCompatActivity(), MainWindowView {
    override fun showName(name: String) {
        material_drawer_account_header_name?.text = name
    }

    var noClub = true

    override fun showClub(clubs: Array<Club>, selected: Club?) {
        if (material_drawer_account_header_background != null) {
            material_drawer_account_header_background.setImageDrawable(ColorDrawable(resources.getColor(R.color.colorPrimary)))

            selected?.also {
                material_drawer_account_header_email?.text = it.Name
            }?.Media?.firstOrNull { it.LayoutName == "Background_Square" }?.let {
                Picasso.get().load(it.Uri).into(material_drawer_account_header_background)
            }

            noClub = if (selected == null) {
                material_drawer_account_header_background.setImageDrawable(ColorDrawable(resources.getColor(R.color.colorPrimary)))
                true
            } else {
                false
            }
        }
    }

    override fun toolbarAlpha(it: Boolean) {
        toolbar?.background?.alpha = if (it) 0 else 255
        toolbarHelper.visibility = if (it) View.GONE else View.VISIBLE
    }

    override fun showTitle(it: String) {
        supportActionBar?.title = it
    }

    override fun showToolbarElevation(it: Boolean) {
        toolbar?.elevation = if (it) dip(6).toFloat() else 0f
    }

    @InjectPresenter
    lateinit var mainWindowPresenter: MainWindowPresenter

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var clubInteractor: ClubInteractor

    @Inject
    lateinit var personInteractor: PersonInteractor

    override fun onBackPressed() {
        if (globalDim.visibility == View.VISIBLE) {
            android.support.transition.TransitionManager.beginDelayedTransition(mainConstraintLayout)
            globalBottomContainer.visibility = View.GONE
            globalDim.visibility = View.GONE
        } else super.onBackPressed()
    }

    private var drawer: Drawer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Injector.inject(this)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        globalDim.setOnClickListener {
            android.support.transition.TransitionManager.beginDelayedTransition(mainConstraintLayout)
            globalBottomContainer.visibility = View.GONE
            globalDim.visibility = View.GONE
        }
        drawer = DrawerBuilder().withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        PrimaryDrawerItem().withName("Главное").withIdentifier(R.id.main.toLong()).withIconTintingEnabled(true),
                        PrimaryDrawerItem().withName("Мой профиль").withIdentifier(R.id.profile.toLong()).withIconTintingEnabled(true),
                        PrimaryDrawerItem().withName("Услуги").withIdentifier(R.id.services.toLong()).withIconTintingEnabled(true),
                        PrimaryDrawerItem().withName("Задолженности").withIdentifier(R.id.debts.toLong()).withIconTintingEnabled(true),
                        PrimaryDrawerItem().withName("Бонусы").withIdentifier(R.id.bonuses.toLong()).withIconTintingEnabled(true),
                        PrimaryDrawerItem().withName("Расписание").withIdentifier(R.id.schedule.toLong()).withIconTintingEnabled(true),
                        PrimaryDrawerItem().withName("Новости, акции и реклама").withIdentifier(R.id.news.toLong()).withIconTintingEnabled(true),
                        PrimaryDrawerItem().withName("О клубе").withIdentifier(R.id.about.toLong()).withIconTintingEnabled(true),
                        PrimaryDrawerItem().withName("Настройки").withIdentifier(R.id.settings.toLong()).withIconTintingEnabled(true),
                        PrimaryDrawerItem().withName("Внешние приложения").withIdentifier(R.id.external.toLong()).withIconTintingEnabled(true)
                )
                .withHeader(R.layout.navigation_header)
                .withOnDrawerItemClickListener { view, position, drawerItem ->

                    if (arrayOf(R.id.profile, R.id.services, R.id.debts, R.id.bonuses, R.id.schedule, R.id.notifications, R.id.about).contains(drawerItem.identifier.toInt()) && noClub) {
                        drawer?.closeDrawer()
                        startActivity<AddClubActivity>(AddClubActivity.noClubTag to true)
                        return@withOnDrawerItemClickListener false
                    }


                    when (drawerItem.identifier.toInt()) {
                        R.id.main -> router.newRootScreen(main)
                        R.id.debts -> router.newRootScreen(debts)
                        R.id.schedule -> router.newRootScreen(schedule)
                        R.id.bonuses -> router.newRootScreen(bonuses)
                        R.id.news -> router.newRootScreen(news)
                        R.id.services -> router.newRootScreen(MainNavigation.services)
                        R.id.about -> router.newRootScreen(about)
                        R.id.profile -> router.newRootScreen(profile)
                        R.id.settings -> router.newRootScreen(settings)
                        R.id.notifications -> router.newRootScreen(notifications)
                        R.id.external -> router.newRootScreen(external)
                    }
                    drawer?.closeDrawer()
                    true
                }.build()
        DrawerImageLoader.init(object : AbstractDrawerImageLoader() {})
        drawer?.setSelection(R.id.main.toLong())
    }

    object MainNavigation {
        private val cicerone by lazy { Cicerone.create() }
        val router = cicerone.router
        val navigatorHolder = cicerone.navigatorHolder
        const val blocks = "blocks"
        const val main = "main"
        const val profile = "profile"
        const val services = "services"
        const val debts = "debts"
        const val bonuses = "bonuses"
        const val schedule = "schedule"
        const val news = "news"
        const val notifications = "notifications"
        const val about = "about"
        const val settings = "settings"
        const val external = "external"
    }

    private val navigator = object : FragmentNavigator(fragmentManager, R.id.contentContainer) {
        override fun createFragment(screenKey: String?, data: Any?): Fragment {
            return when (screenKey) {
                main -> MainFragment()
                profile -> ProfileFragment()
                MainNavigation.services -> ServicesFragment()
                bonuses -> BonusesFragment()
                schedule -> ScheduleFragment()
                news -> NewsFragment()
                notifications -> NotificationsFragment()
                about -> AboutMyClubFragment()
                settings -> SettingsFragment()
                debts -> ServicesFragment().withArguments(ServicesFragment.unpaidTag to true)
                external -> ExternalAppsFragment()
                MainNavigation.blocks -> ServicesFragment().withArguments(ServicesFragment.blocksTag to true)
                else -> {
                    error("invalid screenkey")
                }
            }
        }

        override fun exit() {
        }

        override fun showSystemMessage(message: String?) {
        }


    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

}


fun <T> Single<T>.async(): Single<T> {
    return subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.async(): Observable<T> {
    return subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}


fun <T> Maybe<T>.async(): Maybe<T> {
    return subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}

fun Completable.async(): Completable {
    return subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}

