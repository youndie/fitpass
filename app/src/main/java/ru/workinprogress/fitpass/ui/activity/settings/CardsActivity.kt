package ru.workinprogress.fitpass.ui.activity.settings

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.cards_activity.*
import org.jetbrains.anko.alert
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.PaymentCard
import ru.workinprogress.fitpass.presentation.settings.CardsPresenter
import ru.workinprogress.fitpass.presentation.settings.CardsView
import ru.workinprogress.fitpass.ui.adapter.CardsAdapter

/**
 * Created by panic on 26.03.2018.
 */

class CardsActivity : MvpAppCompatActivity(), CardsView {
    override fun showCards(cards: Array<PaymentCard>) {
        adapter.data.clear()
        adapter.data.addAll(cards)
        adapter.notifyDataSetChanged()
    }

    override fun startLoading() {
    }

    override fun finishLoading() {
    }

    override fun showError() {
    }

    @InjectPresenter
    lateinit var cardsPresenter: CardsPresenter

    val adapter get() = recyclerView.adapter as CardsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cards_activity)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = CardsAdapter { card ->
            alert {
                title = "Удаление карты"
                message = "Вы уверены, что хотите удалить ${card.Name}"
                positiveButton("Удалить", {
                    it.dismiss()
                    cardsPresenter.onDeleteCard(card)
                })
                negativeButton("Отмена", { it.dismiss() })
            }
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Сохраненные карты"
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}