package ru.workinprogress.fitpass.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.price_list_header.view.*
import org.jetbrains.anko.find
import retrofit2.http.HEAD
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.ContractPriceItem
import ru.workinprogress.fitpass.api.serializers.ExternalSystemURI
import ru.workinprogress.fitpass.api.serializers.PricelistItem
import ru.workinprogress.fitpass.utils.rouble
import kotlin.math.roundToInt

/**
 * Created by panic on 07.03.2018.
 */

class PricelistAdapter(val listener: ((PricelistItem) -> Unit), val blocks: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    companion object {
        const val ITEM_TYPE = 0
        const val HEADER_TYPE = 1
    }

    private val data = mutableListOf<Any>()

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is UIHeader -> HEADER_TYPE
            is PricelistItem -> ITEM_TYPE
            else -> -1
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            if (viewType == ITEM_TYPE)
                PricelistItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.price_list_item, parent, false))
            else
                PricelistHeaderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.price_list_header, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? PricelistItemViewHolder)?.let {
            with(data[position] as PricelistItem) {
                holder.titleTextView.text = Name
                holder.textTextView.text = Price.rouble()
                holder.remainingTextView.text =
                        if (blocks) {
                            "Срок действия: ${if (ValidDays != null) " ${holder.itemView.resources.getQuantityString(R.plurals.days, ValidDays, ValidDays)}" else "Бессрочно"}\nКод: $Code"
                        } else {
                            "Код: $Code"
                        }

                holder.itemView.setOnClickListener {
                    listener(this)
                }
            }
        }
        (holder as? PricelistHeaderViewHolder)?.let {
            with(data[position] as UIHeader) {
                it.itemView?.text?.text = title
                it.itemView?.expandClicker?.setImageResource(if (expanded) R.drawable.ic_expand_less_black_24dp else R.drawable.ic_expand_more_black_24dp)
                it.itemView?.setOnClickListener {
                    if (expanded.not()) {
                        data.addAll(holder.adapterPosition + 1, items)
                        notifyItemRangeInserted(holder.adapterPosition + 1, items.size)
                    } else {
                        data.removeAll(items)
                        notifyItemRangeRemoved(holder.adapterPosition + 1, items.size)
                    }
                    expanded = expanded.not()
                    notifyItemChanged(position)
                }
            }

        }
    }

    fun show(items: List<PricelistItem>) {
        items.groupBy {
            it.PriceListSectionCode
        }.entries.sortedBy { it.key }.forEach {
            data.add(UIHeader("${it.key} - ${it.value.first().PriceListSectionName}", it.value))
        }
    }

}

data class UIHeader(val title: String, var items: List<PricelistItem>, var expanded: Boolean = false)

class ContractPricelistAdapter(val listener: (ContractPriceItem) -> Unit) : RecyclerView.Adapter<PricelistItemViewHolder>() {
    val data = mutableListOf<ContractPriceItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PricelistItemViewHolder = PricelistItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.price_list_item, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: PricelistItemViewHolder, position: Int) {
        with(data[position]) {
            holder.titleTextView.text = GroupName
            holder.textTextView.text = Prices.firstOrNull()?.Value?.roundToInt()?.rouble()
            holder.remainingTextView.text = interval(holder.itemView.resources)
            holder.itemView.setOnClickListener {
                listener(this)
            }
        }
    }
}

class PricelistHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

class PricelistItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val titleTextView by lazy { itemView.find<TextView>(R.id.titleTextView) }
    val textTextView by lazy { itemView.find<TextView>(R.id.textTextView) }
    val remainingTextView by lazy { itemView.find<TextView>(R.id.remainingTextView) }
}