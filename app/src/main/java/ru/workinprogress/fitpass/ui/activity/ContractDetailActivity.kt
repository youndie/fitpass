package ru.workinprogress.fitpass.ui.activity

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.ContractStatus
import ru.workinprogress.fitpass.presentation.ContractPresenter
import ru.workinprogress.fitpass.presentation.ContractView
import android.support.design.widget.AppBarLayout
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.contract_detail_activity.*
import org.jetbrains.anko.*
import ru.workinprogress.fitpass.domain.CardsInfo
import ru.workinprogress.fitpass.utils.rouble
import ru.workinprogress.fitpass.utils.toReadableYMDDate
import java.text.SimpleDateFormat


/**
 * Created by panic on 20.03.2018.
 */

class ContractDetailActivity : MvpAppCompatActivity(), ContractView {
    override fun finishResuming() {
        progressDialog?.dismiss()
    }

    override fun startResuming() {
        progressDialog = indeterminateProgressDialog(title = "Восстановление контракта").apply {
            show()
        }
    }

    var progressDialog: Dialog? = null


    override fun showSuccess(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun showFailed(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showPaymentVariants(cards: CardsInfo) {
        paymentsVariantsContainer.removeAllViews()
        cards.list.map { card ->
            LayoutInflater.from(this).inflate(R.layout.button_panel_item, paymentsVariantsContainer, false).apply {
                find<TextView>(R.id.button).text = "Используя карту ${card.Name}"
                setOnClickListener {
                    contractPresenter.onCardPayClicked(card)
                }
            }
        }.forEach {
            paymentsVariantsContainer.addView(it)
        }
        if (cards.newCard)
            paymentsVariantsContainer.addView(LayoutInflater.from(this).inflate(R.layout.button_panel_item, paymentsVariantsContainer, false).apply {
                find<TextView>(R.id.button).text = "Используя новую карту"
                setOnClickListener {
                    contractPresenter.onNewCardPayClicked()
                }
            })
        arrayOf(dim, paymentsVariantsContainer).forEach { it.visibility = View.VISIBLE }
    }

    override fun hidePaymentsVariants() {
        arrayOf(dim, paymentsVariantsContainer).forEach { it.visibility = View.GONE }
    }


    override fun onBackPressed() {
        if (dim.visibility == View.VISIBLE) {
            hidePaymentsVariants()
        } else
            super.onBackPressed()
    }


    override fun showPaymentPage(uri: String) {
        browse(uri)
    }

    override fun startPay() {
        payButton.isEnabled = false
    }

    override fun showError() {
    }

    override fun finishPay() {
        payButton.isEnabled = true
    }

    @SuppressLint("SetTextI18n")
    override fun showContract(contractStatus: ContractStatus) {
        swipeRefresh.isRefreshing = false
        titleTextView.text = contractStatus.Template
        subtitleTextView.text = contractStatus.Status
        dateStartTextView.text = contractStatus.DateBegin.toReadableYMDDate()
        dateEndTextView.text = contractStatus.DateEnd.toReadableYMDDate()
        paymentsContainer.removeAllViews()
        debtTextView.text = contractStatus.ExpectedPayments.sumBy { it.Amount.toInt() }.rouble()
        arrayOf(debtHint, debtTextView, payButton, nearestPaymentHint, nearestPaymentTextView).forEach { it.visibility = if (contractStatus.ExpectedPayments.sumBy { it.Amount.toInt() } == 0) View.GONE else View.VISIBLE }
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        nearestPaymentTextView.text = contractStatus.ExpectedPayments.minBy { sdf.parse(it.DateDue).time }?.DateDue?.toReadableYMDDate()
        durationTextView.text = contractStatus.interval(resources)
        contractStatus.ExpectedPayments.map {
            LayoutInflater.from(this).inflate(R.layout.contract_payment_item, paymentsContainer, false).apply {
                find<TextView>(R.id.valueTextView).text = it.Amount.toInt().rouble()
                find<TextView>(R.id.dateTextView).text = "до " + it.DateDue.toReadableYMDDate()
            }
        }.forEach {
            paymentsContainer.addView(it)
        }
        paymentsHint.visibility = if (contractStatus.ExpectedPayments.isEmpty()) View.GONE else View.VISIBLE
        numberTextView.text = contractStatus.Contract.Number
        dateContractTextView.text = contractStatus.Contract.DateEstablished.toReadableYMDDate()

        resumeButton.visibility = if (contractStatus.StatusCode == 5 && contractStatus.SuspendInfo != null) View.VISIBLE else View.GONE
        resumeButton.setOnClickListener {
            alert {
                title = "Возобновление контракта"
                message = "Вы уверены, что хотите разморозить контракт?"
                negativeButton("Отмена", { it.dismiss() })
                positiveButton("Разморозить", {
                    contractPresenter.resumeContract()
                })
            }.show()
        }

        freezeButton.visibility = if (contractStatus.CanSuspend && contractStatus.SuspendInfo == null) View.VISIBLE else View.GONE
        freezeButton.setOnClickListener {
            startActivity<ProfileContractFreezeActivity>(contractTag to contractStatus)
        }
        freezeTextView.visibility = if (contractStatus.SuspendInfo == null) View.GONE else View.VISIBLE
        contractStatus.SuspendInfo?.let {
            freezeTextView.text = "Заморозка с ${it.BeginDate.toReadableYMDDate()} по ${it.EndDate.toReadableYMDDate()}"
            if (contractStatus.StatusCode != 5)
                freezeTextView.setOnClickListener {
                    startActivity<ProfileContractFreezeActivity>(contractTag to contractStatus)
                }
        }

    }

    companion object {
        const val contractTag = "contractTag"
    }

    @ProvidePresenter
    fun provideContractPresenter(): ContractPresenter = ContractPresenter(intent.getSerializableExtra(contractTag) as ContractStatus)

    @InjectPresenter
    lateinit var contractPresenter: ContractPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contract_detail_activity)
        setSupportActionBar(toolbar)
        toolbar?.background?.alpha = 255
        collapsingToolbarLayout?.background?.alpha = 255
        appBarLayout.background.alpha = 255
        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = true
            internal var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.title = "Контракты"
                    isShow = true
                } else if (isShow) {
                    collapsingToolbarLayout.title = " "
                    isShow = false
                }
            }
        })
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        payButton.setOnClickListener {
            contractPresenter.onPayClicked()
        }
        dim.setOnClickListener {
            hidePaymentsVariants()
        }
        swipeRefresh.apply {
            setColorSchemeResources(R.color.colorPrimary)
            setOnRefreshListener {
                contractPresenter.onUpdate()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}