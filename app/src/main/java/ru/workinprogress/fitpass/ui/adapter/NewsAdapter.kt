package ru.workinprogress.fitpass.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.training_item.view.*
import org.jetbrains.anko.find
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.Advertisement
import ru.workinprogress.fitpass.api.serializers.News
import ru.workinprogress.fitpass.api.serializers.SpecialOffer
import ru.workinprogress.fitpass.utils.Format.sourceYMDHMSFormat
import java.text.SimpleDateFormat

/**
 * Created by panic on 16.03.2018.
 */

class NewsAdapter(val listener: (SpecialOffer) -> Unit) : RecyclerView.Adapter<NewsItemViewHolder>() {

    fun show(offers: Array<SpecialOffer>) {
        data.clear()
        data.addAll(offers)
        notifyDataSetChanged()
    }

    val data = mutableListOf<SpecialOffer>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsItemViewHolder = NewsItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: NewsItemViewHolder, position: Int) {
        holder.let {
            it.itemView.setOnClickListener {
                listener(data[position])
            }
            it.bind(data[position])
        }
    }

}

class NewsItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val logoImageView by lazy { itemView.find<ImageView>(R.id.logoImageView) }
    val typeTextView by lazy { itemView.find<TextView>(R.id.typeTextView) }
    val headerTextView by lazy { itemView.find<TextView>(R.id.headerTextView) }
    val photoImageView by lazy { itemView.find<ImageView>(R.id.photoImageView) }
    val textTextView by lazy { itemView.find<TextView>(R.id.textTextView) }
    val dateTextView by lazy { itemView.find<TextView>(R.id.dateTextView) }
    private val format = SimpleDateFormat("dd.MM.yyyy")

    fun bind(specialOffer: SpecialOffer) {
        with(specialOffer) {
            textTextView.text = Body
            headerTextView.text = Header
            typeTextView.text = when (this) {
                is News -> "Новость"
                is Advertisement -> "Реклама"
                else -> "Акция"
            }
            if (this is News) {
                dateTextView.visibility = View.VISIBLE
                dateTextView.text = format.format(sourceYMDHMSFormat.parse(Date))
            } else {
                dateTextView.visibility = View.GONE
            }
            if (Media.firstOrNull()?.Uri != null) {
                photoImageView.visibility = View.VISIBLE
                Picasso.get().load(this.Media.first().Uri).into(photoImageView)
            } else {
                photoImageView.visibility = View.GONE
            }
        }

    }

}