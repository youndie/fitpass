package ru.workinprogress.fitpass.ui.fragment

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.bonuses_fragment.*
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.BonusDocument
import ru.workinprogress.fitpass.api.serializers.BonusHowToResponse
import ru.workinprogress.fitpass.presentation.BonusesPresenter
import ru.workinprogress.fitpass.presentation.BonusesView
import ru.workinprogress.fitpass.ui.activity.HowToBonusActivity
import ru.workinprogress.fitpass.ui.adapter.BonusDocumentsAdapter

/**
 * Created by panic on 06.03.2018.
 */


class BonusesFragment : MvpFragment(), BonusesView {
    override fun startLoading() {
        snackbar?.dismiss()
        progressBar.visibility = View.VISIBLE
        bonusesTextView.visibility = View.GONE
    }

    override fun showDocuments(docs: Array<BonusDocument>) {
        adapter.data.clear()
        adapter.data.addAll(docs)
        adapter.notifyDataSetChanged()
    }

    override fun showHowTo(howTo: BonusHowToResponse) {
        if (howTo.HowToEarn != null) {
            howToEarn.visibility = View.VISIBLE
            howToEarn.setOnClickListener {
                startActivity<HowToBonusActivity>(HowToBonusActivity.howToTag to HowToBonusActivity.HowToBox(howToEarn.text.toString(), "Как можно получить бонусы", howTo.HowToEarn.orEmpty()))
            }
        }
        if (howTo.HowToSpend != null) {
            howToSpend.visibility = View.VISIBLE
            howToSpend.setOnClickListener {
                startActivity<HowToBonusActivity>(HowToBonusActivity.howToTag to HowToBonusActivity.HowToBox(howToSpend.text.toString(), "На что можно тратить бонусы", howTo.HowToSpend.orEmpty()))
            }
        }
    }

    var snackbar: Snackbar? = null

    override fun showError() {
        snackbar = Snackbar.make(parentLayout, R.string.no_internet_error, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(R.string.retry, {
                bonusesPresenter.load()
            })
        }
    }

    override fun showBalance(balance: Int) {
        progressBar.visibility = View.GONE
        bonusesTextView.visibility = View.VISIBLE
        bonusesTextView.text = balance.toString()
    }

    @InjectPresenter
    lateinit var bonusesPresenter: BonusesPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.bonuses_fragment, container, false)
    }

    val adapter: BonusDocumentsAdapter get() = recyclerView.adapter as BonusDocumentsAdapter

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = BonusDocumentsAdapter()
    }

}