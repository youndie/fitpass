package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.registration_photo_activity.*
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.RegistrationPhotoPresenter
import ru.workinprogress.fitpass.presentation.RegistrationPhotoView

/**
 * Created by panic on 28.02.2018.
 */

class RegistrationPhotoActivity : MvpAppCompatActivity(), RegistrationPhotoView {

    @InjectPresenter
    lateinit var registrationPhotoPresenter: RegistrationPhotoPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registration_photo_activity)
        chooseButton.setOnClickListener {
            //todo choose
        }
        skipButton.setOnClickListener {
            registrationPhotoPresenter.onSkipClicked()
        }
    }

}