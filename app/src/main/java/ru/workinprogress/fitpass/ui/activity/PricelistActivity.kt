package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.accounts_activity.*
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.PricelistItem
import ru.workinprogress.fitpass.presentation.PricelistPresenter
import ru.workinprogress.fitpass.presentation.PricelistView
import ru.workinprogress.fitpass.ui.adapter.PricelistAdapter

/**
 * Created by panic on 06.03.2018.
 */

class PricelistActivity : MvpAppCompatActivity(), PricelistView {
    override fun startLoading() {
        snackbar?.dismiss()
        recyclerView.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    override fun finishLoading() {
        recyclerView.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }

    var snackbar: Snackbar? = null

    override fun showError() {
        snackbar = Snackbar.make(parentLayout, R.string.no_internet_error, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(R.string.retry, {
                servicesPresenter.load()
            })
        }
    }

    val adapter get() = recyclerView.adapter as PricelistAdapter

    override fun showPricelist(pricelist: List<PricelistItem>) {
        adapter.show(pricelist)
        adapter.notifyDataSetChanged()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.price_list_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = if (intent.getIntExtra(typeTag, 0) == 1) "Блоки тренировок" else "Услуги"

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = PricelistAdapter({
            startActivity<PricelistDetailActivity>(PricelistDetailActivity.pricelistItemTag to it)
        }, intent.getIntExtra(typeTag, 0) == 1)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }


    @InjectPresenter
    lateinit var servicesPresenter: PricelistPresenter

    @ProvidePresenter
    fun provideServicesPresenter(): PricelistPresenter = PricelistPresenter(intent.getIntExtra(typeTag, 0))

    companion object {
        const val typeTag = "type:tag"
        const val contractsType = 0
        const val blocksType = 1
        const val servicesType = 2
    }


}