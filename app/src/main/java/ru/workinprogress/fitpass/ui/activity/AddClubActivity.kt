package ru.workinprogress.fitpass.ui.activity

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.add_club_activity.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.customView
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.presentation.AddClubPresenter
import ru.workinprogress.fitpass.presentation.AddClubView
import ru.workinprogress.fitpass.ui.fragment.MainFragment

/**
 * Created by panic on 28.02.2018.
 */

class AddClubActivity : MvpAppCompatActivity(), AddClubView {
    override fun showError() {

    }

    override fun showCodeLoading() {
        progressBar.visibility = View.VISIBLE
        codeTextView.visibility = View.GONE
        newCodeButton.visibility = View.GONE
        codeHintTextView.visibility = View.GONE
        helper.visibility = View.GONE
    }

    override fun showCode(code: String) {
        helper.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
        newCodeButton.visibility = View.VISIBLE
        if (!code.isEmpty()) {
            codeTextView.text = code
            codeTextView.visibility = View.VISIBLE
            codeHintTextView.visibility = View.GONE
        } else {
            codeTextView.visibility = View.GONE
            codeHintTextView.visibility = View.GONE
        }
    }

    companion object {
        const val noClubTag = "no:club"
    }

    @InjectPresenter
    lateinit var addClubPresenter: AddClubPresenter

    override fun onStop() {
        super.onStop()
        MainFragment.needUpdate = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_club_activity)
        if (intent?.getBooleanExtra(noClubTag, false) != true) {
            textView2.text = "Сообщите ваш персональный идентификатор менеджеру либо введите код клуба"
        }
        title = "Добавить клуб"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        newCodeButton.setOnClickListener {
            addClubPresenter.onNewCodeClicked()
        }
        helper.setOnClickListener {
            startActivity<HowToAddClubActivity>()
        }
        enterClubCodeButton.setOnClickListener {
            alert {
                title = "Добавление клуба"
                message = "Если вы знаете код, введите его для добавления клуба"
                val customLayout = layoutInflater.inflate(R.layout.add_club_alertview, null)
                customView = customLayout
                positiveButton("Добавить", {
                    addClubPresenter.onClubCode(customLayout.find<EditText>(R.id.clubCodeEditText).text.toString())
                })
                negativeButton("Отмена", {})
            }.show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}