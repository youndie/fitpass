package ru.workinprogress.fitpass.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import org.jetbrains.anko.find
import ru.workinprogress.fitpass.R
import ru.workinprogress.fitpass.api.serializers.Account
import ru.workinprogress.fitpass.api.serializers.Operation
import ru.workinprogress.fitpass.api.serializers.SaleItem
import ru.workinprogress.fitpass.utils.Format.sourceYMDHMSFormat
import ru.workinprogress.fitpass.utils.Format.targetYMDHMSFormat
import ru.workinprogress.fitpass.utils.rouble
import kotlin.math.roundToInt

/**
 * Created by panic on 26.03.2018.
 */

class OperationsAdapter : RecyclerView.Adapter<OperationViewHolder>() {

    val data = mutableListOf<SaleItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OperationViewHolder = OperationViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.operations_item, parent, false))

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: OperationViewHolder, position: Int) {
        with(data[position]) {
            holder.dateTextView.text = targetYMDHMSFormat.format(sourceYMDHMSFormat.parse(Date))
            holder.itemsContainer.removeAllViews()
            Details.forEach { item ->
                val childView = LayoutInflater.from(holder.itemView.context).inflate(R.layout.operation_layout, holder.itemsContainer, false)
                OperationItemViewHolder(childView).apply {
                    titleTextView.text = item.Name
                    countTextView.text = "${item.Quantity}"
                    priceTextView.text = item.Price.roundToInt().rouble()
                }
                holder.itemsContainer.addView(childView)
            }
            holder.sumTextView.text = Details.sumBy { it.Amount.roundToInt() }.rouble()
        }
    }
}


class OperationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val titleTextView by lazy { itemView.find<TextView>(R.id.titleTextView) }
    val dateTextView by lazy { itemView.find<TextView>(R.id.dateTextView) }
    val itemsContainer by lazy { itemView.find<LinearLayout>(R.id.itemsContainer) }
    val sumTextView by lazy { itemView.find<TextView>(R.id.sumTextView) }

}

class OperationItemViewHolder(val itemView: View) {

    val titleTextView by lazy { itemView.find<TextView>(R.id.title) }
    val countTextView by lazy { itemView.find<TextView>(R.id.count) }
    val priceTextView by lazy { itemView.find<TextView>(R.id.price) }

}