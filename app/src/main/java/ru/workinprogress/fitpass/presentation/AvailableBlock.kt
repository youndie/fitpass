package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.api.serializers.AvailableBlock
import ru.workinprogress.fitpass.di.Injector

interface AvailableBlockView : MvpView {
    fun showBlock(availableBlock: AvailableBlock)

}

@InjectViewState
class AvailableBlockPresenter(val availableBlock: AvailableBlock) : MvpPresenter<AvailableBlockView>() {

    init {
        Injector.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showBlock(availableBlock)
    }

}