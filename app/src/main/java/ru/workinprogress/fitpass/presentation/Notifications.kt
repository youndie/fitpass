package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.RegisterImpressionParams
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 06.03.2018.
 */

interface NotificationsView : MvpView

@InjectViewState
class NotificationsPresenter : TitledPresenter<NotificationsView>() {
    init {
        Injector.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        title = "Уведомления"
    }
}

interface NotificationDetailView : MvpView

@InjectViewState
class NotificationDetailPresenter : MvpPresenter<NotificationDetailView>() {

    init {
        Injector.inject(this)
    }

}