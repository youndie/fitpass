package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 28.02.2018.
 */

interface AddClubView : MvpView {
    fun showCode(code: String)
    fun showCodeLoading()
    fun showError()
}

@InjectViewState
class AddClubPresenter : TitledPresenter<AddClubView>() {

    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var clubInteractor: ClubInteractor

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        alpha = false
        viewState.showCode(clubInteractor.linkCode)
    }


    fun onNewCodeClicked() {
        viewState.showCodeLoading()
        clubInteractor.requestLinkCode().async().subscribe({
            viewState.showCode(it)
        }, {
            viewState.showError()
        })

    }

    fun onClubCode(clubCode: String) {

    }

}
