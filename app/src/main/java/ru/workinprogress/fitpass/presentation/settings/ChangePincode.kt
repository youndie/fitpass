package ru.workinprogress.fitpass.presentation.settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.AuthInteractor
import javax.inject.Inject

interface ChangePincodeView : MvpView {
    fun showNewPin()
    fun showInvalidOldPin()
    @StateStrategyType(SkipStrategy::class)
    fun success()

    fun showInvalidNewPin()
    fun showRepeat()
}

@InjectViewState
class ChangePincodePresenter : MvpPresenter<ChangePincodeView>() {

    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var authInteractor: AuthInteractor

    private var old = true

    private var pin = ""

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        if (!authInteractor.pinEnabled) {
            old = false
            viewState.showNewPin()
        }
    }

    fun onPinEntered(pin: String) {
        if (old) {
            if (authInteractor.checkPin(pin)) {
                old = false
                viewState.showNewPin()
            } else {
                viewState.showInvalidOldPin()
            }
        } else {
            if (this.pin.isEmpty()) {
                this.pin = pin
                viewState.showRepeat()
            } else {
                if (this.pin == pin) {
                    authInteractor.savePin(pin)
                    viewState.success()
                } else {
                    this.pin = ""
                    viewState.showNewPin()
                    viewState.showInvalidNewPin()
                }
            }
        }
    }
}