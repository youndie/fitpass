package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.CheckAttributesParams
import ru.workinprogress.fitpass.api.params.LoginParams
import ru.workinprogress.fitpass.api.params.RegisterParams
import ru.workinprogress.fitpass.api.params.SendConfirmationParams
import ru.workinprogress.fitpass.api.serializers.*
import ru.workinprogress.fitpass.data.API_KEY
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.AuthInteractor
import ru.workinprogress.fitpass.domain.ErrorInteractor
import ru.workinprogress.fitpass.ui.activity.async
import ru.workinprogress.fitpass.utils.Validation
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by panic on 22.02.2018.
 */

interface RegistrationView : MvpView {
    fun showCountries(countries: Array<Country>)
    fun showCities(cities: Array<City>)
    fun showLanguages(languages: Array<Language>)
    fun showDate(date: String)
    fun success(needPincode: Boolean)
    fun showDocs(docs: LegacyDocuments)
    fun startLoading()
    fun showCheckResult(it: CheckAttributesResponse)
    fun finishLoading()
    fun showError(clientMessage: String)
    fun showCodeLayout()
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showCity(city: City)
}

@InjectViewState
class RegistrationPresenter : MvpPresenter<RegistrationView>() {

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var authInteractor: AuthInteractor
    @Inject
    lateinit var errorInteractor: ErrorInteractor

    init {
        Injector.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        api.getCountries().unboxAsync().subscribe({
            viewState.showCountries(it)
        }, {})
        api.getLanguages().unboxAsync().subscribe({
            viewState.showLanguages(it)
        }, {})
        api.getLegalDocuments().unboxAsync().subscribe({
            viewState.showDocs(it)
        }, {})
    }

    private var locateCity = ""
    private var cities: Array<City> = emptyArray()
    fun onCountrySelected(country: Country) {
        this.countryCode = country.Code
        api.getCities(country.Code).unbox().map {
            arrayOf(City("", -1, "")) + it.sortedBy { it.Name }.sortedByDescending { it.Name.contains("Москва") or it.Name.contains("Санкт-") }.toTypedArray()
        }.async().subscribe({
            this.cities = it
            viewState.showCities(it)
            showLocateCity()
        }, {})
    }

    fun onCitySelected(city: City) {
        this.cityId = city.ID
    }

    private var birthDay = Date()
    private var countryCode = ""
    private var cityId = 0
    private var languageId = ""
    private val sdf = SimpleDateFormat("yyyy-MM-dd")
    private val sdf2 = SimpleDateFormat("dd MMMM yyyy")
    private var nickname = ""
    private var login = ""
    private var password = ""
    private var isMale = false
    private var phone = ""
    private var email = ""
    private var needPincode = false

    fun onBirthDaySelected(year: Int, month: Int, day: Int) {
        birthDay = Calendar.getInstance().apply {
            set(Calendar.YEAR, year)
            set(Calendar.MONTH, month)
            set(Calendar.DAY_OF_MONTH, day)
        }.time
        viewState.showDate(sdf2.format(birthDay))
    }

    fun onRegister(name: String, login: String, password: String, b: Boolean, phone: String, email: String, checked: Boolean) {
        if (cityId == -1) {
            viewState.showError("Необходимо выбрать город")
            return
        }
        this.nickname = name
        this.login = login
        this.password = password
        this.isMale = b
        this.phone = Validation.phoneNormalize(phone)
        this.email = email
        this.needPincode = checked

        viewState.startLoading()
        api.checkAttributes(CheckAttributesParams(Login = login, NickName = name, MainPhone = this.phone, MainEmail = email, DateOfBirth = sdf.format(birthDay))).unboxAsync().subscribe({
            if (it.LoginNotUnique || it.PhoneNotUnique) {
                viewState.showCheckResult(it)
                viewState.finishLoading()
            } else {
                api.sendConfirmation(SendConfirmationParams(this.phone)).unboxAsync().subscribe({
                    viewState.showCodeLayout()
                    viewState.finishLoading()
                }, {
                    viewState.finishLoading()
                    errorInteractor.toApiError(it)?.let {
                        viewState.showError(it.ClientMessage)
                    } ?: viewState.showError("Произошла ошибка")
                })
            }
        }, {
            viewState.finishLoading()

            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            } ?: viewState.showError("Произошла ошибка")
        })
    }

    fun onLanguageSelected(language: Language) {
        languageId = language.Code
    }

    fun onCodeEntered(code: String) {
        viewState.startLoading()
        api.register(RegisterParams(login, nickname, sdf.format(birthDay), phone, email, password, if (isMale) "1" else "2", countryCode, cityId, languageId, code, true, true)).unboxAsync().subscribe({
            authInteractor.advancedLogin(LoginParams(2, API_KEY, it.Login, Password = password)).async().subscribe({
                viewState.success(this.needPincode)
                viewState.finishLoading()
            }, {})
        }, {
            viewState.finishLoading()

            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            } ?: viewState.showError("Произошла ошибка")
        })
    }

    fun onLocation(locality: String) {
        this.locateCity = locality
        showLocateCity()
    }

    private fun showLocateCity() {
        cities.find { it.Name.contains(locateCity, true) }?.let {
            viewState.showCity(it)
        }
    }
}

interface CreatePincodeView : MvpView {
    fun showRepeat()
    fun showEnter()
    fun showError()
    fun success()
}

@InjectViewState
class CreatePincodePresenter : MvpPresenter<CreatePincodeView>() {

    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var authInteractor: AuthInteractor

    private var pin = ""
    fun onPinEntered(pin: String) {
        if (this.pin.isEmpty()) {
            this.pin = pin
            viewState.showRepeat()
        } else {
            if (this.pin == pin) {
                authInteractor.savePin(pin)
                viewState.success()
            } else {
                this.pin = ""
                viewState.showEnter()
                viewState.showError()
            }
        }
    }
}

interface RegistrationPhotoView : MvpView

@InjectViewState
class RegistrationPhotoPresenter : MvpPresenter<RegistrationPhotoView>() {

    fun onSkipClicked() {

    }

}
