package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.BonusDocument
import ru.workinprogress.fitpass.api.serializers.BonusHowToResponse
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.domain.PersonInteractor
import javax.inject.Inject

/**
 * Created by panic on 06.03.2018.
 */

interface BonusesView : MvpView {
    fun showError()
    fun showBalance(balance: Int)
    fun showHowTo(howTo: BonusHowToResponse)
    fun showDocuments(docs: Array<BonusDocument>)
    fun startLoading()
}


@InjectViewState
class BonusesPresenter : BasePresenter<BonusesView>() {

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var clubInteractor: ClubInteractor

    @Inject
    lateinit var personInteractor: PersonInteractor

    init {
        Injector.inject(this)
        title = "Бонусы"
        elevation = false
    }

    fun load() {
        viewState.startLoading()
        api.getBonusBalance(clubInteractor.selectedClubId, personInteractor.personId).unboxAsync().subscribe({
            viewState.showBalance(it.Value)
        }, {
            viewState.showError()
        })
        api.bonusHowTo(clubInteractor.selectedClubId).unboxAsync().subscribe({
            viewState.showHowTo(it)
        }, {})
        api.getBonusDocuments(clubInteractor.selectedClubId).unboxAsync().subscribe({
            viewState.showDocuments(it)
        }, {

        })
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        load()
    }

}