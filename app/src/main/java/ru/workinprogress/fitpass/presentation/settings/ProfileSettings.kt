package ru.workinprogress.fitpass.presentation.settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.AccountRemovalParams
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.AccountsInteractor
import ru.workinprogress.fitpass.domain.ErrorInteractor
import ru.workinprogress.fitpass.domain.PersonInteractor
import ru.workinprogress.fitpass.ui.activity.async
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by panic on 15.03.2018.
 */

interface ProfileSettingsView : MvpView {
    fun startLoading()
    fun finishLoading()
    fun codeSended()
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showRecoverError(error: String)
}

@InjectViewState
class ProfileSettingsPresenter : MvpPresenter<ProfileSettingsView>() {

    @Inject
    lateinit var api: Api
    @Inject
    lateinit var personInteractor: PersonInteractor
    @Inject
    lateinit var errorInteractor: ErrorInteractor

    init {
        Injector.inject(this)
    }

    fun sendCode() {
        viewState.startLoading()
        personInteractor.person.flatMap {
            api.sendRemovalConfirmation(AccountRemovalParams(it.MainPhone))
        }.unboxAsync().subscribe({
            viewState.finishLoading()
            if (it.MessageSent) {
                viewState.codeSended()
            } else {
                viewState.showRecoverError("Следующий код может быть отправлен не ранее ${SimpleDateFormat("HH:mm dd.MM.yyyy").apply {
                    timeZone = Calendar.getInstance().timeZone
                }.format(SimpleDateFormat("yyyy-MM-dd HH:mm:ss").apply { timeZone = TimeZone.getTimeZone("UTC") }.parse(it.NextAttemptMinTime))}")
            }
        }, {
            viewState.finishLoading()
            errorInteractor.toApiError(it)?.let {
                viewState.showRecoverError(it.ClientMessage)
            }
        })
    }
}