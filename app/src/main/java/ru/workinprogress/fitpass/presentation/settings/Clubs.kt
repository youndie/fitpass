package ru.workinprogress.fitpass.presentation.settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.api.serializers.Club
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.presentation.BasePresenter
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 15.03.2018.
 */

interface ClubsView : MvpView {
    fun showClubs(clubs: Array<Club>)
    fun finishLoading()
}

@InjectViewState
class ClubsPresenter : BasePresenter<ClubsView>() {

    @Inject
    lateinit var clubInteractor: ClubInteractor

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        whileAlive {
            clubInteractor.observeClubs.async().subscribe {
                viewState.showClubs(it)
            }
        }
    }

    fun refresh() {
        clubInteractor.update().async().subscribe({
            viewState.finishLoading()
        },{})
    }

    init {
        Injector.inject(this)
    }

}