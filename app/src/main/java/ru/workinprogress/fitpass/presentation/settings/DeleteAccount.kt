package ru.workinprogress.fitpass.presentation.settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.ConfirmationParams
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.*
import javax.inject.Inject


interface DeleteAccountView : MvpView {
    fun showError(error: String)
    fun startLoading()
    fun finishLoading()
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun success()

}

@InjectViewState
class DeleteAccountPresenter : MvpPresenter<DeleteAccountView>() {

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var errorInteractor: ErrorInteractor

    @Inject
    lateinit var authInteractor: AuthInteractor

    @Inject
    lateinit var personInteractor: PersonInteractor

    @Inject
    lateinit var clubInteractor: ClubInteractor

    @Inject
    lateinit var cardInteractor: CardInteractor

    init {
        Injector.inject(this)
    }

    fun onPinEntered(pin: String) {
        viewState.startLoading()
        api.personDelete(ConfirmationParams(pin)).unboxAsync().subscribe({
            viewState.finishLoading()
            authInteractor.reset()
            personInteractor.reset()
            clubInteractor.reset()
            cardInteractor.reset()
            viewState.success()
        }, {
            viewState.finishLoading()
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            }
        })
    }

}