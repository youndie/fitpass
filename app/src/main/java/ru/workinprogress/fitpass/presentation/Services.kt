package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.*
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.CardInteractor
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.domain.UnpaidInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 06.03.2018.
 */

interface ServicesView : MvpView {
    fun showBlocks(blocks: Array<AvailableBlock>)
    fun showUnpaid(unpaids: Array<UnpaidService>)
    fun unpaidLoading(it: Boolean)
    abstract fun showBackgrounds(contracts: String?, blocks: String?, services: String?)

}

@InjectViewState
class ServicesPresenter : BasePresenter<ServicesView>() {

    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var api: Api
    @Inject
    lateinit var clubInteractor: ClubInteractor
    @Inject
    lateinit var unpaidInteractor: UnpaidInteractor
    @Inject
    lateinit var cardInteractor: CardInteractor

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        title = "Услуги"
        elevation = false
//        api.getContractPricelist(clubInteractor.selectedClubId).unboxAsync().subscribe({
//            viewState.showContracts(it)
//        }, {})
        refreshAvailableBlocks()
        whileAlive {
            unpaidInteractor.observeUnpaidServices.async().subscribe({
                viewState.showUnpaid(it)
            }, {})
        }
        whileAlive {
            unpaidInteractor.updateEventSubject.async().subscribe({
                viewState.unpaidLoading(it)
            }, {})
        }

        clubInteractor.selectedClub?.async()?.subscribe({
            it?.let {
                viewState.showBackgrounds(it.contractsImage?.Uri, it.blocksImage?.Uri, it.servicesImage?.Uri)
            }
        }, {})
        unpaidInteractor.load().async().subscribe({
        }, {
            it.toString()
        })
        cardInteractor.load().async().subscribe({}, {})

    }

    fun refreshUnpaid() {
        unpaidInteractor.load().async().subscribe({}, {})
    }

    fun refreshAvailableBlocks() {
        api.getAvailableBlocks(clubInteractor.selectedClubId).unboxAsync().map { it.filter { it.SeancesLeft > 0 }.toTypedArray() }.subscribe({
            viewState.showBlocks(it)
        }, {
            it.toString()
        })
    }
}
