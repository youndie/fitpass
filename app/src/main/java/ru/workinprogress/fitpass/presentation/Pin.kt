package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.AuthInteractor
import javax.inject.Inject

interface PinView : MvpView {

    fun success()
    fun error()

}

@InjectViewState
class PinPresenter : MvpPresenter<PinView>() {
    @Inject
    lateinit var authInteractor: AuthInteractor

    fun onPinEntered(pin: String) {
        if (authInteractor.checkPin(pin)) {
            viewState.success()
        } else {
            viewState.error()
        }
    }

    init {
        Injector.inject(this)
    }


}