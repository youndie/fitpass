package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.BuildConfig
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.AuthService
import ru.workinprogress.fitpass.api.serializers.unbox
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.data.API_KEY
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.AuthInteractor
import ru.workinprogress.fitpass.domain.ErrorInteractor
import ru.workinprogress.fitpass.ui.activity.LoginActivity
import ru.workinprogress.fitpass.ui.activity.async
import ru.workinprogress.fitpass.utils.Validation
import javax.inject.Inject
import retrofit2.adapter.rxjava2.Result.response
import okhttp3.ResponseBody
import ru.workinprogress.fitpass.api.params.*
import ru.workinprogress.fitpass.api.serializers.ApiResponseError
import ru.workinprogress.fitpass.api.serializers.Login
import ru.workinprogress.fitpass.utils.Format
import ru.workinprogress.fitpass.utils.Format.targetYMDHMSFormat
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by panic on 22.02.2018.
 */

interface LoginPhoneView : MvpView {
    fun showInvalidPhone()
    fun showError()
    fun showPhoneLayout()
    fun showCodeLayout()
    fun startLogin()
    fun finishLogin()
    fun showLoginError()
    fun success()

}

@InjectViewState
class LoginPhonePresenter : MvpPresenter<LoginPhoneView>() {
    @Inject
    lateinit var api: Api

    init {
        Injector.inject(this)
    }

    var phone = ""

    @Inject
    lateinit var authInteractor: AuthInteractor

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showPhoneLayout()
    }

    fun onLogin(phone: String) {
        if (!Validation.isValidPhone(phone)) {
            viewState.showInvalidPhone()
            return
        }

        val phoneNormalize = Validation.phoneNormalize(phone)

        if (BuildConfig.DEBUG) {
            viewState.showCodeLayout()
//            LoginActivity.LoginNavigation.router.navigateTo(LoginActivity.LoginNavigation.sms, phoneNormalize)
        }
        this.phone = phoneNormalize
        api.sendSingleUserPassword(SendConfirmationParams(phoneNormalize)).unboxAsync().subscribe({
            viewState.showCodeLayout()
//            LoginActivity.LoginNavigation.router.navigateTo(LoginActivity.LoginNavigation.sms, phoneNormalize)
        }, {
            viewState.showError()
        })
    }

    fun onPinEntered(pin: String) {
        viewState.startLogin()
        api.login(LoginParams(2, Phone = phone, SingleUsePassword = pin)).unboxAsync().subscribe({
            authInteractor.authService.save(it)
            viewState.finishLogin()
            viewState.success()
        }, {
            viewState.finishLogin()
            viewState.showLoginError()
        })
    }
}

interface LoginSMSView : MvpView {

}

@InjectViewState
class LoginSMSPresenter(val phone: String) : MvpPresenter<LoginSMSView>() {

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var authService: AuthService

    init {
        Injector.inject(this)
    }


}

interface LoginView : MvpView {
    fun startLogin()
    fun finishLogin()
    @StateStrategyType(SkipStrategy::class)
    fun success()

    fun showLoginError(clientMessage: String)

}

@InjectViewState
class LoginPresenter : MvpPresenter<LoginView>() {

    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var authInteractor: AuthInteractor

    @Inject
    lateinit var errorInteractor: ErrorInteractor

    private fun login(loginParams: LoginParams) {
        viewState.startLogin()
        authInteractor.advancedLogin(loginParams).async().subscribe({
            viewState.finishLogin()
            viewState.success()
        }, {
            errorInteractor.toApiError(it)?.let {
                viewState.showLoginError(it.ClientMessage)
            }
            viewState.finishLogin()
        })
    }

    fun onLoginLogin(login: String, password: String) {
        login(LoginParams(2, Login = login, Password = password))
    }

    fun onEmailLogin(email: String, password: String) {
        if (Validation.isEmailValid(email)) {
            login(LoginParams(2, Email = email, Password = password))
        } else {
            viewState.showLoginError("Введен недопустимый адрес электронной почты")
        }
    }

    fun onPhoneLogin(phone: String, password: String) {
        login(LoginParams(2, Phone = Validation.phoneNormalize(phone), Password = password))
    }
}

interface PasswordRecoverView : MvpView {
    fun startRecover()
    fun finishRecover()
    fun showRecoverError(message: String)
    fun codeSended()
    fun showFields()
    fun startChanging()
    fun finishChanging()
    fun success()
}

@InjectViewState
class PasswordRecoverPresenter : MvpPresenter<PasswordRecoverView>() {

    @Inject
    lateinit var api: Api
    @Inject
    lateinit var errorInteractor: ErrorInteractor
    private var code = ""
    private var login: String? = null
    private var email: String? = null
    private var phone: String? = null


    init {
        Injector.inject(this)
    }

    fun onLoginRecover(login: String) {
        this.login = login
        recover(PasswordCodeParams(Login = login))
    }

    fun onEmailRecover(email: String) {
        this.email = email
        if (Validation.isEmailValid(email)) {
            recover(PasswordCodeParams(Email = email))
        } else {
            viewState.showRecoverError("Введен недопустимый адрес электронной почты")
        }
    }

    fun onPhoneRecover(phone: String) {
        this.phone = phone
        recover(PasswordCodeParams(Phone = Validation.phoneNormalize(phone)))
    }

    private fun recover(params: PasswordCodeParams) {
        viewState.startRecover()
        api.requestPasswordChangeCode(params).unboxAsync().subscribe({
            viewState.finishRecover()
            if (it.MessageSent) {
                viewState.codeSended()
            } else {
                viewState.showRecoverError("Следующий код может быть отправлен не ранее ${SimpleDateFormat("HH:mm dd.MM.yyyy").apply {
                    timeZone = Calendar.getInstance().timeZone
                }.format(SimpleDateFormat("yyyy-MM-dd HH:mm:ss").apply { timeZone = TimeZone.getTimeZone("UTC") }.parse(it.NextAttemptMinTime))}")
            }
        }, {
            viewState.finishRecover()
            errorInteractor.toApiError(it)?.let {
                viewState.showRecoverError(it.ClientMessage)
            }
        })
    }

    fun onPinEntry(pin: String) {
        code = pin
        viewState.showFields()
    }

    fun onChangePassword(new: String) {
        viewState.startChanging()
        api.changePassword(ChangePasswordParams(Login = login, Phone = phone, Email = email, Code = code, NewPassword = new)).unboxAsync().subscribe({
            viewState.finishChanging()
            viewState.success()
        }, {
            viewState.finishChanging()
            viewState.codeSended()
            errorInteractor.toApiError(it)?.let {
                viewState.showRecoverError(it.ClientMessage)
            }
        })
    }

}