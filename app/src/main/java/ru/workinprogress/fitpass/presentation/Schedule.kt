package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.Observable
import io.reactivex.functions.Function3
import org.joda.time.DateTime
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.Lesson
import ru.workinprogress.fitpass.api.serializers.LessonType
import ru.workinprogress.fitpass.api.serializers.Master
import ru.workinprogress.fitpass.api.serializers.Room
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ScheduleInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 06.03.2018.
 */

class UIWeek(val dateTime: DateTime, val items: List<UIScheduleItem>)

class UIScheduleItem(val id: String, val title: String, val place: String, val trainer: String?, var startTime: String, var showStartTime: Boolean = false, var requirePayment: Boolean)
class UILessonDetail(val lesson: Lesson, val lessonType: LessonType?, val master: Master?, val room: Room?)
interface ScheduleView : MvpView {
    fun showSchedule(weeks: List<UIWeek>)
    fun showCurrentDay(date: DateTime, page: Int?)
    fun startLoading()
    fun finishLoading()
}

@InjectViewState
class SchedulePresenter : TitledPresenter<ScheduleView>() {

    @Inject
    lateinit var scheduleInteractor: ScheduleInteractor

    init {
        Injector.inject(this)
        scheduleInteractor.loadSchedule(true).async().subscribe({}, {})
    }

    private var modeAll = true

    var userSchedule = mutableListOf<UIWeek>()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        title = "Расписание"
        elevation = false
        onModeChanged(modeAll)
    }

    fun onDateChanged(date: DateTime) {
        viewState.showCurrentDay(date, userSchedule.map { it.dateTime }.indexOf(date))
    }

    fun onModeChanged(all: Boolean) {
        this.modeAll = all
        viewState.startLoading()
        scheduleInteractor.loadSchedule(!modeAll).async().subscribe({
            userSchedule.clear()
            userSchedule.addAll(it)
            viewState.finishLoading()
            viewState.showSchedule(userSchedule)
            onDateChanged(DateTime.now().withTimeAtStartOfDay())
        }, {
            viewState.finishLoading()
        })
    }

    fun update() {
        onModeChanged(modeAll)
    }

}

