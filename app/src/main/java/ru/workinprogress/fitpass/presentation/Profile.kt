package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpView
import io.reactivex.rxkotlin.Observables
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.*
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.AccountsInteractor
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.domain.PersonInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 05.03.2018.
 */

interface ProfileView : MvpView {
    fun showPerson(person: Person)
    fun showClub(clubs: Array<Club>, club: Club)
    fun showAccounts(accounts: Array<Account>)
    fun showContracts(contracts: Array<ContractStatus>)
    fun showVisits(visits: Array<Visit>)
    fun startLoading()
    fun showEmptyClubs()
    fun accountsLoaded()
}

@InjectViewState
class ProfilePresenter : BasePresenter<ProfileView>() {

    @Inject
    lateinit var personInteractor: PersonInteractor

    @Inject
    lateinit var clubInteractor: ClubInteractor

    @Inject
    lateinit var accountsInteractor: AccountsInteractor

    @Inject
    lateinit var api: Api

    init {
        Injector.inject(this)
        elevation = false
        alpha = true
        whileAlive {
            Observables.combineLatest(
                    clubInteractor.observeClubs,
                    clubInteractor.observeSelectedClubId)
            { u, p -> u to p }.async().subscribe({ pair ->
                if (pair.first.isEmpty()) {
                    viewState.showEmptyClubs()
                } else {
                    viewState.showClub(pair.first, pair.first.first { it.ID == pair.second })
                    loadClubData(pair.second)
                }
            })
        }
        whileAlive {
            accountsInteractor.observeAccount.async().subscribe({
                viewState.showAccounts(it)
            }, {})
        }
    }

    var second = ""

    fun loadClubData(second: String) {
        this.second = second
        viewState.startLoading()
        accountsInteractor.update().async().subscribe({
            viewState.accountsLoaded()
        }, {})
        api.getContracts(second).unboxAsync().subscribe({
            viewState.showContracts(it)
        }, {})
        api.getVisits(second).unboxAsync().subscribe({
            viewState.showVisits(it)
        }, {})
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        personInteractor.person.async().subscribe({
            viewState.showPerson(it)
        }, {})


    }

    fun onClubChanged(club: Club) {
        clubInteractor.selectedClubId = club.ID
    }

    fun updateContracts() {
        loadClubData(second)
    }


}
