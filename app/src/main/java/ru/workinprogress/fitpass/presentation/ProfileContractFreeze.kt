package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.api.serializers.ContractStatus
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ContractsInteractor
import ru.workinprogress.fitpass.domain.ErrorInteractor
import ru.workinprogress.fitpass.ui.activity.async
import ru.workinprogress.fitpass.utils.Format.sourceYMDFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by panic on 06.03.2018.
 */

interface ProfileContractFreezeView : MvpView {
    fun show(start: Date, end: Date, canSuspendDays: Int)
    fun startFreeze()
    fun endFreeze()
    fun showError(clientMessage: String)
    fun success()

}

@InjectViewState
class ProfileContractFreezePresenter(private val contractStatus: ContractStatus) : MvpPresenter<ProfileContractFreezeView>() {

    var startDate = Calendar.getInstance().apply {
        add(Calendar.DAY_OF_YEAR, 1)
    }.time

    var endDate = Calendar.getInstance().apply {
        time = startDate
        add(Calendar.DAY_OF_YEAR, contractStatus.CanSuspendDays)
    }.time

    fun onStartDate(year: Int, monthOfYear: Int, dayOfMonth: Int) {
        startDate = Calendar.getInstance().apply {
            set(Calendar.YEAR, year)
            set(Calendar.MONTH, monthOfYear)
            set(Calendar.DAY_OF_MONTH, dayOfMonth)
        }.time
        val max = Calendar.getInstance().apply {
            time = startDate
            add(Calendar.DAY_OF_YEAR, contractStatus.CanSuspendDays)
        }.time
        if (endDate.after(max)) {
            endDate = max
        }
        val min = Calendar.getInstance().apply {
            time = startDate
            add(Calendar.WEEK_OF_YEAR, 1)
        }.time
        if (endDate.before(min)) {
            endDate = min
        }
        showDates()
    }

    fun onEndDate(year: Int, monthOfYear: Int, dayOfMonth: Int) {
        endDate = Calendar.getInstance().apply {
            set(Calendar.YEAR, year)
            set(Calendar.MONTH, monthOfYear)
            set(Calendar.DAY_OF_MONTH, dayOfMonth)
        }.time
        showDates()
    }

    @Inject
    lateinit var contractsInteractor: ContractsInteractor

    @Inject
    lateinit var errorInteractor: ErrorInteractor

    fun showDates() {
        viewState.show(startDate, endDate, contractStatus.CanSuspendDays)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        showDates()
    }

    fun onFreeze() {
        viewState.startFreeze()
        contractsInteractor.suspend(contractStatus.ID, sourceYMDFormat.format(startDate), sourceYMDFormat.format(endDate)).async().subscribe({
            viewState.endFreeze()
            viewState.success()
        }, {
            viewState.endFreeze()
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            } ?: viewState.showError("Произошла ошибка")
        })
    }

    init {
        Injector.inject(this)
        if (contractStatus.SuspendInfo != null) {
            startDate = sourceYMDFormat.parse(contractStatus.SuspendInfo.BeginDate)
            endDate = sourceYMDFormat.parse(contractStatus.SuspendInfo.EndDate)
        } else {
            startDate = Calendar.getInstance().apply {
                add(Calendar.DAY_OF_YEAR, 1)
            }.time

            endDate = Calendar.getInstance().apply {
                time = startDate
                add(Calendar.DAY_OF_YEAR, contractStatus.CanSuspendDays)
            }.time

        }
    }

}