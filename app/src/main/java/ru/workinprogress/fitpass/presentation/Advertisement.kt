package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.RegisterImpressionParams
import ru.workinprogress.fitpass.api.serializers.Advertisement
import ru.workinprogress.fitpass.api.serializers.News
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

interface AdvertisementView : MvpView {
    fun showAdv(advertisement: Advertisement)

}

@InjectViewState
class AdvertisementPresenter(val advertisement: Advertisement) : TitledPresenter<AdvertisementView>() {

    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var api: Api

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showAdv(advertisement)
        api.registerAdvImpression(RegisterImpressionParams(advertisement.ID)).async().subscribe({}, {})

    }

}
