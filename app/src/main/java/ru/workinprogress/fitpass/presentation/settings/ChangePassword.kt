package ru.workinprogress.fitpass.presentation.settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.ChangePasswordParams
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.PersonInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 15.03.2018.
 */

interface ChangePasswordView : MvpView {
    fun showLoading()
    fun showError()
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun success()

}

@InjectViewState
class ChangePasswordPresenter : MvpPresenter<ChangePasswordView>() {

    @Inject
    lateinit var personInteractor: PersonInteractor

    @Inject
    lateinit var api: Api

    init {
        Injector.inject(this)
    }

    fun changePassword(old: String, new: String) {
        viewState.showLoading()
        personInteractor.person.async().subscribe({
            api.changePassword(when {
                it.MainEmailConfirmed -> ChangePasswordParams(Email = it.MainEmail, OldPassword = old, NewPassword = new)
                it.MainPhoneConfirmed -> ChangePasswordParams(Phone = it.MainPhone, OldPassword = old, NewPassword = new)
                else -> ChangePasswordParams(Login = it.Login, OldPassword = old, NewPassword = new)
            }).unboxAsync().subscribe({
                viewState.success()
            }, {
                viewState.showError()
            })

        }, {
            viewState.showError()
        })

    }

}