package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.PayParams
import ru.workinprogress.fitpass.api.params.QueryPaymentParams
import ru.workinprogress.fitpass.api.serializers.ContractStatus
import ru.workinprogress.fitpass.api.serializers.PaymentCard
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.*
import ru.workinprogress.fitpass.ui.activity.async
import java.text.SimpleDateFormat
import javax.inject.Inject

/**
 * Created by panic on 20.03.2018.
 */

interface ContractView : MvpView {
    fun showContract(contractStatus: ContractStatus)
    fun startPay()
    fun showError()
    fun finishPay()
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showPaymentPage(uri: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showPaymentVariants(cards: CardsInfo)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun hidePaymentsVariants()

    fun showSuccess(message: String)
    fun showFailed(message: String)
    fun finishResuming()
    fun startResuming()

}

@InjectViewState
class ContractPresenter(var contractStatus: ContractStatus) : LifecyclePresenter<ContractView>() {

    val sdf = SimpleDateFormat("yyyy-MM-dd")

    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var api: Api
    @Inject
    lateinit var clubInteractor: ClubInteractor
    @Inject
    lateinit var personInteractor: PersonInteractor
    @Inject
    lateinit var cardInteractor: CardInteractor
    @Inject
    lateinit var contractsInteractor: ContractsInteractor

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        cardInteractor.load().async().subscribe({}, {})
        viewState.showContract(contractStatus)
    }

    fun onPayClicked() {
        contractStatus.ExpectedPayments.minBy { sdf.parse(it.DateDue).time }?.let {
            cardInteractor.cardsByLegalBankInfo(it.LegalBankInfo).async().subscribe({
                viewState.showPaymentVariants(it)
            }, {})

        }
    }

    fun onCardPayClicked(card: PaymentCard) {
        viewState.startPay()
        viewState.hidePaymentsVariants()
        contractStatus.ExpectedPayments.minBy { sdf.parse(it.DateDue).time }?.let { expectedPayment ->
            api.payContract(contractStatus.ID, clubInteractor.selectedClubId, card.ID, PayParams(expectedPayment.Amount.toFloat(), expectedPayment.LegalBankInfo)).unboxAsync().subscribe({
                viewState.finishPay()
                if (it.Success) {
                    contractsInteractor.update().async().subscribe({}, {})
                    viewState.showSuccess(it.Message)
                } else {
                    viewState.showFailed(it.Message)
                }
            }, {
                viewState.finishPay()
                viewState.showError()
            })
        }

    }

    fun onNewCardPayClicked() {
        viewState.startPay()
        viewState.hidePaymentsVariants()
        contractStatus.ExpectedPayments.minBy { sdf.parse(it.DateDue).time }?.let {
            api.queryPayment(contractStatus.ID, clubInteractor.selectedClubId, QueryPaymentParams(it.Amount.toFloat(), it.LegalBankInfo, false)).unboxAsync().subscribe({
                viewState.finishPay()
                it.Data["Uri"]?.let {
                    viewState.showPaymentPage(it)
                } ?: viewState.showError()
            }, {
                viewState.finishPay()
                viewState.showError()
            })
        }
    }

    fun resumeContract() {
        viewState.startResuming()
        api.resumeContract(contractStatus.ID, clubInteractor.selectedClubId, personInteractor.personId).unboxAsync().subscribe({
            contractsInteractor.update().async().subscribe({}, {})
            viewState.showContract(it)
            viewState.finishResuming()
        }, {
            viewState.finishResuming()
        })
    }

    fun onUpdate() {
        cardInteractor.load().async().subscribe({}, {})
        contractsInteractor.update().async().subscribe({
            contractStatus = it.first { it.ID == contractStatus.ID }
            viewState.showContract(contractStatus)
        }, {})
    }

}