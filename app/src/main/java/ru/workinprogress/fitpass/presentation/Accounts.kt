package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.api.AccountPayApi
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.PayParams
import ru.workinprogress.fitpass.api.params.QueryPaymentParams
import ru.workinprogress.fitpass.api.serializers.Account
import ru.workinprogress.fitpass.api.serializers.PaymentCard
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.*
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 26.03.2018.
 */

interface AccountsView : MvpView {
    fun showError(clientMessage: String = "Произошла ошибка")
    fun startLoading()
    fun finishLoading()
    fun showAccounts(accounts: Array<Account>)
    fun showPayVariants(cardsByLegalBankInfo: CardsInfo)
    fun startPay()
    fun finishPay()
    fun paySuccess()
    fun showPaymentPage(url: String)

}

@InjectViewState
class AccountsPresenter : LifecyclePresenter<AccountsView>() {

    @Inject
    lateinit var api: Api
    @Inject
    lateinit var accountPayApi: AccountPayApi
    @Inject

    lateinit var accountsInteractor: AccountsInteractor

    @Inject
    lateinit var clubInteractor: ClubInteractor
    @Inject
    lateinit var cardInteractor: CardInteractor

    private var defaultLegalBankInfo: String = ""

    fun load() {
        viewState.startLoading()
        cardInteractor.load().async().subscribe({
            accountsInteractor.update().async().subscribe({
                viewState.finishLoading()
            }, {
                viewState.finishLoading()
                viewState.showError()
            })
        }, {})


    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        load()
        clubInteractor.selectedClub
                ?.map { it?.LegalEntities?.firstOrNull { it.IsDefaultForAccountPayment }?.BankInfo }
                ?.filter { it != null }?.async()?.subscribe({
                    defaultLegalBankInfo = it!!
                }, {})
    }

    init {
        Injector.inject(this)
        whileAlive {
            accountsInteractor.observeAccount.async().subscribe({
                viewState.showAccounts(it)
            }, {})
        }

    }

    var accountId = ""
    var legalBankInfo: String? = ""

    fun onPayAccountClicked(account: Account) {
        accountId = account.AccountID
        legalBankInfo = account.LegalBankInfo
        cardInteractor.cardsByLegalBankInfo(legalBankInfo).async().subscribe({
            viewState.showPayVariants(it)
        }, {
            it.toString()
        })
    }


    fun onNewCardPayClicked(sum: Int) {
        viewState.startPay()
        api.queryAccountPayment(clubInteractor.selectedClubId, accountId, QueryPaymentParams(sum.toFloat(), legalBankInfo
                ?: defaultLegalBankInfo)).unboxAsync().subscribe({
            viewState.finishPay()
            it.Data["Uri"]?.let {
                viewState.showPaymentPage(it)
            } ?: viewState.showError()
        }, {
            viewState.finishPay()
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            } ?: viewState.showError()
        })

    }

    @Inject
    lateinit var errorInteractor: ErrorInteractor

    fun onCardPayClicked(card: PaymentCard, sum: Int) {
        viewState.startPay()
        api.cardAccountPayment(clubInteractor.selectedClubId, accountId, card.ID, PayParams(sum.toFloat(), card.LegalBankInfo
                ?: defaultLegalBankInfo))
                .unboxAsync()
                .subscribe({
                    viewState.finishPay()
                    if (it.Success) {
                        viewState.showError(it.Message)
                        viewState.paySuccess()
                        load()
                    } else {
                        viewState.showError(it.Message)
                    }
                }, {
                    viewState.finishPay()
                    errorInteractor.toApiError(it)?.let {
                        viewState.showError(it.ClientMessage)
                    } ?: viewState.showError()
                })
    }


}