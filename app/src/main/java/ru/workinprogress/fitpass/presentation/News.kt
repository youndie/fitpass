package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.Advertisement
import ru.workinprogress.fitpass.api.serializers.News
import ru.workinprogress.fitpass.api.serializers.SpecialOffer
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.domain.NewsInteractor
import ru.workinprogress.fitpass.domain.SpecialOffersInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 15.03.2018.
 */

interface NewsView : MvpView {
    fun showSpecialOffers(offers: Array<SpecialOffer>)
    fun showNews(news: MutableList<News>)
    fun showAds(ads: Array<Advertisement>)
    fun adsLoading(b: Boolean)
    fun newsLoading(b: Boolean)
    fun offersLoading(b: Boolean)
    fun showError()
}

@InjectViewState
class NewsPresenter : BasePresenter<NewsView>() {
    @Inject
    lateinit var newsInteractor: NewsInteractor

    @Inject
    lateinit var specialOffersInteractor: SpecialOffersInteractor

    @Inject
    lateinit var api: Api

    init {
        Injector.inject(this)
        title = "Новости, акции и реклама"
        elevation = false

        whileAlive {
            specialOffersInteractor.observeSpecialOffers.async().subscribe({
                viewState.showSpecialOffers(it)
            }, {})
        }
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        refreshOffers()
        refreshNews()
        refreshAds()
    }


    fun refreshOffers() {
        viewState.offersLoading(true)
        specialOffersInteractor.refresh().async().subscribe({
            viewState.offersLoading(false)
        }, {
            viewState.offersLoading(false)
            viewState.showError()
        })
    }



    fun refreshNews() {
        viewState.newsLoading(true)
        newsInteractor.loadNews().async().subscribe({
            viewState.showNews(it)
            viewState.newsLoading(false)
        }, {
            viewState.newsLoading(false)
            viewState.showError()
        })

    }

    fun refreshAds() {
        viewState.adsLoading(true)
        newsInteractor.loadAds().async().subscribe({
            viewState.showAds(it)
            viewState.adsLoading(false)
        }, {
            viewState.adsLoading(false)
            viewState.showError()
        })


    }


}