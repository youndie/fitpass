package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.RegisterImpressionParams
import ru.workinprogress.fitpass.api.serializers.SpecialOffer
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.SpecialOffersInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 16.03.2018.
 */

interface SpecialOfferView : MvpView {
    fun showOffer(specialOffer: SpecialOffer)
    fun startAction()
    fun finishAction()

    fun showError()

}

@InjectViewState
class SpecialOfferPresenter(private val specialOffer: SpecialOffer) : MvpPresenter<SpecialOfferView>() {

    @Inject
    lateinit var specialOffersInteractor: SpecialOffersInteractor

    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var api: Api


    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showOffer(specialOffer)
        api.registerSpecialOfferImpression(RegisterImpressionParams(specialOffer.UniqueID)).async().subscribe({}, {})
    }


    fun onOfferAction() {
        viewState.startAction()
        specialOffersInteractor.onOfferAccept(specialOffer.UniqueID).async().subscribe({
            viewState.finishAction()
        }, {
            viewState.showError()
        })
    }

    fun onOfferReject() {
        specialOffersInteractor.onOfferReject(specialOffer.UniqueID).async().async().subscribe({
            viewState.finishAction()
        }, {
            viewState.showError()
        })
    }

}