package ru.workinprogress.fitpass.presentation.settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.EditPersonParams
import ru.workinprogress.fitpass.api.params.EmailConfirmationParams
import ru.workinprogress.fitpass.api.params.PhoneConfirmationParams
import ru.workinprogress.fitpass.api.serializers.*
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ErrorInteractor
import ru.workinprogress.fitpass.domain.PersonInteractor
import ru.workinprogress.fitpass.ui.activity.async
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by panic on 15.03.2018.
 */


interface EditProfileView : MvpView {
    fun showPerson(person: Person)
    fun showCountries(countries: Array<Country>, code: String)
    fun showLanguages(languages: Array<Language>, code: String)
    fun showCities(cities: Array<City>, cityId: Int)
    fun startLoading()
    fun showDate(date: String?)
    fun finishLoading()
    fun showError(clientMessage: String)
    fun emailCodeSend()
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun phoneCodeSend(phone: String, login: String)

}

@InjectViewState
class EditProfilePresenter : MvpPresenter<EditProfileView>() {

    @Inject
    lateinit var personInteractor: PersonInteractor
    @Inject
    lateinit var errorInteractor: ErrorInteractor
    @Inject
    lateinit var api: Api

    init {
        Injector.inject(this)
    }

    var countryCode = ""
    var languageCode = ""
    var cityId = 0
    var sex = -1

    private var email = ""
    private var login = ""
    private var phone = ""

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        refresh()

    }

    fun onCountrySelected(country: Country) {
        this.countryCode = country.Code
        api.getCities(country.Code).unboxAsync().subscribe({
            viewState.showCities(it, cityId)
        }, {})
    }

    fun onCitySelected(city: City) {
        this.cityId = city.ID
    }

    fun onEditProfile(email: String) {
        viewState.startLoading()
        api.editPerson(EditPersonParams(languageCode, email, cityId, countryCode, sex.takeIf { it != -1 }, birthDay?.let { sdf.format(it) })).unboxAsync().subscribe({
            viewState.finishLoading()
            viewState.showPerson(it)
            viewState.showDate(sdf2.format(sdf.parse(it.DateOfBirth)))
        }, {
            viewState.finishLoading()
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            }
        })
    }

    var birthDay: Date? = null

    private val sdf = SimpleDateFormat("yyyy-MM-dd")
    private val sdf2 = SimpleDateFormat("dd MMMM yyyy")


    fun onBirthDaySelected(year: Int, month: Int, day: Int) {
        birthDay = Calendar.getInstance().apply {
            set(Calendar.YEAR, year)
            set(Calendar.MONTH, month)
            set(Calendar.DAY_OF_MONTH, day)
        }.time
        viewState.showDate(sdf2.format(birthDay))
    }

    fun onSexChanged(male: Boolean) {
        sex = if (male) 1 else 2
    }

    fun onEmailConfirmClicked() {
        viewState.startLoading()
        api.sendEmailConfirmation(EmailConfirmationParams(login, email)).unboxAsync().subscribe({
            viewState.finishLoading()

            if (it.MessageSent) {
                viewState.emailCodeSend()
            } else {
                viewState.showError("Следующий запрос может быть отправлен не ранее ${SimpleDateFormat("HH:mm dd.MM.yyyy").apply {
                    timeZone = Calendar.getInstance().timeZone
                }.format(SimpleDateFormat("yyyy-MM-dd HH:mm:ss").apply { timeZone = TimeZone.getTimeZone("UTC") }.parse(it.NextAttemptMinTime))}")
            }
        }, {
            viewState.finishLoading()
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            }
        })
    }

    fun onPhoneConfirmClicked() {
        viewState.startLoading()
        api.sendPhoneConfirmation(PhoneConfirmationParams(phone)).unboxAsync().subscribe({
            if (it.MessageSent) {
                viewState.phoneCodeSend(phone, login)
            } else {
                viewState.showError("Следующий код может быть отправлен не ранее ${SimpleDateFormat("HH:mm dd.MM.yyyy").apply {
                    timeZone = Calendar.getInstance().timeZone
                }.format(SimpleDateFormat("yyyy-MM-dd HH:mm:ss").apply { timeZone = TimeZone.getTimeZone("UTC") }.parse(it.NextAttemptMinTime))}")
            }
            viewState.finishLoading()
        }, {
            viewState.finishLoading()
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            }
        })
    }

    fun refresh() {
        personInteractor.person.async().subscribe({ person ->
            viewState.showPerson(person)
            this.email = person.MainEmail
            this.login = person.Login
            this.phone = person.MainPhone
            viewState.showDate(sdf2.format(sdf.parse(person.DateOfBirth)))
            countryCode = person.ResidenceCountry.Code
            languageCode = person.Language.Code
            cityId = person.ResidenceCity.ID

            api.getCountries().unboxAsync().subscribe({
                viewState.showCountries(it, countryCode)
            }, {})
            api.getLanguages().unboxAsync().subscribe({
                viewState.showLanguages(it, languageCode)
            }, {})
        }, {})
    }

}

