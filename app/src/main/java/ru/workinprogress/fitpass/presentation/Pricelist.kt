package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.BuyContractParams
import ru.workinprogress.fitpass.api.params.SaleParams
import ru.workinprogress.fitpass.api.serializers.ContractPriceItem
import ru.workinprogress.fitpass.api.serializers.PricelistItem
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.domain.ErrorInteractor
import ru.workinprogress.fitpass.domain.PersonInteractor
import ru.workinprogress.fitpass.domain.UnpaidInteractor
import ru.workinprogress.fitpass.ui.activity.async
import java.util.*
import javax.inject.Inject

/**
 * Created by panic on 07.03.2018.
 */

interface PricelistView : MvpView {
    fun showPricelist(pricelist: List<PricelistItem>)
    fun startLoading()
    fun finishLoading()
    fun showError()

}

@InjectViewState
class PricelistPresenter(val type: Int) : MvpPresenter<PricelistView>() {
    fun itemTypeToType(itemType: Int) = when (itemType) {
        1 -> 0
        2 -> 2
        3 -> 1
        515 -> 1
        else -> {
            2
        }
    }

    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var clubInteractor: ClubInteractor

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        load()
    }

    fun load() {
        viewState.startLoading()
        api.getPricelist(clubInteractor.selectedClubId).unboxAsync().subscribe({
            viewState.finishLoading()
            viewState.showPricelist(it.filter { itemTypeToType(it.ItemType) == type })
        }, {
            viewState.finishLoading()
            viewState.showError()
        })
    }
}

interface ContractPriceListView : MvpView {
    fun showError()
    fun showPricelist(pricelist: Array<ContractPriceItem>)
    fun startLoading()
    fun finishLoading()

}

@InjectViewState
class ContractPriceListPresenter : MvpPresenter<ContractPriceListView>() {

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var clubInteractor: ClubInteractor

    init {
        Injector.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        load()
    }

    fun load() {
        viewState.startLoading()
        api.getContractPricelist(clubInteractor.selectedClubId).unboxAsync().subscribe({
            viewState.finishLoading()
            viewState.showPricelist(it)
        }, {
            viewState.finishLoading()
            viewState.showError()
        })
    }
}

interface ContractPricelistItemView : MvpView {
    fun showItem(contractPriceItem: ContractPriceItem)
    fun startBuying()
    fun finishBuying()
    fun success()
    fun showError(clientMessage: String)
}

@InjectViewState
class ContractPricelistItemPresenter(val contractPriceItem: ContractPriceItem) : MvpPresenter<ContractPricelistItemView>() {

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var clubInteractor: ClubInteractor

    @Inject
    lateinit var personInteractor: PersonInteractor

    @Inject
    lateinit var errorInteractor: ErrorInteractor

    init {
        Injector.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showItem(contractPriceItem)
    }

    fun onBuyClicked() {
        viewState.startBuying()
        api.buyContract(UUID.randomUUID().toString(), personInteractor.personId, clubInteractor.selectedClubId, BuyContractParams(contractPriceItem.Prices.first { it.AllowIndependentSale }.ID)).unboxAsync().subscribe({
            viewState.finishBuying()
            viewState.success()
        }, {
            viewState.finishBuying()
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            }
        })

    }

}

interface PricelistItemView : MvpView {
    fun showItem(item: PricelistItem)
    fun startLoading()
    fun finishLoading()
    fun showError(clientMessage: String)
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun success()

}

@InjectViewState
class PricelistItemPresenter(val item: PricelistItem) : MvpPresenter<PricelistItemView>() {
    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var personInteractor: PersonInteractor
    @Inject
    lateinit var clubInteractor: ClubInteractor
    @Inject
    lateinit var unpaidInteractor: UnpaidInteractor

    @Inject
    lateinit var errorInteractor: ErrorInteractor

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showItem(item)
    }

    fun onBuyClicked() {
        viewState.startLoading()
        api.saleNew(UUID.randomUUID().toString().substring(0..8), personInteractor.personId, clubInteractor.selectedClubId, SaleParams(item.ID, 1, item.Price)).unboxAsync().subscribe({
            unpaidInteractor.load().async().subscribe({}, {})
            viewState.finishLoading()
            viewState.success()
        }, {
            viewState.finishLoading()
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            } ?: viewState.showError("Произошла ошибка")
        })
    }

}