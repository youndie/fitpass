package ru.workinprogress.fitpass.presentation.settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.ConfirmationParams
import ru.workinprogress.fitpass.api.params.PhoneConfirmParams
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ErrorInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

interface ConfirmPhoneView : MvpView {
    fun showError(error: String)
    fun startLoading()
    fun finishLoading()
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun success()
}

@InjectViewState
class ConfirmPhonePresenter(val phone: String, val login: String) : MvpPresenter<ConfirmPhoneView>() {

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var errorInteractor: ErrorInteractor

    init {
        Injector.inject(this)
    }

    fun onPinEntered(pin: String) {
        viewState.startLoading()
        api.phoneConfirm(PhoneConfirmParams(phone, login, pin)).async().subscribe({
            viewState.finishLoading()
            viewState.success()
        }, {
            viewState.finishLoading()
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            }
        })
    }

}