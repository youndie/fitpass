package ru.workinprogress.fitpass.presentation.settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.di.Injector

interface ChangePhoneView : MvpView {


}

@InjectViewState
class ChangePhonePresenter : MvpPresenter<ChangePhoneView>() {
    fun changePhone(password: String, phone: String) {

    }

    init {
        Injector.inject(this)
    }

}