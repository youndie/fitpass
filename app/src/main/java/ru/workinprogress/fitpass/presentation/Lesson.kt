package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.Observable
import io.reactivex.functions.Function3
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.JoinParams
import ru.workinprogress.fitpass.api.serializers.*
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.domain.ErrorInteractor
import ru.workinprogress.fitpass.domain.ScheduleInteractor
import ru.workinprogress.fitpass.domain.UnpaidInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 14.03.2018.
 */

interface LessonView : MvpView {
    fun showLesson(detailTraining: UILessonDetail)
    fun showFreeplaces(capacity: Int)
    fun showJoined(joined: Boolean)
    fun success()
    fun showJoinVariants(blocks: Boolean, items: Boolean, lessonId: String, typeId: String)
    fun showBlocks(availableBlocks: MutableList<AvailableBlock>)
    fun startJoining()
    fun finishJoining()
    fun reloadPlaces()
    fun showError(s: String)
}

interface LessonPricelistView : MvpView {
    fun showItems(items: List<PricelistItem>)
    fun showError()
    fun startLoading()
    fun finishLoading()
}

@InjectViewState
class LessonPricelistDetailPresenter(val item: PricelistItem, val lessonId: String, val lessonTypeId: String) : MvpPresenter<PricelistItemView>() {

    init {
        Injector.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showItem(item)
    }

    @Inject
    lateinit var api: Api
    @Inject
    lateinit var clubInteractor: ClubInteractor
    @Inject
    lateinit var unpaidInteractor: UnpaidInteractor
    @Inject
    lateinit var errorInteractor: ErrorInteractor

    fun onBuyClicked() {
        viewState.startLoading()
        api.reservationJoin(clubInteractor.selectedClubId, lessonId, JoinParams(ItemID = item.ID)).unboxAsync().subscribe({
            unpaidInteractor.load().async().subscribe({}, {})
            viewState.finishLoading()
            viewState.success()
        }, {
            viewState.finishLoading()
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            } ?: viewState.showError("Произошла ошибка")
        })

    }

}


@InjectViewState
class LessonPricelistPresenter(val lessonId: String, val lessonTypeId: String) : MvpPresenter<LessonPricelistView>() {

    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var api: Api
    @Inject
    lateinit var clubInteractor: ClubInteractor

    fun load() {
        viewState.startLoading()
        api.getPricelist(clubInteractor.selectedClubId).unbox().map { it.filter { it.AvailableLessonTypeIDs.contains(lessonTypeId) } }.async().subscribe({
            viewState.showItems(it)
            viewState.finishLoading()
        }, {
            viewState.showError()
            viewState.finishLoading()
        })
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        load()
    }

}

@InjectViewState
class LessonPresenter(val id: String, var personLesson: Boolean) : MvpPresenter<LessonView>() {

    @Inject
    lateinit var scheduleInteractor: ScheduleInteractor

    @Inject
    lateinit var clubInteractor: ClubInteractor

    @Inject
    lateinit var api: Api

    init {
        Injector.inject(this)
    }

    val availableBlocks = mutableListOf<AvailableBlock>()
    val availableItems = mutableListOf<PricelistItem>()

    val detail get() = scheduleInteractor.getDetailTraining(id, personLesson)
    val joined get() = scheduleInteractor.personLessons.find { it.ID == id } != null

    private fun showLesson() {
        viewState.showLesson(detail)
        Observable.combineLatest(
                api.getLessonAvailability(clubInteractor.selectedClubId, id).unbox().toObservable(),
                scheduleInteractor.loadAvailableBlocks().map { it.filter { it.AvailableLessonTypeIDs.contains(detail.lesson.LessonTypeID) && it.SeancesLeft > 0 } }.toObservable(),
                api.getPricelist(clubInteractor.selectedClubId).unbox().map { it.filter { it.AvailableLessonTypeIDs.contains(detail.lesson.LessonTypeID) } }.toObservable(),
                Function3<Array<LessonRemainingCapacity>, List<AvailableBlock>, List<PricelistItem>, Triple<Array<LessonRemainingCapacity>, List<AvailableBlock>, List<PricelistItem>>> { t1, t2, t3 -> Triple(t1, t2, t3) }
        ).async().subscribe({
            it.second.let {
                availableBlocks.clear()
                availableBlocks.addAll(it)
            }
            it.third.let {
                availableItems.clear()
                availableItems.addAll(it)
            }
            it.first.let {
                it.first().let {
                    viewState.showFreeplaces(it.RemainingCapacity)
                    viewState.showJoined(joined)
                }
            }
        }, {})

    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        showLesson()

    }

    fun onActionClicked() {
        if (!joined) {
            if (detail.lessonType?.RequirePayment == true) {
                viewState.showJoinVariants(availableBlocks.isNotEmpty(), availableItems.isNotEmpty(), detail.lesson.ID, detail.lesson.LessonTypeID)
            } else {
                viewState.startJoining()
                api.reservationJoin(clubInteractor.selectedClubId, id, JoinParams()).unboxAsync().subscribe({
                    onJoined()
                }, {
                    viewState.finishJoining()
                    errorInteractor.toApiError(it)?.let {
                        viewState.showError(it.ClientMessage)
                    } ?: viewState.showError("Произошла ошибка")
                })
            }
        } else {

        }
    }

    @Inject
    lateinit var errorInteractor: ErrorInteractor

    fun onBlockClicked() {
        viewState.showBlocks(availableBlocks)
    }

    fun onBlockSelected(block: AvailableBlock) {
        viewState.startJoining()
        api.reservationJoin(clubInteractor.selectedClubId, id, JoinParams(BlockID = block.ID)).unboxAsync().subscribe({
            onJoined()
        }, {
            viewState.finishJoining()
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            } ?: viewState.showError("Произошла ошибка")
        })
    }

    fun onJoined() {
        personLesson = true
        viewState.reloadPlaces()
        viewState.showJoined(true)
        scheduleInteractor.loadSchedule(true).async().subscribe({
            showLesson()
            viewState.finishJoining()
        }, {
            viewState.finishJoining()
        })
    }
}