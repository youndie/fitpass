package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.PayParams
import ru.workinprogress.fitpass.api.params.QueryPaymentParams
import ru.workinprogress.fitpass.api.serializers.Account
import ru.workinprogress.fitpass.api.serializers.PaymentCard
import ru.workinprogress.fitpass.api.serializers.UnpaidService
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.*
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 23.03.2018.
 */

interface UnpaidGroupView : MvpView {
    fun showItems(unpaid: List<UnpaidService>)
    fun showSum(sum: Int)
    @StateStrategyType(SkipStrategy::class)
    fun showPayVariants(cards: CardsInfo, accounts: List<Account>)

    @StateStrategyType(SkipStrategy::class)
    fun startPay()

    @StateStrategyType(SkipStrategy::class)
    fun finishPay()

    @StateStrategyType(SkipStrategy::class)
    fun showPaymentPage(page: String)

    @StateStrategyType(SkipStrategy::class)
    fun showError(clientMessage: String = "Произошла ошибка")

    @StateStrategyType(SkipStrategy::class)
    fun paySuccess()

    fun accountsLoading(show: Boolean)
}

@InjectViewState
class UnpaidGroupPresenter(val scopeId: String) : LifecyclePresenter<UnpaidGroupView>() {

    @Inject
    lateinit var unpaidInteractor: UnpaidInteractor

    @Inject
    lateinit var cardInteractor: CardInteractor

    @Inject
    lateinit var clubInteractor: ClubInteractor

    @Inject
    lateinit var api: Api

    init {
        Injector.inject(this)
    }

    val checkedItems = hashSetOf<UnpaidService>()

    private val sales get() = checkedItems.joinToString(separator = ",") { it.ID }
    private val sum get() = checkedItems.sumBy { it.Amount }
    private val accounts = mutableListOf<Account>()


    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        whileAlive {
            unpaidInteractor.observeUnpaidServices.map { it.filter { it.ScopeID == scopeId } }.async().subscribe({
                checkedItems.clear()
                viewState.showItems(it)
                viewState.accountsLoading(true)
                api.getAccounts(clubInteractor.selectedClubId).unboxAsync().subscribe({
                    accounts.clear()
                    accounts.addAll(it.filter { ((it.ScopeID == scopeId || ((it.ScopeID == null) && (scopeId.isEmpty()))) and (it.Amount >= sum)) })
                    viewState.accountsLoading(false)
                }, {
                    viewState.accountsLoading(false)
                })
            })
        }
    }

    fun onCheckChange(item: UnpaidService, b: Boolean) {
        if (b) checkedItems.add(item) else checkedItems.remove(item)
        calculateSum()
    }

    private fun calculateSum() {
        viewState.showSum(checkedItems.sumBy { it.Amount })
    }

    fun onPayClicked() {
        cardInteractor.cardsByLegalBankInfo(checkedItems.first().LegalBankInfo).async().subscribe({
            viewState.showPayVariants(it, accounts)
        }, {})
    }

    @Inject
    lateinit var errorInteractor: ErrorInteractor

    fun onNewCardPayClicked() {
        viewState.startPay()
        api.querySalePayment(clubInteractor.selectedClubId, sales, QueryPaymentParams(sum.toFloat(), checkedItems.first().LegalBankInfo)).unboxAsync().subscribe({
            viewState.finishPay()
            it.Data["Uri"]?.let {
                viewState.showPaymentPage(it)
            } ?: viewState.showError()
        }, {
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            } ?: viewState.showError()
        })

    }

    fun onCardPayClicked(card: PaymentCard) {
        viewState.startPay()
        api.paySaleItems(clubInteractor.selectedClubId, sales, card.ID, PayParams(sum.toFloat(), card.LegalBankInfo.orEmpty())).unboxAsync().subscribe({
            viewState.finishPay()
            if (it.Success) {
                unpaidInteractor.load().async().subscribe({}, {})
                viewState.paySuccess()
            } else {
                viewState.showError(it.Message)
            }

        }, {
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            } ?: viewState.showError()
        })
    }

    fun onAccountPayClicked(account: Account) {
        viewState.startPay()
        api.paySaleItemsFromAccount(clubInteractor.selectedClubId, sales, account.AccountID).unboxAsync().subscribe({
            viewState.finishPay()
            unpaidInteractor.load().async().subscribe({}, {})
            viewState.paySuccess()
        }, {
            errorInteractor.toApiError(it)?.let {
                viewState.showError(it.ClientMessage)
            } ?: viewState.showError()
        })
    }

}