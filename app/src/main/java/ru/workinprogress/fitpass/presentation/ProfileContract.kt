package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView

/**
 * Created by panic on 06.03.2018.
 */

interface ProfileContractView : MvpView {

}

class ProfileContractPresenter : MvpPresenter<ProfileContractView>() {

}