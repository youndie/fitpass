package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.params.RegisterImpressionParams
import ru.workinprogress.fitpass.api.serializers.News
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 16.03.2018.
 */

interface NewsDetailView : MvpView {
    fun showNews(newsItem: News)
}

@InjectViewState
class NewsDetailPresenter(val newsItem: News) : LifecyclePresenter<NewsDetailView>() {

    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var api: Api


    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showNews(newsItem)
        api.registerNewsImpression(RegisterImpressionParams(newsItem.ID)).async().subscribe({}, {})
    }


}