package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import io.reactivex.Single
import io.reactivex.rxkotlin.Observables
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response
import ru.workinprogress.fitpass.api.serializers.Club
import ru.workinprogress.fitpass.api.serializers.Person
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.CardInteractor
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.domain.ErrorInteractor
import ru.workinprogress.fitpass.domain.PersonInteractor
import ru.workinprogress.fitpass.ui.activity.async
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by panic on 19.03.2018.
 */

interface MainWindowView : TitledView {
    fun showName(name: String)
    fun showClub(clubs: Array<Club>, selected: Club?)

}

@InjectViewState
class MainWindowPresenter : BasePresenter<MainWindowView>() {

    @Inject
    lateinit var cardInteractor: CardInteractor

    @Inject
    lateinit var personInteractor: PersonInteractor

    @Inject
    lateinit var clubInteractor: ClubInteractor
    @Inject
    lateinit var errorInteractor: ErrorInteractor

    init {
        Injector.inject(this)
        whileAlive {
            toolbarInteractor.observeTitle().subscribe {
                viewState.showTitle(it)
            }
        }
        whileAlive {
            toolbarInteractor.observeShowing().subscribe {
                viewState.showToolbarElevation(it)
            }
        }
        whileAlive {
            toolbarInteractor.observeAlpha().subscribe {
                viewState.toolbarAlpha(it)
            }
        }
        whileAlive {
            Observables.combineLatest(
                    clubInteractor.observeClubs,
                    clubInteractor.observeSelectedClubId)
            { u, p -> u to p }.async().subscribe({ pair ->
                viewState.showClub(pair.first, pair.first.firstOrNull { it.ID == pair.second })
            })
        }
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        cardInteractor.load().async().subscribe({}, {})
        personInteractor.person.delay(300, TimeUnit.MILLISECONDS).async().subscribe({
            viewState.showName(it.NickName)
        }, {
            errorInteractor.toApiError(it)
        })
        clubInteractor.update().async().subscribe({}, {})

    }

}