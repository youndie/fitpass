package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.Observables
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.*
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.*
import ru.workinprogress.fitpass.ui.activity.MainActivity.MainNavigation.router
import ru.workinprogress.fitpass.ui.activity.async
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by panic on 06.03.2018.
 */

interface MainView : MvpView {
    fun showClubs(clubs: Array<Club>, selectedClubId: String)
    fun showClubsLoading()
    fun showFitpassNewsLoading()
    fun showNews(it: MutableList<SpecialOffer>)
    fun showTrainingsLoading()
    fun showClubNewsLoading()
    fun showContractsLoading()
    fun showStatusLoading(b: Boolean)
    fun showTodayTrainings(trainings: List<UIScheduleItem>)
    fun showFavTrainings(trainings: List<UIScheduleItem>)
    fun showContracts(contracs: Array<ContractStatus>)
    fun showClubStatus(contracts: Boolean, access: Boolean, debts: Boolean, blocks: Boolean, needPay: ContractStatus?)
    fun showClubNews(subList: List<SpecialOffer>)
    fun showLogo(uri: String?, logo: String?, fitpass: Boolean = false)
    fun showLogo(uri: String?)
    fun finishAllUpdate()

}

@InjectViewState
class MainPresenter : BasePresenter<MainView>() {
    @Inject
    lateinit var unpaidInteractor: UnpaidInteractor

    init {
        Injector.inject(this)
        elevation = false
        alpha = true
        title = "Главное"

        whileAlive {
            Observables.combineLatest(
                    clubInteractor.observeClubs,
                    clubInteractor.observeSelectedClubId)
            { u, p -> u to p }.async().subscribe({
                loadClub(it.second)
                viewState.showClubs(it.first, it.second)
            })
        }

    }

    @Inject
    lateinit var scheduleInteractor: ScheduleInteractor

    @Inject
    lateinit var clubInteractor: ClubInteractor

    @Inject
    lateinit var contractsInteractor: ContractsInteractor

    @Inject
    lateinit var api: Api


    val clubs = mutableListOf<Club>()
    val fitpassNews = mutableListOf<SpecialOffer>()
    val clubNews = mutableListOf<SpecialOffer>()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        whileAlive {
            contractsInteractor.observeContracts.async().subscribe({
                viewState.showContracts(it)
                viewState.showStatusLoading(true)
                api.getAvailableBlocks(clubInteractor.selectedClubId).unboxAsync().subscribe({ blocks ->
                    unpaidInteractor.load().async().subscribe({ unpaid ->
                        viewState.showClubStatus(it.any { it.StatusCode == 1 }, it.any { it.StatusCode == 1 }, unpaid.isNotEmpty(), blocks.isNotEmpty(), it.firstOrNull { it.ExpectedPayments.isNotEmpty() })
                    }, {
                        viewState.showStatusLoading(false)
                    })

                }, {})

            }, {
                it.toString()
            })
        }
        contractsInteractor.update().async().subscribe({},{})

        viewState.showClubsLoading()
        loadNews()

    }

    private fun loadNews() {
        viewState.showFitpassNewsLoading()

        api.getNews(offset = 0).unboxAsync().subscribe({
            fitpassNews.clear()
            fitpassNews.addAll(it)
            viewState.showNews(if (fitpassNews.size > 4) fitpassNews.subList(0, 4) else fitpassNews)
        }, {})


    }

    private fun loadClub(clubId: String) {
        viewState.showTrainingsLoading()
        viewState.showClubNewsLoading()
        viewState.showContractsLoading()

        scheduleInteractor.getTodayTrainings().async().subscribe({
            viewState.showTodayTrainings(it)
        }, {})
        scheduleInteractor.getFavTrainings().async().subscribe({
            viewState.showFavTrainings(it)
        }, {})
        contractsInteractor.update().async().subscribe({
        }, {})
        api.getNews(clubId = clubId, offset = 0).unboxAsync().subscribe({
            clubNews.clear()
            clubNews.addAll(it)
            if (clubNews.size > 4)
                viewState.showClubNews(clubNews.subList(0, 4))
            else
                viewState.showClubNews(clubNews)
        }, {})


    }


    fun onPageChanged(fitPassPage: Boolean) {
        if (fitPassPage) viewState.showLogo(null, null, true) else {
            clubInteractor.selectedClub.async().subscribe({
                if (it?.logo != null) {
                    viewState.showLogo(it.logo?.Uri)
                } else {
                    viewState.showLogo(it?.background?.Uri, it?.icon?.Uri)
                }
            }, {})
        }
    }

    fun onFitPassNewsMoreClicked() {
//        viewState.showNews(fitpassNews)
    }

    fun onClubChanged(club: Club) {
        clubInteractor.selectedClubId = club.ID
    }

    fun refreshAll() {
        clubInteractor.update().async().subscribe({
            viewState.finishAllUpdate()
        }, {
        })
    }

    fun refreshClub() {
        clubInteractor.selectedClub.async().subscribe({
            loadClub(it?.ID.orEmpty())

        }, {})
    }


}

abstract class LifecyclePresenter<F : MvpView> : MvpPresenter<F>() {
    private val disposables = CompositeDisposable()

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }


    fun whileAlive(block: () -> Disposable) {
        disposables.add(block())
    }
}

abstract class BasePresenter<F : MvpView> : TitledPresenter<F>() {

    private val disposables = CompositeDisposable()

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }


    fun whileAlive(block: () -> Disposable) {
        disposables.add(block())
    }
}

interface TitledView : MvpView {
    fun showTitle(it: String)
    fun showToolbarElevation(it: Boolean)
    fun toolbarAlpha(it: Boolean)
}

abstract class TitledPresenter<F : MvpView> : MvpPresenter<F>() {
    @Inject
    lateinit var toolbarInteractor: ToolbarInteractor

    private var _showing = true
    private var _title = ""
    private var _alpha = false

    var elevation: Boolean
        get() = _showing
        set(value) {
            _showing = value
            toolbarInteractor.showing = value
        }
    var title: String
        get() = _title
        set(value) {
            _title = value
            toolbarInteractor.title = value
        }

    var alpha: Boolean
        get() = _alpha
        set(value) {
            _alpha = value
            toolbarInteractor.alpha = value
        }

    override fun attachView(view: F) {
        super.attachView(view)
        title = _title
        elevation = _showing
        alpha = _alpha
    }


}