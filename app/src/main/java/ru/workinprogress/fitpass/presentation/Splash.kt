package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.api.AuthService
import ru.workinprogress.fitpass.api.params.LoginParams
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.AuthInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

interface SplashView : MvpView {
    @StateStrategyType(SkipStrategy::class)
    fun startEnterActivity()

    @StateStrategyType(SkipStrategy::class)
    fun startMainActivity()
    @StateStrategyType(SkipStrategy::class)
    fun startPinActivity()

}

@InjectViewState
class SplashPresenter : MvpPresenter<SplashView>() {

    @Inject
    lateinit var authInteractor: AuthInteractor

    init {
        Injector.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        authInteractor.resetSession()
        if (!authInteractor.paramsExists) {
            authInteractor.simpleLogin().async().subscribe({
                viewState.startEnterActivity()
            }, {})
        } else {
            authInteractor.advancedLogin().async().subscribe({
                if (authInteractor.pinEnabled) {
                    viewState.startPinActivity()
                } else {
                    viewState.startMainActivity()
                }
            }, {})
        }

    }
}