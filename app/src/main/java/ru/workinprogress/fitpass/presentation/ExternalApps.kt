package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.ExternalSystemURI
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

interface ExternalAppsView : MvpView {
    fun startLoading()
    fun finishLoading()
    fun showLinks(externalSystemURI: Array<ExternalSystemURI>?)
}

@InjectViewState
class ExternalAppsPresenter : BasePresenter<ExternalAppsView>() {

    @Inject
    lateinit var clubInteractor: ClubInteractor

    init {
        Injector.inject(this)
        title = "Внешние приложения"
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.startLoading()
        clubInteractor.selectedClub.async().subscribe({
            it?.ExternalSystemURIs.let(viewState::showLinks)
            viewState.finishLoading()
        }, {
            viewState.finishLoading()
        })
    }


}