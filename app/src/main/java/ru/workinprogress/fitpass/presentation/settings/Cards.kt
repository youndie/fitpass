package ru.workinprogress.fitpass.presentation.settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import ru.workinprogress.fitpass.api.serializers.PaymentCard
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.CardInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 26.03.2018.
 */

interface CardsView : MvpView {
    fun showCards(cards: Array<PaymentCard>)
    fun startLoading()
    fun finishLoading()
    fun showError()
}

@InjectViewState
class CardsPresenter : MvpPresenter<CardsView>() {

    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var cardInteractor: CardInteractor

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.startLoading()
        cardInteractor.load().async().subscribe({
            viewState.finishLoading()
            viewState.showCards(it)
        }, {
            viewState.finishLoading()
            viewState.showError()
        })
    }

    fun onDeleteCard(card: PaymentCard) {

    }


}