package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.LegacyDocuments
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.AuthInteractor
import ru.workinprogress.fitpass.domain.CardInteractor
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.domain.PersonInteractor
import javax.inject.Inject

/**
 * Created by panic on 26.03.2018.
 */

interface SettingsView : MvpView {
    fun showDocs(it: LegacyDocuments)
    @StateStrategyType(SkipStrategy::class)
    fun startPinActivity(pinEnabled: Boolean)

}

@InjectViewState
class SettingsPresenter : BasePresenter<SettingsView>() {

    @Inject
    lateinit var authInteractor: AuthInteractor

    @Inject
    lateinit var personInteractor: PersonInteractor

    @Inject
    lateinit var clubInteractor: ClubInteractor

    @Inject
    lateinit var cardInteractor: CardInteractor

    @Inject
    lateinit var api: Api

    fun onExit() {
        authInteractor.reset()
        personInteractor.reset()
        clubInteractor.reset()
        cardInteractor.reset()
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        api.getLegalDocuments().unboxAsync().subscribe({
            viewState.showDocs(it)
        }, {})
    }

    fun onPinClicked() {
        viewState.startPinActivity(authInteractor.pinEnabled)
    }

    init {
        Injector.inject(this)
        title = "Настройки"
    }

}