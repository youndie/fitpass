package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.api.serializers.Club
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ClubInteractor
import ru.workinprogress.fitpass.ui.activity.async
import javax.inject.Inject

/**
 * Created by panic on 15.03.2018.
 */

interface AboutClubView : MvpView {
    fun showClub(club: Club)
    fun showError()

}

@InjectViewState
class AboutMyClubPresenter : TitledPresenter<AboutClubView>() {

    @Inject
    lateinit var clubInteractor: ClubInteractor

    init {
        Injector.inject(this)
        title = "О клубе"
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        clubInteractor.selectedClub.async().subscribe({
            if (it != null) {
                viewState.showClub(it)
            } else {
                viewState.showError()
            }
        }, {})
    }

}

@InjectViewState
class AboutClubPresenter(private val club: Club) : LifecyclePresenter<AboutClubView>() {

    init {
        Injector.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showClub(club)
    }

}