package ru.workinprogress.fitpass.presentation

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.workinprogress.fitpass.api.Api
import ru.workinprogress.fitpass.api.serializers.Operation
import ru.workinprogress.fitpass.api.serializers.SaleItem
import ru.workinprogress.fitpass.api.serializers.unboxAsync
import ru.workinprogress.fitpass.di.Injector
import ru.workinprogress.fitpass.domain.ClubInteractor
import javax.inject.Inject

/**
 * Created by panic on 26.03.2018.
 */

interface OperationsView : MvpView {
    fun showOperations(it: Array<SaleItem>)
    fun startLoading()
    fun finishLoading()
    fun showError()
}


@InjectViewState
class OperationsPresenter : LifecyclePresenter<OperationsView>() {
    init {
        Injector.inject(this)
    }

    @Inject
    lateinit var api: Api
    @Inject
    lateinit var clubInteractor: ClubInteractor

    fun load() {
        viewState.startLoading()
        api.getSales(clubInteractor.selectedClubId).unboxAsync().subscribe({
            viewState.finishLoading()
            viewState.showOperations(it)
        }, {
            viewState.finishLoading()
            viewState.showError()
        })
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        load()
    }


}